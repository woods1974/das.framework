﻿using System;

namespace Das.Framework.Core
{
    /// <summary>
    /// Generic Class Factory
    /// </summary>
    /// <typeparam name="TInterface">Supported interface</typeparam>
    /// <typeparam name="TClass">Class which implements the supported interface.</typeparam>
    public static class ClassFactory<TInterface, TClass> where TClass : TInterface
    {
        /// <summary>
        /// Provides the functionality to derive from another concerte class that implements the same interface.
        /// </summary>
        public static Func<object[], TInterface> Dispenser { get; set; }
        /// <summary>
        /// Create an instance of a concerte class that supports the Interface.
        /// </summary>
        /// <param name="args">Overload parameters</param>
        /// <returns>Returns an instance of the new concerte class.</returns>
        public static TInterface CreateInstance(params object[] args)
        {
            if (Dispenser != null)
                return Dispenser(args);

            return (TInterface)Activator.CreateInstance(typeof(TClass), args);
        }
    }
}
