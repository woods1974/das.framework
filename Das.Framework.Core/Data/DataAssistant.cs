﻿using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Data.Extensions;

namespace Das.Framework.Core.Data
{
    public class DataAssistant
    {
        public DataAssistant(string connectionString)
        {
            ConnectionString = connectionString;
        }

        #region Properties

        /// <summary>
        /// Database connection string.
        /// </summary>
        protected string ConnectionString { get; private set; }

        #endregion

        /// <summary>
        /// Get an entity instance that is mapped from a stored procedure.
        /// </summary>
        /// <typeparam name="T">the entity type</typeparam>
        /// <param name="procedureName">the procedure name</param>
        /// <param name="materializer">the mapping logic to create the entity from the <see cref="IDataRecord"/></param>
        /// <param name="parameters">the input <see cref="SqlParameter"/>s</param>
        /// <returns>the single entity instance expected from the sproc</returns>
        public virtual T ExecuteStoredProcedureSingle<T>(string procedureName, Materializer<T> materializer, params SqlParameter[] parameters)
        {
            var resultSet = ExecuteStoredProcedureCollection(procedureName, materializer, parameters);
            return resultSet != null && resultSet.Any() ? resultSet.Single() : default(T);
        }

        /// <summary>
        /// Get a collection of entity instances returned from the specified stored procedure.
        /// </summary>
        /// <typeparam name="T">the entity type</typeparam>
        /// <param name="procedureName">the procedure name</param>
        /// <param name="materializer">the mapping logic to create the entities from the <see cref="IDataRecord"/>s</param>
        /// <param name="parameters">the input <see cref="SqlParameter"/>s</param>
        /// <returns>a collection of entity instances expected from the sproc</returns>
        public virtual Collection<T> ExecuteStoredProcedureCollection<T>(string procedureName, Materializer<T> materializer, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (connection.CreateConnectionScope())
                {
                    using (
                        var command = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure })
                    {
                        command.Parameters.AddRange(parameters);

                        var results = materializer.Materialize(command);
                        if (results == null)
                            return null;

                        var resultSet = new Collection<T>();
                        foreach (T result in results)
                            resultSet.Add(result);

                        return resultSet;
                    }
                }
            }
        }

        /// <summary>
        /// Execute a non-query stored procedure.
        /// </summary>
        /// <param name="procedureName">the procedure name</param>
        /// <param name="parameters">the input <see cref="SqlParameter"/>s</param>
        /// <returns>Returns the number of rows affected</returns>
        public virtual int ExecuteStoredProcedure(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (connection.CreateConnectionScope())
                {
                    using (var command = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure })
                    {
                        command.Parameters.AddRange(parameters);
                        return command.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
