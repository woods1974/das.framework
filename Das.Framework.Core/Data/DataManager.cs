﻿namespace Das.Framework.Core.Data
{
    /// <summary>
    /// Data Manager base class 
    /// </summary>
    public abstract class DataManager
    {
        private DataAssistant _sqlAssistant;
        /// <summary>
        /// Data Manager base class 
        /// </summary>
        /// <param name="connectionString">Database connection string</param>
        protected DataManager(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Database connection stirng
        /// </summary>
        protected string ConnectionString { get; set; }

        /// <summary>
        /// Sql database assistant 
        /// </summary>
        protected DataAssistant SqlAssistant
        {
            get { return _sqlAssistant ?? (_sqlAssistant = new DataAssistant(ConnectionString)); }
        }
    }
}
