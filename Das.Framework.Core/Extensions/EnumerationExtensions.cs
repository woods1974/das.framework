﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Das.Framework.Core.Extensions
{
    /// <summary>
    /// Extension methods that act on enumerations
    /// </summary>
    public static class EnumerationExtensions
    {
        /// <summary>
        /// Creates a comma delimited string from the items in the enumeration.
        /// </summary>
        /// <typeparam name="T">Type of items in the enumeration</typeparam>
        /// <param name="enumeration">The enumeration to be described</param>
        /// <returns>Comma delimited string for each item in the enumeration, or <see cref="String.Empty" /> if the enumeration is null or empty.</returns>
        public static string Delimit<T>(this IEnumerable<T> enumeration)
        {
            return Delimit(enumeration, ", ");
        }

        /// <summary>
        /// Creates a delimited string from the items in the enumeration.
        /// </summary>
        /// <typeparam name="T">Type of items in the enumeration</typeparam>
        /// <param name="enumeration">The enumeration to be described</param>
        /// <param name="delimiter">The delimiter to use between each value</param>
        /// <returns>Comma delimited string for each item in the enumeration, or <see cref="String.Empty" /> if the enumeration is null or empty.</returns>
        public static string Delimit<T>(this IEnumerable<T> enumeration, string delimiter)
        {
            return Delimit(enumeration, delimiter, value => value.ToString());
        }

        /// <summary>
        /// Creates a delimited string from the items in the enumeration.
        /// </summary>
        /// <typeparam name="T">Type of items in the enumeration</typeparam>
        /// <param name="enumeration">The enumeration to be described</param>
        /// <param name="delimiter">The delimiter to use between each value</param>
        /// <param name="descriptor">Descriptor function to use for describing each value in the enumeration (by default, this is the object's ToString() method).</param>
        /// <returns>Comma delimited string for each item in the enumeration, or <see cref="String.Empty" /> if the enumeration is null or empty.</returns>
        public static string Delimit<T>(this IEnumerable<T> enumeration, string delimiter, Func<T, string> descriptor)
        {
            if (enumeration == null || !enumeration.Any())
                return String.Empty;

            var builder = new StringBuilder();
            foreach (T item in enumeration)
            {
                if (builder.Length > 0)
                    builder.Append(delimiter);

                builder.Append(descriptor.Invoke(item));
            }

            return builder.ToString();
        }
    }
}
