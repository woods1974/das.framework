﻿using System;
using System.Linq.Expressions;

namespace Das.Framework.Core.Extensions
{
    /// <summary>
    /// Extension methods that operate on expressions
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Given a property expression return the property name.
        /// Eg. contactDetails.FirstName would return "FirstName".
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        public static string GetPropertyName<T, TProperty>(this Expression<Func<T, TProperty>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
            {
                var unaryExpression = expression.Body as UnaryExpression;
                if (unaryExpression != null)
                {
                    memberExpression = unaryExpression.Operand as MemberExpression;
                    if (memberExpression == null)
                    {
                        throw new NotSupportedException();
                    }
                }
                else
                {
                    throw new NotSupportedException();
                }
            }

            return memberExpression.Member.Name;
        }

        public static MemberExpression GetMemberExpression<T>(Expression<Func<T, object>> expression)
        {
            // Default expression
            MemberExpression memberExpression = null;
            // Convert
            switch (expression.Body.NodeType)
            {
                case ExpressionType.Convert:
                    {
                        var body = (UnaryExpression)expression.Body;
                        memberExpression = body.Operand as MemberExpression;
                    }
                    break;
                case ExpressionType.MemberAccess:
                    memberExpression = expression.Body as MemberExpression;
                    break;
            }
            // Not a member access
            if (memberExpression == null)
                throw new ArgumentException("Not a member access", "expression");
            // Return the member expression
            return memberExpression;
        }
    }
}
