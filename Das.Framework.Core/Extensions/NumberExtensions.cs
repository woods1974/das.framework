﻿using System;

namespace Das.Framework.Core.Extensions
{
    /// <summary>
    /// Extension methods that operate on numbers
    /// </summary>
    public static class NumberExtensions
    {
        /// <summary>
        /// Rounds using symmetric arithmetic rounding
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static decimal ToMoney(this decimal val)
        {
            return Math.Round(val, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Rounds using symmetric arithmetic rounding
        /// </summary>
        /// <param name="val"></param>
        /// <param name="decimalPlaces"></param>
        /// <returns></returns>
        /// <see cref="ArgumentException"/>
        public static decimal RoundValue(this decimal val, int decimalPlaces)
        {
            if (decimalPlaces > 28)
            {
                throw new ArgumentException("decimalPlace must be less than 29 digits", "decimalPlaces");
            }

            return Math.Round(val, decimalPlaces, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Rounds using symmetric arithmetic rounding
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static double Round(this float val)
        {
            return Convert.ToDouble(System.Math.Round(val, 2, MidpointRounding.AwayFromZero));
        }

        public static int ToPercentage(this int total, int count)
        {
            if (total == 0 || count >= total)
                return 100;

            if (count == 0)
                return 0;

            return (int)(0.5f + ((100f * count) / total));
        }
    }
}
