﻿using System;
using System.Linq;

namespace Das.Framework.Core.Extensions
{
    /// <summary>
    /// Extension methods which can operate on objects or primitives
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Determines if any of the given matches are equal to the nominated value.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="matches">the list of potential matches</param>
        /// <returns></returns>
        public static bool IsIn(this object obj, params object[] matches)
        {
            return matches.Any(o => ReferenceEquals(obj, o) || Equals(obj, o));
        }

        /// <summary>
        /// Retrieves the name of the constant in the specified enumeration that has the specified value.
        /// </summary>
        /// <param name="enumeration">The value of a particular enumerated constant in terms of its underlying type.</param>
        /// <returns>A string containing the name of the enumerated constant in enumType whose value is value, or null if no such constant is found.</returns>
        public static string GetName(this Enum enumeration)
        {
            return Enum.GetName(enumeration.GetType(), enumeration);
        }
    }
}
