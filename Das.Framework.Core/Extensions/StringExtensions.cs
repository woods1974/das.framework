﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace Das.Framework.Core.Extensions
{ /// <summary>
    /// Extension methods that act on strings
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Take all the words in the input string and separate them.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] SplitWords(this string input)
        {
            return Regex.Split(input, @"\W+");
        }

        /// <summary>
        /// Determines if any of the given matches are a case sensitive match for the
        /// nominated string value
        /// </summary>
        /// <param name="text">The string value to be checked</param>
        /// <param name="caseSensitive"><c>true</c> to perform a case
        /// sensitive match</param>
        /// <param name="matches">The list of potential matches</param>
        public static bool IsIn(this string text, bool caseSensitive, params string[] matches)
        {
            var comparer = caseSensitive
                ? StringComparison.CurrentCulture
                : StringComparison.CurrentCultureIgnoreCase;

            return matches.Any(value => text.Equals(value, comparer));
        }

        /// <summary>
        /// Get substring of specified number of characters on the right.
        /// </summary>
        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        /// <summary>
        /// Convets text into Proper Case.
        /// </summary>
        /// <param name="value">Text to be converted.</param>
        /// <returns>String in proper case format.</returns>
        public static string ToProperCase(this string value)
        {
            var cultureInfo = Thread.CurrentThread.CurrentCulture;
            var textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(value);
        }
    }
}
