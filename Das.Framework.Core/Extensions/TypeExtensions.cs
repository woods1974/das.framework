﻿using System;
using System.Linq;

namespace Das.Framework.Core.Extensions
{
    /// <summary>
    /// Extensions that operate on a <see cref="System.Type" />
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Checks if a type is derived from a generic type for unlimited levels of inheritance
        /// </summary>
        /// <param name="typeToCheck"></param>
        /// <param name="genericType"></param>
        /// <returns></returns>
        public static bool IsDerivedFromGenericType(this Type typeToCheck, Type genericType)
        {
            if (typeToCheck == typeof(object))
            {
                return false;
            }

            if (typeToCheck == null)
            {
                return false;
            }

            if (typeToCheck.IsGenericType && typeToCheck.GetGenericTypeDefinition() == genericType)
            {
                return true;
            }

            return IsDerivedFromGenericType(typeToCheck.BaseType, genericType) || typeToCheck.GetInterfaces()
                .Select(type => IsDerivedFromGenericType(type, genericType))
                .Any(result => result);
        }

        /// <summary>
        /// Checks if the <see cref="Type"/> is derived from a particular base <see cref="Type"/> for unlimited levels of inheritance.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static bool HasBaseType(this Type type, out Type baseType)
        {
            return HasBaseType(type, out baseType, null);
        }

        /// <summary>
        /// Checks if the <see cref="Type"/> is derived from a particular base <see cref="Type"/> for unlimited levels of inheritance.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="baseType"></param>
        /// <param name="exclusion"></param>
        /// <returns></returns>
        public static bool HasBaseType(this Type type, out Type baseType, Type exclusion)
        {
            Type originalType = type;
            baseType = GetBaseType(type, exclusion);
            return baseType != originalType;
        }

        /// <summary>
        /// Returns the base <see cref="Type"/> for a given <see cref="Type"/> where the base <see cref="Type"/> isn't of the type of the exclusion for unlimited levels of inheritance.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="exclusion"></param>
        /// <returns></returns>
        public static Type GetBaseType(this Type type, Type exclusion)
        {
            Type baseType = type.BaseType;

            return baseType != null && baseType != exclusion
                ? GetBaseType(type.BaseType, exclusion)
                : type;
        }
    }
}
