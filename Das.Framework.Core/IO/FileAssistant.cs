﻿using System.IO;

namespace Das.Framework.Core.IO
{
    public static class FileAssistant
    {
        /// <summary>
        /// Delete any file within the directory and or sub-directory.
        /// </summary>
        /// <param name="directory">Directory to delete files from.</param>
        /// <param name="includeSubDirectories">If true, also deletes files from sub-directories.</param>
        public static void EmptyDirectory(this DirectoryInfo directory, bool includeSubDirectories = false)
        {
            foreach (var file in directory.GetFiles()) file.Delete();
            if (!includeSubDirectories) return;
            foreach (var subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }

        /// <summary>
        /// Determine whether the file is in use.
        /// </summary>
        /// <param name="filename">Filename and path.</param>
        /// <returns>True, when the file is currently in use.</returns>
        public static bool IsFileInUse(string filename)
        {
            try
            {
                using (var fs = new FileStream(filename, FileMode.OpenOrCreate))
                {
                    return fs.CanWrite;
                }
            }
            catch (IOException)
            {
                return true;
            }
        }
    }
}
