﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Das.Framework.Core.Extensions;
using Das.Framework.Core.Notification.Interfaces;

namespace Das.Framework.Core.Notification
{
    public class ChangeNotification<T> : IChangeNotification
    {
        /// <summary>
        /// Raises all property changed events in the propertyNames array.
        /// </summary>
        /// <param name="propertyNames"></param>
        public void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null || IsNotificationSuspended) return;

            foreach (var property in propertyNames)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
        }

        /// <summary>
        /// Sets the suspended state of the change notification.
        /// When true, will disable all property changed notification events.
        /// </summary>
        /// <param name="suspendNotification"></param>
        public void SetSuspendNotification(bool suspendNotification)
        {
            IsNotificationSuspended = suspendNotification;
        }

        /// <summary>
        /// Indicates whether the notification events has been suspended.
        /// </summary>
        public bool IsNotificationSuspended { get; private set; }

        /// <summary>
        /// Notify a property changed event.
        /// </summary>
        /// <param name="property">Property name that has changed.</param>
        public void NotifyPropertyChanged(Expression<Func<T, object>> property)
        {
            // Get the property
            var propertyExpression = ExpressionExtensions.GetMemberExpression(property);
            if (propertyExpression == null)
                throw new InvalidOperationException("You must specify a property");

            // Property name
            var propertyName = propertyExpression.Member.Name;
            NotifyPropertyChanged(propertyName);
        }
        
        /// <summary>
        /// Event for notifying property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
