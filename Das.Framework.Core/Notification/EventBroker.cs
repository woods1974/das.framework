﻿using System;
using System.Collections.Generic;
using Das.Framework.Core.Notification.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Das.Framework.Core.Notification
{
    /// <summary>
    /// Manages Composite event subscriptions.
    /// </summary>
    public class EventBroker : IEventBroker
    {
        private Dictionary<object, List<KeyValuePair<SubscriptionToken, EventBase>>> _eventSubscriptions;

        private readonly IEventAggregator _eventAggregator;

        public EventBroker(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        private Dictionary<object, List<KeyValuePair<SubscriptionToken, EventBase>>> EventSubscriptions
        {
            get { return _eventSubscriptions ?? (_eventSubscriptions = new Dictionary<object, List<KeyValuePair<SubscriptionToken, EventBase>>>()); }
        }

        public void Subscribe<TEventType, TPayloadType>(object owner, Action<TPayloadType> action) where TEventType : EventBase
        {
            Subscribe<TEventType, TPayloadType>(owner, action, ThreadOption.PublisherThread, null);
        }

        private void Subscribe<TEventType, TPayloadType>(object owner, Action<TPayloadType> action, ThreadOption threadOption, Predicate<TPayloadType> filter) where TEventType : EventBase
        {
            var compositeEvent = (_eventAggregator.GetEvent<TEventType>() as CompositePresentationEvent<TPayloadType>);
            if (compositeEvent == null)
            {
                return;
            }

            var subscriptionToken = compositeEvent.Subscribe(action, threadOption, true, filter);
            var subscriptionList = CommandEventsByKey(owner);
            if (subscriptionList != null)
            {
                subscriptionList.Add(new KeyValuePair<SubscriptionToken, EventBase>(subscriptionToken, compositeEvent));
            }
            else
            {
                EventSubscriptions.Add(owner, new List<KeyValuePair<SubscriptionToken, EventBase>>{
                                                                                                      new KeyValuePair<SubscriptionToken, EventBase>(subscriptionToken, compositeEvent)
                                                                                                  });
            }
        }

        private List<KeyValuePair<SubscriptionToken, EventBase>> CommandEventsByKey(object obj)
        {
            return EventSubscriptions.ContainsKey(obj) ? EventSubscriptions[obj] : null;
        }

        public void Unsubscribe(object owner)
        {
            lock (EventSubscriptions)
            {
                var eventList = CommandEventsByKey(owner);
                if (eventList != null)
                {
                    eventList.ForEach(kvp => kvp.Value.Unsubscribe(kvp.Key));
                }
                EventSubscriptions.Remove(owner);
            }
        }

        public void Publish<TEventType, TPayloadType>(TPayloadType payload, bool publishOnCallingThread = true) where TEventType : EventBase
        {

            var compositeEvent = _eventAggregator.GetEvent<TEventType>() as CompositePresentationEvent<TPayloadType>;
            if (compositeEvent == null)
            {
                return;
            }

            PublishEventOnCorrectThread(compositeEvent, payload, publishOnCallingThread);
        }

        internal void PublishEventOnCorrectThread<TPayloadType>(CompositePresentationEvent<TPayloadType> compositeEvent, TPayloadType payload, bool publishOnCallingThread)
        {
            if (publishOnCallingThread)
            {
                compositeEvent.Publish(payload);
            }
        }
    }
}
