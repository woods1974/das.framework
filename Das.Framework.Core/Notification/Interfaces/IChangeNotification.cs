﻿using System.ComponentModel;

namespace Das.Framework.Core.Notification.Interfaces
{
    /// <summary>
    /// Specialised INotifyPropertyChanged interface.
    /// </summary>
    public interface IChangeNotification : INotifyPropertyChanged
    {
        /// <summary>
        /// Implement this method to cycle through specified properties calling the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyNames">List of property names</param>
        void NotifyPropertyChanged(params string[] propertyNames);


        /// <summary>
        /// Sets the suspended state of the change notification.
        /// When true, will disable all property changed notification events.
        /// </summary>
        /// <param name="suspendNotification"></param>
        void SetSuspendNotification(bool suspendNotification);

        /// <summary>
        /// Indicates whether the notification events has been suspended.
        /// </summary>
        bool IsNotificationSuspended { get; }

    }
}
