﻿using System;

namespace Das.Framework.Core.Notification.Interfaces
{
    public interface IEventBroker
    {
        /// <summary>
        /// Publishies an event.
        /// </summary>
        /// <typeparam name="TEventType"></typeparam>
        /// <typeparam name="TPayloadType"></typeparam>
        /// <param name="payload"></param>
        /// <param name="publishOnCallingThread"></param>
        void Publish<TEventType, TPayloadType>(TPayloadType payload, bool publishOnCallingThread = true) where TEventType : Microsoft.Practices.Composite.Events.EventBase;

        /// <summary>
        /// Subscribe to an event.
        /// </summary>
        /// <typeparam name="TEventType"></typeparam>
        /// <typeparam name="TPayloadType"></typeparam>
        /// <param name="owner"></param>
        /// <param name="action"></param>
        void Subscribe<TEventType, TPayloadType>(object owner, Action<TPayloadType> action) where TEventType : Microsoft.Practices.Composite.Events.EventBase;

        /// <summary>
        /// Unsubscribe from all events.
        /// </summary>
        /// <param name="owner"></param>
        void Unsubscribe(object owner);
    }
}
