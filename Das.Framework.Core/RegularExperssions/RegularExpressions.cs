namespace Das.Framework.Core.RegularExperssions
{
    /// <summary>
    /// Common list of regular expressions.
    /// </summary>
    public static class RegularExpressions
    {
        /// <summary>
        /// Restrict to only numeric values using [0-9].
        /// </summary>
        public const string Numeric = "[0-9]";
    }
}