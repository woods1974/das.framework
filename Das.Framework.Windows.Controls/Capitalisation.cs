﻿namespace Das.Framework.Windows.Controls
{
    /// <summary>
    /// Represents the text capitalisation style.
    /// </summary>
    public enum Capitalisation
    {
        Default,
        UpperCase,
        LowerCase,
        ProperCase
    }
}
