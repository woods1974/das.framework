﻿using System;

namespace Das.Framework.Windows.Controls.Common
{
    public static class DateTimeAssistant
    {
        public const string ShortDate = "dd-MMM-yyyy";
        public const string LongDate = "dd MMMM yyyy";

        /// <summary>
        /// Calculates the age between two dates.
        /// </summary>
        /// <param name="fromDate">From date, generally the date of birth.</param>
        /// <param name="toDate">To Date, a point in time to be evaluated.</param>
        /// <returns></returns>
        public static double GetAge(DateTime fromDate, DateTime toDate)
        {
            var years = toDate.Year - fromDate.Year;
            
            var fraction = 365
                    + (DateTime.IsLeapYear(toDate.Year) && toDate.DayOfYear >= 59
                    && (fromDate.DayOfYear < 59 || fromDate.DayOfYear > toDate.DayOfYear)
                    ? 1 : 0);

          
            if (DateTime.IsLeapYear(toDate.Year) == DateTime.IsLeapYear(fromDate.Year))
                return years + (toDate.DayOfYear - fromDate.DayOfYear) / fraction;


            if (DateTime.IsLeapYear(toDate.Year))
                return (years + (toDate.DayOfYear - fromDate.DayOfYear
                                 - (fromDate.DayOfYear >= 59 ? 1 : 0))/fraction);

            if (fromDate.DayOfYear != 59)
                return years + (toDate.DayOfYear - fromDate.DayOfYear
                                + (fromDate.DayOfYear > 59 ? 1 : 0))/fraction;
            
            return years + (toDate.DayOfYear - 58.5) / fraction; 
        }

        public static double GetAge(DateTime fromDate)
        {
            return GetAge(fromDate, DateTime.Now);
        }

        /// <summary>
        /// Gets a string representation of the age.
        /// </summary>
        /// <param name="fromDate">Generally the date of birth.</param>
        /// <param name="toDate">A point in time to be calculated.</param>
        /// <returns></returns>
        public static string GetDisplayAge(DateTime fromDate, DateTime toDate)
        {
            var age = GetAge(fromDate, toDate);
            return age < 1
                       ? string.Format("{0: #.0 month(s)}", age).Replace(".", string.Empty)
                       : (age == 1 ? string.Format("{0:0.# year}", age).Replace(".", string.Empty)
                              : string.Format("{0:0.0 years}", age)); 
        }
    }
}
