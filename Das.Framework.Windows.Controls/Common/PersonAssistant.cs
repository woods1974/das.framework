﻿using Das.Framework.Core.Extensions;
using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Common
{
    public static class PersonAssistant
    {
        /// <summary>
        /// Provides a string representation of the persons name.
        /// </summary>
        /// <param name="person">Person class to be evaluated.</param>
        /// <returns>Returns the formatted persons name FAMILYNAME, GivenName (Title)</returns>
        public static string ToPerson(ICribPerson person)
        {
            if (person == null)
                return string.Empty;

            if (HasFullyQualifiedName(person))
                return string.Format("{0}, {1} ({2})", person.FamilyName.ToUpper(), person.GivenName.ToProperCase(), person.Title.ToProperCase());
            if(HasFamilyAndGivenNames(person))
                return string.Format("{0}, {1}", person.FamilyName.ToUpper(), person.GivenName.ToProperCase());

            return string.Empty;
        }

        /// <summary>
        /// Determine if the person has only the family and given names.
        /// </summary>
        /// <param name="person">Person class to be evaluated.</param>
        /// <returns>Ture if the family and given names only exist.</returns>
        public static bool HasFamilyAndGivenNames(ICribPerson person)
        {
            return person != null && !string.IsNullOrWhiteSpace(person.FamilyName) &&
                            !string.IsNullOrWhiteSpace(person.GivenName);
        }

        /// <summary>
        /// Determine if the person has all required values such as Family Name, Given name and Title.
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static bool HasFullyQualifiedName(ICribPerson person)
        {
            return HasFamilyAndGivenNames(person) && !string.IsNullOrWhiteSpace(person.Title);
        }
    }
}
