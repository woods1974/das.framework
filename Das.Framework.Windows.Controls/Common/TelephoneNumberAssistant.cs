﻿using System.Text.RegularExpressions;
using Das.Framework.Core.Extensions;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Common
{
    /// <summary>
    /// Provides format assists
    /// </summary>
    public class TelephoneNumberAssistant
    {
        /// <summary>
        /// Convert the local number into a well formed mobile number.
        /// </summary>
        /// <param name="cribTelephoneNumber">Phone number</param>
        /// <returns>Well formed formatted string representation of the local number.</returns>
        public static string ToMobileNumber(ICribTelephoneNumber cribTelephoneNumber)
        {
            if (cribTelephoneNumber == null || string.IsNullOrWhiteSpace(cribTelephoneNumber.LocalNumber))
                return string.Empty;

            var localNumber = ToDigits(cribTelephoneNumber.LocalNumber); 

            if(localNumber.Length < 10)
                return localNumber;

            if (!string.IsNullOrWhiteSpace(cribTelephoneNumber.CountryCode))
            {
                localNumber = localNumber.Replace(localNumber.Substring(0, 1),string.Empty);
                return string.Format("{0} {1} {2} {3}", FormatCountryCode(cribTelephoneNumber.CountryCode), 
                    localNumber.Substring(0, 3), localNumber.Substring(3, 3), localNumber.Substring(6));
            }

            return localNumber.Length >= 10 ? 
                string.Format("{0} {1} {2}", localNumber.Substring(0, 4), 
                            localNumber.Substring(4, 3), localNumber.Substring(7)) : localNumber;
        }

        /// <summary>
        /// Formats the telephone number as a land line.
        /// </summary>
        /// <param name="cribTelephoneNumber">The telephone number to be formatted.</param>
        /// <param name="parentheses">If true, will add brackets around the area code if available.</param>
        /// <returns></returns>
        public static string ToLandLineNumber(ICribTelephoneNumber cribTelephoneNumber, bool parentheses = false)
        {
            if (cribTelephoneNumber == null || string.IsNullOrWhiteSpace(cribTelephoneNumber.LocalNumber))
                return string.Empty;
            
            if (!string.IsNullOrWhiteSpace(cribTelephoneNumber.CountryCode) && 
                !string.IsNullOrWhiteSpace(cribTelephoneNumber.AreaCode))
            {  
                return string.Format("{0} {1} {2}",
                    FormatCountryCode(cribTelephoneNumber.CountryCode), 
                    FormatAreaCode(cribTelephoneNumber.AreaCode, parentheses, true), 
                    FormatLocalNumber(cribTelephoneNumber.LocalNumber));
            }
            if(!string.IsNullOrWhiteSpace(cribTelephoneNumber.AreaCode))
            {
                return string.Format("{0} {1}", FormatAreaCode(cribTelephoneNumber.AreaCode, parentheses),
                                        FormatLocalNumber(cribTelephoneNumber.LocalNumber));
            }

            return FormatLocalNumber(cribTelephoneNumber.LocalNumber);
        }

        /// <summary>
        /// Formats the telephone number to a land line number with extersion number support.
        /// </summary>
        /// <param name="cribTelephoneNumber">Telephone number to format.</param>
        /// <param name="parentheses">When true, places brackets around the area code.</param>
        /// <returns>Formatted representation of the phone and extension numbers</returns>
        public static string ToLandLineNumberWithExtension(ICribTelephoneNumber cribTelephoneNumber, bool parentheses = false)
        {
            if(cribTelephoneNumber == null)
                return string.Empty;

            if (string.IsNullOrWhiteSpace(cribTelephoneNumber.ExtensionNumber))
                return ToLandLineNumber(cribTelephoneNumber);

            return string.Format("{0} x{1}", ToLandLineNumber(cribTelephoneNumber, parentheses), ToDigits(cribTelephoneNumber.ExtensionNumber));
        }

        /// <summary>
        /// Formats the Local Number of telephone number.
        /// </summary>
        /// <param name="value">Local Number to be formatted.</param>
        /// <returns>Returns a formatted local number.</returns>
        public static string FormatLocalNumber(string value)
        {
            var formattedLocalNumber = ToDigits(value);
            if (formattedLocalNumber.Length >= 8)
                return string.Format("{0} {1}", formattedLocalNumber.Substring(0, 4), formattedLocalNumber.Substring(4));
            if (formattedLocalNumber.Length == 6)
                return string.Format("{0} {1}", formattedLocalNumber.Substring(0, 3), formattedLocalNumber.Substring(3));
            return value;
        }

        /// <summary>
        /// Formats the Area Code of a telephone number.
        /// </summary>
        /// <param name="value">Area Code</param>
        /// <param name="parentheses">Indicates whether brackets should be around the Area Code.</param>
        /// <param name="removeLeadingZero">Removes any leading zero in the Area Code.</param>
        /// <returns></returns>
        public static string FormatAreaCode(string value, bool parentheses, bool removeLeadingZero = false)
        {
            var digits = removeLeadingZero ? ToDigits(value).Replace("0", string.Empty) : ToDigits(value);
            return parentheses ? string.Format("({0})", digits) : digits;
        }
        /// <summary>
        /// Applies a leading + character to the Country Code if available.
        /// </summary>
        /// <param name="value">Country Code.</param>
        /// <returns>Formatted Country Code.</returns>
        public static string FormatCountryCode(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? string.Empty : string.Format("+{0}", ToDigits(value));
        }

        /// <summary>
        /// Strips non numeric characters from the string.
        /// </summary>
        /// <param name="value">The value to be evaluated.</param>
        /// <returns>Returns any digits that remain in the string.</returns>
        public static string ToDigits(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? string.Empty : Regex.Replace(value, "[^[0-9]", string.Empty);
        }

        /// <summary>
        /// Converts a string representation of a telephone number into a CribTelephoneNumber.
        /// </summary>
        /// <param name="telephoneNumber">Telephone number to parse.</param>
        /// <returns>Returns a CribTelephoneNumber if valid otherwise returns null.</returns>
        public static ICribTelephoneNumber ToLandLineNumber(string telephoneNumber)
        {
            var number = ToDigits(telephoneNumber);
            var cribTelephoneNumber = new CribTelephoneNumber();
            if (string.IsNullOrWhiteSpace(number))
                return cribTelephoneNumber;
            
            if(number.Length == 11)
            {
                cribTelephoneNumber.CountryCode = number.Substring(0, 2);
                cribTelephoneNumber.AreaCode = string.Format("0{0}", number.Substring(2, 1));
                cribTelephoneNumber.LocalNumber = number.Substring(3);
            }
            else if(number.Length == 12)
            {
                cribTelephoneNumber.CountryCode = number.Substring(0, 2);
                cribTelephoneNumber.AreaCode = number.Substring(2, 2);
                cribTelephoneNumber.LocalNumber = number.Substring(4);
            }
            else if(number.Length == 13)
            {
                cribTelephoneNumber.CountryCode = number.Substring(0, 3);
                cribTelephoneNumber.AreaCode = string.Format("0{0}", number.Substring(4, 1));
                cribTelephoneNumber.LocalNumber = number.Substring(5);
            }
            else if(number.Length == 10)
            {
                cribTelephoneNumber.AreaCode = number.Substring(0, number.Length - 8);    
                cribTelephoneNumber.LocalNumber = number.Right(8);
            }
            else 
            {
                cribTelephoneNumber.LocalNumber = number;
            }

            return cribTelephoneNumber;
        }

        /// <summary>
        /// Converts a string representation of a telephone number into a CribTelephoneNumber.
        /// </summary>
        /// <param name="telephoneNumber">Telephone number to parse.</param>
        /// <returns>Returns a CribTelephoneNumber if valid otherwise returns null.</returns>
        public static ICribTelephoneNumber ToMobileNumber(string telephoneNumber)
        {
            var number = ToDigits(telephoneNumber);
            var cribTelephoneNumber = new CribTelephoneNumber() { IsMobileNumber = true };
            if (string.IsNullOrWhiteSpace(number))
                return cribTelephoneNumber;
           
            if(number.Length > 10)
            {
                cribTelephoneNumber.LocalNumber = string.Format("0{0}", number.Right(9));
                cribTelephoneNumber.CountryCode = number.Substring(0, number.Length - 9);
            }
            else
            {
                cribTelephoneNumber.LocalNumber = number;
            }

            return cribTelephoneNumber;
        }
    }
}
