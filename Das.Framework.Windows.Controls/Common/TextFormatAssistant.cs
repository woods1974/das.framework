﻿using Das.Framework.Core.Extensions;

namespace Das.Framework.Windows.Controls.Common
{
    public static class TextFormatAssistant
    {
        public static string ToCapitalisation(Capitalisation capitalisation, string text)
        {
            switch (capitalisation)
            {
                case Capitalisation.LowerCase:
                    return text.ToLower();
                case Capitalisation.UpperCase:
                    return text.ToUpper();
                case Capitalisation.ProperCase:
                    return text.ToProperCase();
            }
            return text;
        }
    }
}
