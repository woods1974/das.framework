﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribAddressInFormInput : DasUserControl, ICribAddressInFormInput
    {

        public CribAddressInFormInput()
        {
            InitializeComponent();
        }
       
        #region Implementation of ICribAddress

        /// <summary>
        /// Address Line 1
        /// </summary>
        [Bindable(true)]
        public string AddressLine1
        {
            get { return line1TextBox.Text; }
            set { line1TextBox.Text = value; }
        }
        
        /// <summary>
        /// Address Line 2
        /// </summary>
        [Bindable(true)]
        public string AddressLine2
        {
            get { return line2TextBox.Text; }
            set { line2TextBox.Text = value; }
        }

        /// <summary>
        /// Address Line 3
        /// </summary>
        [Bindable(true)]
        public string AddressLine3
        {
            get {return line3TextBox.Text;}
            set { line3TextBox.Text = value; }
        }

        /// <summary>
        /// Suburb or town
        /// </summary>
        [Bindable(true)]
        public string SuburbTown
        {
            get { return townSuburbTextBox.Text; }
            set { townSuburbTextBox.Text = value; }
        }

        /// <summary>
        /// State
        /// </summary>
        public string State
        {
            get
            {
                return SelectedState != null ? SelectedState.Name : string.Empty;
            }
        }

        /// <summary>
        /// Country
        /// </summary>
        public string Country
        {
            get
            {
                return SelectedCountry != null ? SelectedCountry.Name : string.Empty;
            }
        }

        /// <summary>
        /// Postcode or zip code
        /// </summary>
        [Bindable(true)]
        public string Postcode
        {
            get { return postcodeTextBox.Text; }
            set { postcodeTextBox.Text = value; }
        }

        #endregion

        #region Implementation of ICribAddressInFormInput

        /// <summary>
        /// List of available countries
        /// </summary>
        [Bindable(true)]
        public List<ICountry> Countries
        {
            get { return (List<ICountry>)countryComboBox.DataSource; } 
            set
            {
                countryComboBox.DataSource = value;
                countryComboBox.DisplayMember = "Name";
            }
        }

        /// <summary>
        /// Selected country from the list.
        /// </summary>
        [Bindable(true)]
        public ICountry SelectedCountry
        {
            get { return (ICountry)countryComboBox.SelectedItem; }
            set { countryComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// List of available states
        /// </summary>
        [Bindable(true)]
        public List<IState> States
        {
            get { return (List<IState>)stateComboBox.DataSource; } 
            set
            {
                stateComboBox.DataSource = value;
                stateComboBox.DisplayMember = "Name";
            }
        }

        /// <summary>
        /// Selected state from States
        /// </summary>
        [Bindable(true)]
        public IState SelectedState
        {
            get { return (IState)stateComboBox.SelectedItem; }
            set { stateComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Set the address
        /// </summary>
        /// <param name="address"></param>
        public void SetAddress(ICribAddress address)
        {
            if (address == null)
            {
                AddressLine1 = string.Empty;
                AddressLine2 = string.Empty;
                AddressLine3 = string.Empty;
                SelectedState = null;
                SelectedCountry = null;
                Postcode = string.Empty;
                SuburbTown = string.Empty;
                return;
            }

            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            AddressLine3 = address.AddressLine3;
            SelectedState = States != null ? States.FirstOrDefault(s=>s.Name == address.State) : null;
            SelectedCountry = Countries != null ? Countries.FirstOrDefault(c => c.Name == address.Country) : null;
            Postcode = address.Postcode;
            SuburbTown = address.SuburbTown;
        }

        #endregion
        
        [Browsable(false)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }

        #region Designer

        private DasLabel dasLabel1;
        private DasLabel dasLabel2;
        private DasLabel dasLabel3;
        private DasTextBox line1TextBox;
        private DasLabel dasLabel4;
        private DasLabel dasLabel5;
        private DasLabel dasLabel6;
        private DasTextBox line2TextBox;
        private DasTextBox line3TextBox;
        private DasTextBox townSuburbTextBox;
        private DasComboBox countryComboBox;
        private DasComboBox stateComboBox;
        private DasLabel dasLabel7;
        private DasTextBox postcodeTextBox;

        private void InitializeComponent()
        {
            this.dasLabel1 = new Das.Framework.Windows.Controls.DasLabel();
            this.dasLabel2 = new Das.Framework.Windows.Controls.DasLabel();
            this.dasLabel3 = new Das.Framework.Windows.Controls.DasLabel();
            this.line1TextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.dasLabel4 = new Das.Framework.Windows.Controls.DasLabel();
            this.dasLabel5 = new Das.Framework.Windows.Controls.DasLabel();
            this.dasLabel6 = new Das.Framework.Windows.Controls.DasLabel();
            this.line2TextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.line3TextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.townSuburbTextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.countryComboBox = new Das.Framework.Windows.Controls.DasComboBox();
            this.postcodeTextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.stateComboBox = new Das.Framework.Windows.Controls.DasComboBox();
            this.dasLabel7 = new Das.Framework.Windows.Controls.DasLabel();
            this.SuspendLayout();
            // 
            // dasLabel1
            // 
            // 
            // 
            // 
            this.dasLabel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel1.DelegateCommand = null;
            this.dasLabel1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel1.Location = new System.Drawing.Point(55, 2);
            this.dasLabel1.Name = "dasLabel1";
            this.dasLabel1.Size = new System.Drawing.Size(46, 23);
            this.dasLabel1.TabIndex = 0;
            this.dasLabel1.Text = "Line 1";
            this.dasLabel1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // dasLabel2
            // 
            // 
            // 
            // 
            this.dasLabel2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel2.DelegateCommand = null;
            this.dasLabel2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel2.Location = new System.Drawing.Point(55, 34);
            this.dasLabel2.Name = "dasLabel2";
            this.dasLabel2.Size = new System.Drawing.Size(46, 23);
            this.dasLabel2.TabIndex = 1;
            this.dasLabel2.Text = "Line 2";
            this.dasLabel2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // dasLabel3
            // 
            // 
            // 
            // 
            this.dasLabel3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel3.DelegateCommand = null;
            this.dasLabel3.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel3.Location = new System.Drawing.Point(55, 66);
            this.dasLabel3.Name = "dasLabel3";
            this.dasLabel3.Size = new System.Drawing.Size(46, 23);
            this.dasLabel3.TabIndex = 2;
            this.dasLabel3.Text = "Line 3";
            this.dasLabel3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // line1TextBox
            // 
            this.line1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line1TextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.line1TextBox.Border.Class = "TextBoxBorder";
            this.line1TextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.line1TextBox.Capitalisation = Das.Framework.Windows.Controls.Capitalisation.ProperCase;
            this.line1TextBox.DelegateCommand = null;
            this.line1TextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.line1TextBox.ForeColor = System.Drawing.Color.Black;
            this.line1TextBox.Location = new System.Drawing.Point(115, 0);
            this.line1TextBox.Name = "line1TextBox";
            this.line1TextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.line1TextBox.RegularExpression = null;
            this.line1TextBox.Size = new System.Drawing.Size(168, 26);
            this.line1TextBox.TabIndex = 0;
            // 
            // dasLabel4
            // 
            // 
            // 
            // 
            this.dasLabel4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel4.DelegateCommand = null;
            this.dasLabel4.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel4.Location = new System.Drawing.Point(5, 98);
            this.dasLabel4.Name = "dasLabel4";
            this.dasLabel4.Size = new System.Drawing.Size(96, 23);
            this.dasLabel4.TabIndex = 4;
            this.dasLabel4.Text = "Town / Suburb";
            this.dasLabel4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // dasLabel5
            // 
            // 
            // 
            // 
            this.dasLabel5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel5.DelegateCommand = null;
            this.dasLabel5.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel5.Location = new System.Drawing.Point(5, 130);
            this.dasLabel5.Name = "dasLabel5";
            this.dasLabel5.Size = new System.Drawing.Size(96, 23);
            this.dasLabel5.TabIndex = 5;
            this.dasLabel5.Text = "Country";
            this.dasLabel5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // dasLabel6
            // 
            // 
            // 
            // 
            this.dasLabel6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel6.DelegateCommand = null;
            this.dasLabel6.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel6.Location = new System.Drawing.Point(5, 194);
            this.dasLabel6.Name = "dasLabel6";
            this.dasLabel6.Size = new System.Drawing.Size(96, 23);
            this.dasLabel6.TabIndex = 6;
            this.dasLabel6.Text = "Postcode";
            this.dasLabel6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // line2TextBox
            // 
            this.line2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line2TextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.line2TextBox.Border.Class = "TextBoxBorder";
            this.line2TextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.line2TextBox.Capitalisation = Das.Framework.Windows.Controls.Capitalisation.ProperCase;
            this.line2TextBox.DelegateCommand = null;
            this.line2TextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.line2TextBox.ForeColor = System.Drawing.Color.Black;
            this.line2TextBox.Location = new System.Drawing.Point(115, 32);
            this.line2TextBox.Name = "line2TextBox";
            this.line2TextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.line2TextBox.RegularExpression = null;
            this.line2TextBox.Size = new System.Drawing.Size(168, 26);
            this.line2TextBox.TabIndex = 1;
            // 
            // line3TextBox
            // 
            this.line3TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line3TextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.line3TextBox.Border.Class = "TextBoxBorder";
            this.line3TextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.line3TextBox.Capitalisation = Das.Framework.Windows.Controls.Capitalisation.ProperCase;
            this.line3TextBox.DelegateCommand = null;
            this.line3TextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.line3TextBox.ForeColor = System.Drawing.Color.Black;
            this.line3TextBox.Location = new System.Drawing.Point(115, 64);
            this.line3TextBox.Name = "line3TextBox";
            this.line3TextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.line3TextBox.RegularExpression = null;
            this.line3TextBox.Size = new System.Drawing.Size(168, 26);
            this.line3TextBox.TabIndex = 2;
            // 
            // townSuburbTextBox
            // 
            this.townSuburbTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.townSuburbTextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.townSuburbTextBox.Border.Class = "TextBoxBorder";
            this.townSuburbTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.townSuburbTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.townSuburbTextBox.DelegateCommand = null;
            this.townSuburbTextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.townSuburbTextBox.ForeColor = System.Drawing.Color.Black;
            this.townSuburbTextBox.Location = new System.Drawing.Point(115, 96);
            this.townSuburbTextBox.Name = "townSuburbTextBox";
            this.townSuburbTextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.townSuburbTextBox.RegularExpression = null;
            this.townSuburbTextBox.Size = new System.Drawing.Size(168, 26);
            this.townSuburbTextBox.TabIndex = 3;
            // 
            // countryComboBox
            // 
            this.countryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.countryComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.countryComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.countryComboBox.DelegateCommand = null;
            this.countryComboBox.DisplayMember = "Text";
            this.countryComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.countryComboBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.countryComboBox.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Medium;
            this.countryComboBox.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Regular;
            this.countryComboBox.FormattingEnabled = true;
            this.countryComboBox.ItemHeight = 20;
            this.countryComboBox.Location = new System.Drawing.Point(116, 128);
            this.countryComboBox.Name = "countryComboBox";
            this.countryComboBox.Size = new System.Drawing.Size(167, 26);
            this.countryComboBox.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.countryComboBox.TabIndex = 4;
            // 
            // postcodeTextBox
            // 
            this.postcodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.postcodeTextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.postcodeTextBox.Border.Class = "TextBoxBorder";
            this.postcodeTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.postcodeTextBox.DelegateCommand = null;
            this.postcodeTextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.postcodeTextBox.ForeColor = System.Drawing.Color.Black;
            this.postcodeTextBox.Location = new System.Drawing.Point(116, 192);
            this.postcodeTextBox.Name = "postcodeTextBox";
            this.postcodeTextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.postcodeTextBox.RegularExpression = null;
            this.postcodeTextBox.Size = new System.Drawing.Size(108, 26);
            this.postcodeTextBox.TabIndex = 6;
            // 
            // stateComboBox
            // 
            this.stateComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stateComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.stateComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.stateComboBox.DelegateCommand = null;
            this.stateComboBox.DisplayMember = "Text";
            this.stateComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.stateComboBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.stateComboBox.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Medium;
            this.stateComboBox.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Regular;
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.ItemHeight = 20;
            this.stateComboBox.Location = new System.Drawing.Point(116, 160);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(167, 26);
            this.stateComboBox.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.stateComboBox.TabIndex = 5;
            // 
            // dasLabel7
            // 
            // 
            // 
            // 
            this.dasLabel7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dasLabel7.DelegateCommand = null;
            this.dasLabel7.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dasLabel7.Location = new System.Drawing.Point(5, 162);
            this.dasLabel7.Name = "dasLabel7";
            this.dasLabel7.Size = new System.Drawing.Size(96, 23);
            this.dasLabel7.TabIndex = 8;
            this.dasLabel7.Text = "State";
            this.dasLabel7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // CribAddressInFormInput
            // 
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.dasLabel7);
            this.Controls.Add(this.postcodeTextBox);
            this.Controls.Add(this.countryComboBox);
            this.Controls.Add(this.townSuburbTextBox);
            this.Controls.Add(this.line3TextBox);
            this.Controls.Add(this.line2TextBox);
            this.Controls.Add(this.dasLabel6);
            this.Controls.Add(this.dasLabel5);
            this.Controls.Add(this.dasLabel4);
            this.Controls.Add(this.line1TextBox);
            this.Controls.Add(this.dasLabel3);
            this.Controls.Add(this.dasLabel2);
            this.Controls.Add(this.dasLabel1);
            this.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.MinimumSize = new System.Drawing.Size(218, 186);
            this.Name = "CribAddressInFormInput";
            this.OverrideAppearance = true;
            this.Size = new System.Drawing.Size(283, 236);
            this.ResumeLayout(false);

        }

        #endregion
       
    }
}
