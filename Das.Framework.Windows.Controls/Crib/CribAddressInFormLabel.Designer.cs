﻿namespace Das.Framework.Windows.Controls.Crib
{
    partial class CribAddressInFormLabel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addressLine1Label = new Das.Framework.Windows.Controls.DasLabel();
            this.addressLine2Label = new Das.Framework.Windows.Controls.DasLabel();
            this.addressLine3Label = new Das.Framework.Windows.Controls.DasLabel();
            this.suburbTownLabel = new Das.Framework.Windows.Controls.DasLabel();
            this.countryLabel = new Das.Framework.Windows.Controls.DasLabel();
            this.postcodeLabel = new Das.Framework.Windows.Controls.DasLabel();
            this.stateLabel = new Das.Framework.Windows.Controls.DasLabel();
            this.SuspendLayout();
            // 
            // addressLine1Label
            // 
            this.addressLine1Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.addressLine1Label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.addressLine1Label.DelegateCommand = null;
            this.addressLine1Label.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.addressLine1Label.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.addressLine1Label.Location = new System.Drawing.Point(3, 0);
            this.addressLine1Label.Name = "addressLine1Label";
            this.addressLine1Label.Size = new System.Drawing.Size(90, 16);
            this.addressLine1Label.TabIndex = 0;
            this.addressLine1Label.Text = "Address Line 1";
            this.addressLine1Label.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // addressLine2Label
            // 
            this.addressLine2Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.addressLine2Label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.addressLine2Label.DelegateCommand = null;
            this.addressLine2Label.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.addressLine2Label.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.addressLine2Label.Location = new System.Drawing.Point(3, 20);
            this.addressLine2Label.Name = "addressLine2Label";
            this.addressLine2Label.Size = new System.Drawing.Size(90, 16);
            this.addressLine2Label.TabIndex = 1;
            this.addressLine2Label.Text = "Address Line 2";
            this.addressLine2Label.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // addressLine3Label
            // 
            this.addressLine3Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.addressLine3Label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.addressLine3Label.DelegateCommand = null;
            this.addressLine3Label.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.addressLine3Label.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.addressLine3Label.Location = new System.Drawing.Point(3, 42);
            this.addressLine3Label.Name = "addressLine3Label";
            this.addressLine3Label.Size = new System.Drawing.Size(90, 16);
            this.addressLine3Label.TabIndex = 2;
            this.addressLine3Label.Text = "Address Line 3";
            this.addressLine3Label.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // suburbTownLabel
            // 
            this.suburbTownLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.suburbTownLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.suburbTownLabel.DelegateCommand = null;
            this.suburbTownLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.suburbTownLabel.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.suburbTownLabel.Location = new System.Drawing.Point(3, 64);
            this.suburbTownLabel.Name = "suburbTownLabel";
            this.suburbTownLabel.Size = new System.Drawing.Size(90, 16);
            this.suburbTownLabel.TabIndex = 3;
            this.suburbTownLabel.Text = "Suburb / Town";
            this.suburbTownLabel.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // countryLabel
            // 
            this.countryLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.countryLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.countryLabel.DelegateCommand = null;
            this.countryLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.countryLabel.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.countryLabel.Location = new System.Drawing.Point(3, 107);
            this.countryLabel.Name = "countryLabel";
            this.countryLabel.Size = new System.Drawing.Size(90, 16);
            this.countryLabel.TabIndex = 4;
            this.countryLabel.Text = "Country";
            this.countryLabel.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // postcodeLabel
            // 
            this.postcodeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.postcodeLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.postcodeLabel.DelegateCommand = null;
            this.postcodeLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.postcodeLabel.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.postcodeLabel.Location = new System.Drawing.Point(3, 129);
            this.postcodeLabel.Name = "postcodeLabel";
            this.postcodeLabel.Size = new System.Drawing.Size(90, 16);
            this.postcodeLabel.TabIndex = 5;
            this.postcodeLabel.Text = "Postcode";
            this.postcodeLabel.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // stateLabel
            // 
            this.stateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.stateLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.stateLabel.DelegateCommand = null;
            this.stateLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.stateLabel.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.stateLabel.Location = new System.Drawing.Point(3, 86);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(90, 16);
            this.stateLabel.TabIndex = 6;
            this.stateLabel.Text = "State";
            this.stateLabel.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // CribAddressInFormLabel
            // 
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.postcodeLabel);
            this.Controls.Add(this.countryLabel);
            this.Controls.Add(this.suburbTownLabel);
            this.Controls.Add(this.addressLine3Label);
            this.Controls.Add(this.addressLine2Label);
            this.Controls.Add(this.addressLine1Label);
            this.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.FontWeight = Das.Framework.Windows.Controls.Fonts.DasFontWeight.Light;
            this.Name = "CribAddressInFormLabel";
            this.OverrideAppearance = true;
            this.Size = new System.Drawing.Size(100, 148);
            this.ResumeLayout(false);

        }

        #endregion

        private DasLabel addressLine1Label;
        private DasLabel addressLine2Label;
        private DasLabel addressLine3Label;
        private DasLabel suburbTownLabel;
        private DasLabel countryLabel;
        private DasLabel postcodeLabel;
        private DasLabel stateLabel;

    }
}
