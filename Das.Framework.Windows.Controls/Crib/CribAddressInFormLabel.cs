﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Crib
{
    public partial class CribAddressInFormLabel : DasUserControl, ICribAddressInForm
    {
        private ICribAddress _address;

        public CribAddressInFormLabel()
        {
            InitializeComponent();
            UpdateAddress();
        }

        #region Implementation of ICribAddressInFormLabel

        /// <summary>
        /// Current Address that has been initialised.
        /// </summary>
        public ICribAddress Address
        {
            get {return _address;}
            set
            {
                _address = value;
                UpdateAddress();
            }
        }

        #endregion

        [Browsable(false)]
        [DefaultValue(true)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }

        private void UpdateAddress()
        {
            if (DesignMode)
            {
                addressLine1Label.Text = CribDesignerDefaults.Address.AddressLine1;
                addressLine2Label.Text = CribDesignerDefaults.Address.AddressLine2;
                addressLine3Label.Text = CribDesignerDefaults.Address.AddressLine3;
                suburbTownLabel.Text = CribDesignerDefaults.Address.SuburbTown;
                postcodeLabel.Text = CribDesignerDefaults.Address.Postcode;
                countryLabel.Text = CribDesignerDefaults.Address.Country;
                stateLabel.Text = CribDesignerDefaults.Address.State;
                return;
            }
            ClearAddress();
            if (Address == null) return;
            var addressList = new List<string>
                                  {
                                      Address.AddressLine1,
                                      Address.AddressLine2,
                                      Address.AddressLine3,
                                      Address.SuburbTown,
                                      Address.State,
                                      Address.Country,
                                      Address.Postcode
                                  };
            
            foreach (var line in addressList.Where(item=> !string.IsNullOrWhiteSpace(item)))
            {
                if (IsMissingAddressLine(addressLine1Label.Text))
                {
                    addressLine1Label.Text = line;
                    continue;
                }
                if (IsMissingAddressLine(addressLine2Label.Text))
                {
                    addressLine2Label.Text = line;
                    continue;
                }  
                if(IsMissingAddressLine(addressLine3Label.Text))
                {
                    addressLine3Label.Text = line;
                    continue;
                }
                if (IsMissingAddressLine(suburbTownLabel.Text))
                {
                    suburbTownLabel.Text = line;
                    continue;
                }
                if (IsMissingAddressLine(stateLabel.Text))
                {
                    stateLabel.Text = line;
                    continue;
                }
                if (IsMissingAddressLine(countryLabel.Text))
                {
                    countryLabel.Text = line;
                    continue;
                }
                if (IsMissingAddressLine(postcodeLabel.Text))
                {
                    postcodeLabel.Text = line;
                }
            }

            Invalidate(true);
        }

        private void ClearAddress()
        {
            addressLine1Label.Text = string.Empty;
            addressLine2Label.Text = string.Empty;
            addressLine3Label.Text = string.Empty;
            suburbTownLabel.Text = string.Empty;
            postcodeLabel.Text = string.Empty;
            countryLabel.Text = string.Empty;
            stateLabel.Text = string.Empty;
        }

        private static bool IsMissingAddressLine(string line)
        {
            return string.IsNullOrWhiteSpace(line);
        }
    }
}
