﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribAddressInLineLabel : DasLabel, ICribAddressInLine
    {
        private ICribAddress _address;

        public CribAddressInLineLabel()
        {
            UpdateAddress();
        }

        #region Implementation of ICribAddressInLine

        /// <summary>
        /// Current Address that has been initialised.
        /// </summary>
        public ICribAddress Address
        {
            get { return _address; }
            set
            {
                _address = value;
                UpdateAddress();
            }
        }

        #endregion

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        private void UpdateAddress()
        {
            if (DesignMode)
            {
                _address = new CribAddress()
                              {
                                  AddressLine1 = CribDesignerDefaults.Address.AddressLine1,
                                  AddressLine2 = CribDesignerDefaults.Address.AddressLine2,
                                  AddressLine3 = CribDesignerDefaults.Address.AddressLine3,
                                  SuburbTown = CribDesignerDefaults.Address.SuburbTown,
                                  Country = CribDesignerDefaults.Address.Country,
                                  Postcode = CribDesignerDefaults.Address.Postcode,
                                  State = CribDesignerDefaults.Address.State
                              };
            }

            if (Address == null)
                Text = string.Empty;
            else
            {
                var addressList = new List<string>
                                      {
                                          Address.AddressLine1,
                                          Address.AddressLine2,
                                          Address.AddressLine3,
                                          Address.SuburbTown,
                                          Address.State,
                                          Address.Country,
                                          Address.Postcode
                                      };

                var addressLine = new StringBuilder();
                foreach (var line in addressList.Where(line => !string.IsNullOrWhiteSpace(line)))
                    addressLine.Append(", ").Append(line);

                Text = addressLine.ToString().TrimStart(new char[] {Convert.ToChar(","), Convert.ToChar(" ")});
            }


            Invalidate();
        }
    }
}
