﻿using System;
using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribDateLabel : DasLabel
    {
        private DateTime _dateTime;
        private DateFormat _dateFormat = DateFormat.ShortDate;

        public CribDateLabel()
        {
            _dateTime = DateTime.Now.Date;
            _dateFormat = DateFormat.ShortDate;
            UpdateLabel();
        }
        
        /// <summary>
        /// Date to be displayed.
        /// </summary>
        [Browsable(true)]
        public DateTime Date
        {
            get { return _dateTime; }
            set
            {
                _dateTime = value;
                UpdateLabel();
            }
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        /// <summary>
        /// Represents the date display layout.
        /// </summary>
        [Browsable(true)]
        public DateFormat DateFormat
        {
            get { return _dateFormat; } 
            set
            {
                _dateFormat = value;
                UpdateLabel();
            }
        }

        private void UpdateLabel()
        {
            Text = Date.ToString(DateFormat == DateFormat.ShortDate ? 
                                DateTimeAssistant.ShortDate : DateTimeAssistant.LongDate);
            Invalidate();
        }
    }
}
