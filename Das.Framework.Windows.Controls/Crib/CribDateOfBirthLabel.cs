﻿using System;
using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribDateOfBirthLabel : DasLabel
    {
        private DateTime _fromDate;
        private DateTime _toDate;

        public CribDateOfBirthLabel()
        {
            _fromDate = DateTime.Now.AddYears(-2).Date;
            _toDate = DateTime.Now.Date;
            UpdateLabel();
        }

        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Data)]
        public DateTime FromDate
        {
            get { return _fromDate; } 
            set
            {
                _fromDate = value;
                UpdateLabel();
            }
        }

        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Data)]
        public DateTime ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                UpdateLabel();
            }
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
        
        private void UpdateLabel()
        {
            Text = string.Format("{0} ({1})", FromDate.ToString(DateTimeAssistant.ShortDate), 
                                    DateTimeAssistant.GetDisplayAge(this.FromDate, this.ToDate));
            Invalidate();
        }
    }
}
