﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Core.Drawing;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribDatePicker : DasUserControl
    {
        private ToolStripDropDown _popupWindow;
        private MonthCalendar _calendar;
        private CribDateTextBox _dateTextBox;

        public CribDatePicker()
        {
            InitializeComponent();
            InitialiseControl();
        }

        private void InitialiseControl()
        {
            _calendar = new MonthCalendar(){MaxSelectionCount = 1};
            var host = new ToolStripControlHost(_calendar);
            _popupWindow = new ToolStripDropDown();
            _popupWindow.Items.Add(host);
            Date = DateTime.Now.Date;
            _dateTextBox.MaxLength = 11;
            Image = ImageAssistant.GetResizedImage(Image, new Rectangle(0, 0, 20, 20));
            SubscribeToEvents();
            base.Initialise();
        }

        private void UnsubscribeToEvents()
        {
            _calendar.DateSelected -= DateSelectedEvent;
            _dateTextBox.ButtonCustomClick -= CalendarButtonClickEvent;
            
        }

        private void SubscribeToEvents()
        {
            _calendar.DateSelected += DateSelectedEvent;
            _dateTextBox.ButtonCustomClick += CalendarButtonClickEvent;
        }

        private void CalendarButtonClickEvent(object sender, EventArgs e)
        {
            ShowCalendar();
        }

        private void DateSelectedEvent(object sender, DateRangeEventArgs e)
        {
            Date = e.Start.Date;
            _popupWindow.Hide();
        }

        private void ShowCalendar()
        {
            _calendar.SetDate(Date);
            _popupWindow.Show(_dateTextBox, 0, _dateTextBox.Height);
        }
        
        public event DateRangeEventHandler DateSelected
        {
            add { _calendar.DateSelected += value; }
            remove { _calendar.DateSelected -= value; }
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public Image Image
        {
            get { return _dateTextBox.ButtonCustom.Image; }
            set { _dateTextBox.ButtonCustom.Image = value; }
        }

        [Browsable(true)]
        public DateTime Date
        {
            get { return _dateTextBox.Date; }
            set { _dateTextBox.Date = value; }
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
                UnsubscribeToEvents();

            base.Dispose(disposing);
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        [Browsable(false)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }
        
        #region Designer


        private void InitializeComponent()
        {
            this._dateTextBox = new Das.Framework.Windows.Controls.Crib.CribDateTextBox();
            this.SuspendLayout();
            // 
            // _dateTextBox
            // 
            this._dateTextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._dateTextBox.Border.Class = "TextBoxBorder";
            this._dateTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._dateTextBox.ButtonCustom.Image = global::Das.Framework.Windows.Controls.Properties.Resources.calendar;
            this._dateTextBox.ButtonCustom.Visible = true;
            this._dateTextBox.Date = new System.DateTime(2013, 8, 12, 19, 1, 24, 505);
            this._dateTextBox.DelegateCommand = null;
            this._dateTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dateTextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this._dateTextBox.ForeColor = System.Drawing.Color.Black;
            this._dateTextBox.Location = new System.Drawing.Point(0, 0);
            this._dateTextBox.MaxLength = 12;
            this._dateTextBox.Name = "_dateTextBox";
            this._dateTextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this._dateTextBox.RegularExpression = null;
            this._dateTextBox.Size = new System.Drawing.Size(120, 30);
            this._dateTextBox.TabIndex = 0;
            this._dateTextBox.Text = "12-Aug-2013";
            // 
            // CribDatePicker
            // 
            this.Controls.Add(this._dateTextBox);
            this.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.MinimumSize = new System.Drawing.Size(120, 26);
            this.Name = "CribDatePicker";
            this.OverrideAppearance = true;
            this.Size = new System.Drawing.Size(120, 26);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
