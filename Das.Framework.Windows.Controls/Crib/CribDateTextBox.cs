﻿using System;
using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribDateTextBox : DasTextBox
    {
        private DateTime _date;

        public CribDateTextBox()
        {
            InitialiseTextBox();
        }
        
        private void InitialiseTextBox()
        {
            Date = DateTime.Now.Date;
            MaxLength = 11;
            SubscribeToEvents();
            UpdateTextBox();
        }

        private void UnsubscribeToEvents()
        {
            LostFocus -= DateTextBoxLostFocusEvent;
        }
        
        private void SubscribeToEvents()
        {
            LostFocus += DateTextBoxLostFocusEvent;
        }
        
        private void DateTextBoxLostFocusEvent(object sender, EventArgs e)
        {
            SetDate();
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        [Browsable(true)]
        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    UpdateTextBox();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                UnsubscribeToEvents();
            base.Dispose(disposing);
        }

        private void SetDate()
        {
            DateTime date;
            IsDateValid = DateTime.TryParse(Text, out date);
            if (IsDateValid)
            {
                _date = date;
                UpdateTextBox();
            }
            else
            {
                Text = string.Empty;
            }
        }

        /// <summary>
        /// Determines whether the data entered is valid.
        /// </summary>
        [Browsable(false)]
        public bool IsDateValid { get; private set; }

        private void UpdateTextBox()
        {
            Text = Date.ToString(DateTimeAssistant.ShortDate);
        }
    }
}
