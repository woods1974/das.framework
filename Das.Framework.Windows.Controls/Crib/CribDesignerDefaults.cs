﻿namespace Das.Framework.Windows.Controls.Crib
{
    public class CribDesignerDefaults
    {
        public static class Address
        {
             public const string AddressLine1 = "Address Line 1";
             public const string AddressLine2 = "Address Line 2";
             public const string AddressLine3 = "Address Line 3";
             public const string SuburbTown = "Suburb/Town";
             public const string Country = "Country";
             public const string Postcode = "Postcode";
             public const string State = "State";
        }
    }
}
