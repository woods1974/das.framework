﻿using System.ComponentModel;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribGenderLabel : DasLabel
    {
        private Gender _gender;

        public CribGenderLabel()
        {
            _gender = Gender.Unknown;
            UpdateLabel();
        }

        [Browsable(true)]
        [DefaultValue(Gender.Unknown)]
        [Category(DasPropertySettings.PropertyCategory.Data)]
        public Gender Gender
        {
            get { return _gender; } 
            set
            {
                _gender = value;
                UpdateLabel();
            }
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        private void UpdateLabel()
        {
            Text = _gender.ToString();
            Invalidate();
        }
    }
}
