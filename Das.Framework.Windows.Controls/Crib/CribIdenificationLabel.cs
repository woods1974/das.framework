﻿using System.ComponentModel;
using System.Globalization;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribIdenificationLabel : DasLabel
    {
        private int _id;

        public CribIdenificationLabel()
        {
            UpdateLabel();
        }
        
        [Category(DasPropertySettings.PropertyCategory.Data)]
        [DefaultValue(123456)]
        [Browsable(true)]
        public int Id
        {
            get { return _id; } 
            set
            {
                _id = value;
                UpdateLabel();
            }
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        private void UpdateLabel()
        {
            var format = new NumberFormatInfo
                             {
                                 NumberGroupSeparator = " ",
                                 NumberGroupSizes = new[] {3}, 
                                 NumberDecimalDigits = 0,

                             };
            Text = _id.ToString("n",format);
            Invalidate(true);
        }
    }
}
