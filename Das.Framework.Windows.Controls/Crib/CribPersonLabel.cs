﻿using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribPersonLabel : DasLabel
    {
        public CribPersonLabel()
        {
            Person = new CribPerson()
                          {
                              FamilyName = "Family name",
                              GivenName = "Given Name",
                              Title = "Mr"
                          };
        }

        private ICribPerson _person;

        [Browsable(false)]
        public ICribPerson Person
        {
            get { return _person; } 
            set
            {
                _person = value;
                UpdateLabel();
            }
        }

        protected override void UpdateLabel()
        {
            Text = PersonAssistant.ToPerson(Person);
            base.UpdateLabel();
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
    }
}
