﻿using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribTelephoneNumberLabel : DasLabel
    {
        private ICribTelephoneNumber _cribTelephoneNumber;
        private bool _useBrackets = true;

        public CribTelephoneNumberLabel()
        {
            DesignerSupport();
        }

        public ICribTelephoneNumber TelephoneNumber
        {
            get { return _cribTelephoneNumber; }
            set
            {
                _cribTelephoneNumber = value;
                UpdateLabel();
            }
        }
        
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        [DefaultValue(true)]
        [Browsable(true)]
        public bool BracketsEnabled
        {
            get { return _useBrackets; }
            set
            {
                _useBrackets = value;
                UpdateLabel();
            }
        }

        private void DesignerSupport()
        {
           
            TelephoneNumber = new CribTelephoneNumber()
                {
                    CountryCode = "00",
                    AreaCode = "00",
                    LocalNumber = "00000000",
                    ExtensionNumber = "0000"
                };
        }

        private void UpdateLabel()
        {
            if (_cribTelephoneNumber == null)
                Text = string.Empty;
            else
            {
                if (TelephoneNumber.IsMobileNumber)
                    Text = TelephoneNumberAssistant.ToMobileNumber(TelephoneNumber);
                else if (string.IsNullOrWhiteSpace(TelephoneNumber.ExtensionNumber))
                    Text = TelephoneNumberAssistant.ToLandLineNumber(TelephoneNumber, BracketsEnabled);
                else
                    Text = TelephoneNumberAssistant.ToLandLineNumberWithExtension(TelephoneNumber, BracketsEnabled);
            }
            Invalidate();
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
    }
}
