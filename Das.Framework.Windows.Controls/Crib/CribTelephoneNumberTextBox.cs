using System;
using System.ComponentModel;
using System.Drawing;
using Das.Framework.Windows.Controls.Common;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Crib.Models;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribTelephoneNumberTextBox : DasUserControl, ICribTelephoneNumber
    {
        private DasTextBox telephoneNumber;
        private DasTextBox extensionNumberTextBox;
        private string _countryCode;
        private string _areaCode;
        private string _localNumber;
        private string _extensionNumber;
        private bool _isMobileNumber;
        private bool _extensionNumberEnabled = true;
        private ICribTelephoneNumber _cribTelephoneNumber;

        public CribTelephoneNumberTextBox()
        {
            InitializeComponent();
            Intialise();
        }

        private void Intialise()
        {
            TelephoneNumberMaxLength = 15;
            ExtensionNumberMaxLength = 8;
            telephoneNumber.MaxLength = TelephoneNumberMaxLength;
            extensionNumberTextBox.MaxLength = ExtensionNumberMaxLength;
            telephoneNumber.RegexEnabled = true;
            telephoneNumber.RegularExpression = Core.RegularExperssions.RegularExpressions.Numeric;
            extensionNumberTextBox.RegexEnabled = true;
            extensionNumberTextBox.RegularExpression = Core.RegularExperssions.RegularExpressions.Numeric;
            _cribTelephoneNumber = new CribTelephoneNumber();
            SubscribeToEvents();
            UpdateLayout();
        }

        private void UnsubscribeToEvents()
        {
            telephoneNumber.GotFocus -= TelephoneNumberGotFocusEvent;
            telephoneNumber.LostFocus -= TelephoneNumberLostFocusEvent;
        }

        private void SubscribeToEvents()
        {
            telephoneNumber.GotFocus += TelephoneNumberGotFocusEvent;
            telephoneNumber.LostFocus += TelephoneNumberLostFocusEvent;
        }

        private void UpdateTelephoneNumberProperties()
        {
            _countryCode = _cribTelephoneNumber.CountryCode;
            _areaCode = _cribTelephoneNumber.AreaCode;
            _localNumber = _cribTelephoneNumber.LocalNumber;
            _extensionNumber = _cribTelephoneNumber.ExtensionNumber;
        }
        
        private void UpdateTelephoneNumber()
        {
            _cribTelephoneNumber.CountryCode = CountryCode;
            _cribTelephoneNumber.AreaCode = AreaCode;
            _cribTelephoneNumber.LocalNumber = LocalNumber;
            _cribTelephoneNumber.ExtensionNumber = ExtensionNumber;
            _cribTelephoneNumber.IsMobileNumber = IsMobileNumber;

            telephoneNumber.Text = _cribTelephoneNumber.IsMobileNumber ?
                    TelephoneNumberAssistant.ToMobileNumber(_cribTelephoneNumber) :
                    TelephoneNumberAssistant.ToLandLineNumber(_cribTelephoneNumber);
        }
        
        private void UpdateTelephoneNumberDisplayText()
        {
            if (IsMobileNumber)
            {
                _cribTelephoneNumber = TelephoneNumberAssistant.ToMobileNumber(telephoneNumber.Text);
                telephoneNumber.Text = TelephoneNumberAssistant.ToMobileNumber(_cribTelephoneNumber);
            }
            else
            {
                _cribTelephoneNumber = TelephoneNumberAssistant.ToLandLineNumber(telephoneNumber.Text);
                telephoneNumber.Text = TelephoneNumberAssistant.ToLandLineNumber(_cribTelephoneNumber);
            } 

            UpdateTelephoneNumberProperties();
        }

        private void TelephoneNumberLostFocusEvent(object sender, EventArgs e)
        {
            UpdateTelephoneNumberDisplayText();
        }
      
        private void TelephoneNumberGotFocusEvent(object sender, EventArgs e)
        {
            telephoneNumber.Text = TelephoneNumberAssistant.ToDigits(telephoneNumber.Text);
        }

        #region Implementation of ICribTelephoneNumber

        /// <summary>
        /// Country code.
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        public string CountryCode
        {
            get { return _countryCode; }
            set
            {
                _countryCode = value;
                MaximumSize = new Size(300, 50);
                UpdateLayout();
                UpdateTelephoneNumber();
            }
        }

        /// <summary>
        /// Area code.
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        public string AreaCode
        {
            get { return _areaCode; }
            set
            {
                _areaCode = value;
                UpdateLayout();
                UpdateTelephoneNumber();
            }
        }

        /// <summary>
        /// Local number.
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        public string LocalNumber
        {
            get { return _localNumber; }
 
            set
            {
                _localNumber = value;
                UpdateLayout();
                UpdateTelephoneNumber();
            }
        }

        /// <summary>
        /// Extension number.
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        public string ExtensionNumber
        {
            get { return _extensionNumber; } 
            set
            {
                _extensionNumber = TelephoneNumberAssistant.ToDigits(value);
                extensionNumberTextBox.Text = _extensionNumber;
                UpdateLayout();
            }
        }

        /// <summary>
        /// Indicates if this is a mobile phone number.
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        [DefaultValue(false)]
        public bool IsMobileNumber
        {
            get { return _isMobileNumber; } 
            set
            {
                _isMobileNumber = value;
                UpdateLayout();
                UpdateTelephoneNumber();
            }
        }
        
        #endregion
        
        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        [DefaultValue(8)]
        [Browsable(true)]
        public int TelephoneNumberMaxLength { get; set; }

        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        [DefaultValue(8)]
        [Browsable(true)]
        public int ExtensionNumberMaxLength { get; set; }

        [Category(DasPropertySettings.PropertyCategory.TelephoneNumber)]
        [DefaultValue(true)]
        [Browsable(true)]
        public bool ExtensionNumberEnabled
        {
            get { return _extensionNumberEnabled; }
            set
            {
                _extensionNumberEnabled = value;
                UpdateLayout();
            }
        }

        [Browsable(false)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }

        private void UpdateLayout()
        {
            if (IsMobileNumber)
                SetMobileView();
            else
                SetLandLineView();
            
            Height = telephoneNumber.Height;
            Invalidate(true);
        }

        private void SetLandLineView()
        {
            if(ExtensionNumberEnabled)
            {
                extensionNumberTextBox.Visible = true;
                telephoneNumber.Width = (Width - extensionNumberTextBox.Width) - 10;
            }
            else
            {
                extensionNumberTextBox.Visible = false;
                telephoneNumber.Width = Width;
            }
        }

        private void SetMobileView()
        {
            _extensionNumberEnabled = false;
            telephoneNumber.Width = Width;
            extensionNumberTextBox.Visible = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                UnsubscribeToEvents();
            base.Dispose(disposing);
        }
        
        #region Designer

        private void InitializeComponent()
        {
            this.telephoneNumber = new Das.Framework.Windows.Controls.DasTextBox();
            this.extensionNumberTextBox = new Das.Framework.Windows.Controls.DasTextBox();
            this.SuspendLayout();
            // 
            // telephoneNumber
            // 
            this.telephoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telephoneNumber.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.telephoneNumber.Border.Class = "TextBoxBorder";
            this.telephoneNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.telephoneNumber.DelegateCommand = null;
            this.telephoneNumber.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.telephoneNumber.ForeColor = System.Drawing.Color.Black;
            this.telephoneNumber.Location = new System.Drawing.Point(0, 0);
            this.telephoneNumber.Name = "telephoneNumber";
            this.telephoneNumber.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.telephoneNumber.RegularExpression = null;
            this.telephoneNumber.Size = new System.Drawing.Size(117, 26);
            this.telephoneNumber.TabIndex = 0;
            // 
            // extensionNumberTextBox
            // 
            this.extensionNumberTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.extensionNumberTextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.extensionNumberTextBox.Border.Class = "TextBoxBorder";
            this.extensionNumberTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.extensionNumberTextBox.DelegateCommand = null;
            this.extensionNumberTextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.extensionNumberTextBox.ForeColor = System.Drawing.Color.Black;
            this.extensionNumberTextBox.Location = new System.Drawing.Point(123, 0);
            this.extensionNumberTextBox.Name = "extensionNumberTextBox";
            this.extensionNumberTextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.extensionNumberTextBox.RegularExpression = null;
            this.extensionNumberTextBox.Size = new System.Drawing.Size(57, 26);
            this.extensionNumberTextBox.TabIndex = 1;
            // 
            // CribTelephoneNumberTextBox
            // 
            this.AutoSize = true;
            this.Controls.Add(this.extensionNumberTextBox);
            this.Controls.Add(this.telephoneNumber);
            this.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Name = "CribTelephoneNumberTextBox";
            this.OverrideAppearance = true;
            this.Size = new System.Drawing.Size(180, 26);
            this.ResumeLayout(false);

        }
        
        #endregion
    }
}