﻿namespace Das.Framework.Windows.Controls.Crib
{
    partial class CribTimePicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.timeTextBox = new Das.Framework.Windows.Controls.DasTimeTextBox();
            this.SuspendLayout();
            // 
            // timeTextBox
            // 
            this.timeTextBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.timeTextBox.Border.Class = "TextBoxBorder";
            this.timeTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.timeTextBox.ButtonCustom.Image = global::Das.Framework.Windows.Controls.Properties.Resources.clock;
            this.timeTextBox.ButtonCustom.Visible = true;
            this.timeTextBox.DelegateCommand = null;
            this.timeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timeTextBox.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.timeTextBox.ForeColor = System.Drawing.Color.Black;
            this.timeTextBox.Location = new System.Drawing.Point(0, 0);
            this.timeTextBox.MaxLength = 5;
            this.timeTextBox.Name = "timeTextBox";
            this.timeTextBox.RegexEnabled = true;
            this.timeTextBox.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.timeTextBox.RegularExpression = "[0-9:]";
            this.timeTextBox.Size = new System.Drawing.Size(70, 30);
            this.timeTextBox.TabIndex = 0;
            this.timeTextBox.Text = "06:00";
            this.timeTextBox.Time = System.TimeSpan.Parse("06:00:00");
            this.timeTextBox.WatermarkText = "HH:MM";
            // 
            // CribTimePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.timeTextBox);
            this.MinimumSize = new System.Drawing.Size(70, 26);
            this.Name = "CribTimePicker";
            this.Size = new System.Drawing.Size(70, 26);
            this.ResumeLayout(false);

        }

        #endregion

        private DasTimeTextBox timeTextBox;

    }
}
