﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Core.Drawing;
using DevComponents.Editors.DateTimeAdv;

namespace Das.Framework.Windows.Controls.Crib
{
    public partial class CribTimePicker : DasUserControl
    {
        private ToolStripDropDown _popupWindow;
        private TimeSelector _timeSelector;

        public CribTimePicker()
        {
            InitializeComponent();
            InitialiseControl();
        }

        private void InitialiseControl()
        {
            _timeSelector = new TimeSelector
                                {
                                    SelectedTime = new TimeSpan(0, 6, 0, 0), 
                                    Size = new Size(100,100),                              
                                };
            var host = new ToolStripControlHost(_timeSelector){Width = 200,Height = 200};
            _popupWindow = new ToolStripDropDown {Height = 200, Width = 200};
            _popupWindow.Items.Add(host);
            Image = ImageAssistant.GetResizedImage(Image, new Rectangle(0, 0, 20, 20));
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            timeTextBox.LostFocus += TimeTextBoxLostFocusEvent;
            timeTextBox.ButtonCustomClick += TimeButtonClickEvent;
            _timeSelector.OkClick += OkButtonEvent;
        }

        private void TimeTextBoxLostFocusEvent(object sender, EventArgs e)
        {
            _timeSelector.SelectedTime = Time;
        }

        private void OkButtonEvent(object sender, EventArgs e)
        {
            Time = _timeSelector.SelectedTime;
            _popupWindow.Hide();
        }

        private void UnsubscribeToEvents()
        {
            timeTextBox.LostFocus -= TimeTextBoxLostFocusEvent;
            timeTextBox.ButtonCustomClick -= TimeButtonClickEvent;
            _timeSelector.OkClick -= OkButtonEvent;
        }

        private void TimeButtonClickEvent(object sender, EventArgs e)
        {
            _popupWindow.Show(timeTextBox, 0, timeTextBox.Height);
            _popupWindow.Width = 200;
            _popupWindow.Height = 200;
            _timeSelector.Invalidate();
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public Image Image
        {
            get { return timeTextBox.ButtonCustom.Image; }
            set { timeTextBox.ButtonCustom.Image = value; }
        }
        
        [Browsable(false)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                UnsubscribeToEvents();

            base.Dispose(disposing);
        }

         [Browsable(true)]
        public TimeSpan Time
        {
            get { return timeTextBox.Time; }
            set
            {
                timeTextBox.Time = value;
                _timeSelector.SelectedTime = value;
            }
        }
    }
}

