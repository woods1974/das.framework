﻿using System.ComponentModel;
using System.Drawing;
using Das.Framework.Windows.Controls.Crib.Interfaces;
using Das.Framework.Windows.Controls.Fonts;

namespace Das.Framework.Windows.Controls.Crib
{
    public class CribTitle : DasUserControl, ICribTitle
    {
      public CribTitle()
      {
          InitializeComponent();
          Initialise();
      }

     private void Initialise()
     {
         OverrideAppearance = false;
         FontWeight = DasFontWeight.Regular;
         FontSize = DasFontSize.Medium;
     }
        
      #region Implementation of ICribTitle

        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public string Title
        {
            get { return titleLabel.Text; }
            set { titleLabel.Text = value; }
        }

        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public Color LineColor
        {
            get { return line.ForeColor; }
            set { line.ForeColor = value; }
        }

        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        [Browsable(true)]
        public int LineThickness
        {
            get { return line.Thickness; }
            set { line.Thickness = value; }
        }

        #endregion

        [Browsable(true)]
        public override DasFontSize FontSize
        {
            get
            {
                return base.FontSize;
            }
            set
            {
                base.FontSize = value;
                if (titleLabel != null)
                    titleLabel.FontSize = value;
            }
        }
        [Browsable(true)]
        public override DasFontWeight FontWeight
        {
            get
            {
                return base.FontWeight;
            }
            set
            {
                base.FontWeight = value;
                if (titleLabel != null)
                    titleLabel.FontWeight = value;
            }
        }

        [Browsable(false)]
        public override bool OverrideAppearance
        {
            get
            {
                return base.OverrideAppearance;
            }
            set
            {
                base.OverrideAppearance = value;
            }
        }

      #region Designer

        private DevComponents.DotNetBar.Controls.Line line;
        private DasLabel titleLabel;
        
        private void InitializeComponent()
        {
            this.titleLabel = new Das.Framework.Windows.Controls.DasLabel();
            this.line = new DevComponents.DotNetBar.Controls.Line();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            // 
            // 
            // 
            this.titleLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.titleLabel.DelegateCommand = null;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.titleLabel.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Tall;
            this.titleLabel.Location = new System.Drawing.Point(0, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(280, 23);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Title";
            // 
            // line
            // 
            this.line.Dock = System.Windows.Forms.DockStyle.Fill;
            this.line.ForeColor = System.Drawing.SystemColors.ControlText;
            this.line.Location = new System.Drawing.Point(0, 23);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(280, 10);
            this.line.TabIndex = 1;
            // 
            // CribTitle
            // 
            this.Controls.Add(this.line);
            this.Controls.Add(this.titleLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Tall;
            this.Name = "CribTitle";
            this.OverrideAppearance = true;
            this.Size = new System.Drawing.Size(280, 33);
            this.ResumeLayout(false);

        }

        #endregion

        #region Implementation of ITextFormat

        /// <summary>
        /// Set or get the Capitalisation of the text.
        /// </summary>
        [Browsable(true), DefaultValue(Windows.Controls.Capitalisation.Default),
        Category(DasPropertySettings.PropertyCategory.Behaviour)]
        public Capitalisation Capitalisation
        {
            get { return titleLabel.Capitalisation; }
            set { titleLabel.Capitalisation = value; }
        }



        #endregion
    }
}
