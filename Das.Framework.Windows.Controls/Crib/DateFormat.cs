namespace Das.Framework.Windows.Controls.Crib
{
    public enum DateFormat
    {
        ShortDate,
        LongDate
    }
}