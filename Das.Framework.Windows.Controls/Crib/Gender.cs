namespace Das.Framework.Windows.Controls.Crib
{
    public enum Gender
    {
        Unknown,
        Male,
        Female
    }
}