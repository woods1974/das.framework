﻿namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    public interface ICountry
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
