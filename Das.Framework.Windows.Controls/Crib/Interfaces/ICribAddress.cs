﻿namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    /// <summary>
    /// Interface for the Crib Address
    /// </summary>
    public interface ICribAddress
    {
        /// <summary>
        /// Address Line 1
        /// </summary>
        string AddressLine1 { get; }
        /// <summary>
        /// Address Line 2
        /// </summary>
        string AddressLine2 { get; }
        /// <summary>
        /// Address Line 3
        /// </summary>
        string AddressLine3 { get; }
        /// <summary>
        /// Suburb or town
        /// </summary>
        string SuburbTown { get; }

        /// <summary>
        /// State
        /// </summary>
        string State { get; }

        /// <summary>
        /// Country
        /// </summary>
        string Country { get; }

        /// <summary>
        /// Postcode or zip code
        /// </summary>
        string Postcode { get; }
    }
}
