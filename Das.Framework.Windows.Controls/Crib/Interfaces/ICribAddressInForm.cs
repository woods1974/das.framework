namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    /// <summary>
    /// Crib Address In Form
    /// </summary>
    public interface ICribAddressInForm
    {
        /// <summary>
        /// Current Address that has been initialised.
        /// </summary>
        ICribAddress Address { get; set; }
    }
}