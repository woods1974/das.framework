﻿using System.Collections.Generic;

namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    public interface ICribAddressInFormInput : ICribAddress
    {
        /// <summary>
        /// List of available countries
        /// </summary>
        List<ICountry> Countries { get; set; }
        /// <summary>
        /// Selected country from the list of countries.
        /// </summary>
        ICountry SelectedCountry { get; set; }
        /// <summary>
        /// List of available states
        /// </summary>
        List<IState> States { get; set; }
        /// <summary>
        /// Selected state from States
        /// </summary>
        IState SelectedState { get; set; }
        /// <summary>
        /// Set the address
        /// </summary>
        /// <param name="address"></param>
        void SetAddress(ICribAddress address);
    }
}
