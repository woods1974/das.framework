namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    /// <summary>
    /// Crib Address In Line
    /// </summary>
    public interface ICribAddressInLine
    {
        /// <summary>
        /// Current Address that has been initialised.
        /// </summary>
        ICribAddress Address { get; set; }
    }
}