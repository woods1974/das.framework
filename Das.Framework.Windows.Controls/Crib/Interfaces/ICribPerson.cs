﻿namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    public interface ICribPerson
    {
        /// <summary>
        /// The persons title such as Mr, Mrs, Miss Dr etc.
        /// </summary>
        string Title { get; set; }
        /// <summary>
        /// The given name of the person
        /// </summary>
        string GivenName { get; set; }
        /// <summary>
        /// Surname / Family name of the person
        /// </summary>
        string FamilyName { get; set; }  
    }
}
