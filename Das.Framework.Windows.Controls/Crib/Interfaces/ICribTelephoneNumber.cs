﻿namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    /// <summary>
    /// Interface to support the crib telephone number.
    /// </summary>
    public interface ICribTelephoneNumber
    {
        /// <summary>
        /// Country code.
        /// </summary>
        string CountryCode { get; set; }

        /// <summary>
        /// Area code.
        /// </summary>
        string AreaCode { get; set; }

        /// <summary>
        /// Local number.
        /// </summary>
        string LocalNumber { get; set; }

        /// <summary>
        /// Extension number.
        /// </summary>
        string ExtensionNumber { get; set; }

        /// <summary>
        /// Indicates if this is a mobile phone number.
        /// </summary>
        bool IsMobileNumber { get; set; }
    }
}
