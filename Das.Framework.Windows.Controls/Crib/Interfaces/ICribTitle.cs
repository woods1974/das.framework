﻿using System.Drawing;
using Das.Framework.Windows.Controls.Interfaces;

namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    public interface ICribTitle : ITextFormat
    {
        string Title { get; set; }
        Color LineColor { get; set; }
        int LineThickness { get; set; }
    }
}
