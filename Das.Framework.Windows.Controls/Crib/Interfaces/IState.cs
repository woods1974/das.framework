namespace Das.Framework.Windows.Controls.Crib.Interfaces
{
    public interface IState
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}