﻿using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Crib.Models
{
    public class Country : ICountry
    {
        #region Implementation of ICountry

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion
    }
}
