﻿using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Crib.Models
{
    /// <summary>
    /// Class that represents a Crib address
    /// </summary>
    public class CribAddress : ICribAddress
    {
        #region Implementation of ICribAddress

        /// <summary>
        /// Address Line 1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address Line 2
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Address Line 3
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Suburb or town
        /// </summary>
        public string SuburbTown { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Postcode or zip code
        /// </summary>
        public string Postcode { get; set; }

        #endregion
    }
}
