﻿using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Crib.Models
{
    public class CribPerson : ICribPerson
    {
        #region Implementation of ICribPerson

        /// <summary>
        /// The persons title such as Mr, Mrs, Miss Dr etc.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The given name of the person
        /// </summary>
        public string GivenName { get; set; }

        /// <summary>
        /// Surname / Family name of the person
        /// </summary>
        public string FamilyName { get; set; }

        #endregion
    }
}
