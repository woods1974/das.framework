﻿using Das.Framework.Windows.Controls.Crib.Interfaces;

namespace Das.Framework.Windows.Controls.Crib.Models
{
    public class CribTelephoneNumber : ICribTelephoneNumber 
    {
        public CribTelephoneNumber()
        {}
        public CribTelephoneNumber(ICribTelephoneNumber cribTelephoneNumber)
        {
            Update(cribTelephoneNumber);
        }

        public void Update(ICribTelephoneNumber cribTelephoneNumber)
        {
            if(cribTelephoneNumber == null)
                return;

            this.CountryCode = cribTelephoneNumber.CountryCode;
            this.AreaCode = cribTelephoneNumber.AreaCode;
            this.LocalNumber = cribTelephoneNumber.LocalNumber;
            this.ExtensionNumber = cribTelephoneNumber.ExtensionNumber;
            this.IsMobileNumber = cribTelephoneNumber.IsMobileNumber;           
        }

        #region Implementation of ICribTelephoneNumber

        /// <summary>
        /// Country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Area code.
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// Local number.
        /// </summary>
        public string LocalNumber { get; set; }

        /// <summary>
        /// Extension number.
        /// </summary>
        public string ExtensionNumber { get; set; }

        /// <summary>
        /// Indicates if this is a mobile phone number.
        /// </summary>
        public bool IsMobileNumber { get; set; }

        #endregion
    }
}
