using System.ComponentModel;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar;

namespace Das.Framework.Windows.Controls
{
    public class DasButton : ButtonX, IDasControl
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;

        public DasButton()
        {
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
        }

        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateButton();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateButton();
            }
        }

        #endregion

        private void UpdateButton()
        {
            Font = DasFonts.SetFont(this);
            Invalidate();
        }
    }
}