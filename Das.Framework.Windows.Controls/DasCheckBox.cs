using System.ComponentModel;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;

namespace Das.Framework.Windows.Controls
{
    public class DasCheckBox : CheckBoxX, IDasControl
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;

        public DasCheckBox()
        {
            CheckBoxStyle = eCheckBoxStyle.CheckBox;
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
        }

        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateCheckBox();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateCheckBox();
            }
        }

        #endregion

        private void UpdateCheckBox()
        {
            Font = DasFonts.SetFont(this);
            Invalidate();
        }
    }
}