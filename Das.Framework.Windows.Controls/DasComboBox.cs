using System.ComponentModel;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar.Controls;

namespace Das.Framework.Windows.Controls
{
    public class DasComboBox : ComboBoxEx, IDasControl
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;

        public DasComboBox()
        {
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
        }

        #region Implementation of ICribControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; } 
            set
            {
                _fontWeight = value;
                UpdateComboBox();
            }
        }

        /// <summary>
        /// Control Font Size
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateComboBox();
            }
        }

        #endregion

        private void UpdateComboBox()
        {
            Font = DasFonts.SetFont(this);
            Invalidate();
        }
    }
}