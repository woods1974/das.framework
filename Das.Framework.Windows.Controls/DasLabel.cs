﻿using System.ComponentModel;
using Das.Framework.Windows.Controls.Common;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar;

namespace Das.Framework.Windows.Controls
{
    public class DasLabel : LabelX, IDasControl, ITextFormat
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;
        private Capitalisation _capitalisation;

        public DasLabel()
        {
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
        }

        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateLabel();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateLabel();
            }
        }

        #endregion

        protected virtual void UpdateLabel()
        {
            Font = DasFonts.SetFont(this);
            Text = TextFormatAssistant.ToCapitalisation(Capitalisation, Text);
            Invalidate();
        }

        #region Implementation of ITextFormat

        /// <summary>
        /// Set or get the Capitalisation of the text.
        /// </summary>
        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.Behaviour)]
        [DefaultValue(Capitalisation.Default)]
        public Capitalisation Capitalisation
        {
            get { return _capitalisation; } 
            set
            {
                _capitalisation = value;
                UpdateLabel();
            }
        }

        #endregion
    }
}
