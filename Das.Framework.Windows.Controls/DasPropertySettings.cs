﻿namespace Das.Framework.Windows.Controls
{
    public static class DasPropertySettings
    {
        public static class PropertyCategory
        {
            public const string Appearance = "Appearance";
            public const string Behaviour = "Behavior";
            public const string RegularExpression = "Regular Expression";
            public const string Data = "Data";
            public const string TelephoneNumber = "Telephone Number";
        }
    }
}
