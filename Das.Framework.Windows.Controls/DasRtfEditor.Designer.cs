﻿namespace Das.Framework.Windows.Controls
{
    partial class DasRtfEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolBar = new DevComponents.DotNetBar.Bar();
            this.cutButton = new DevComponents.DotNetBar.ButtonItem();
            this.copyButton = new DevComponents.DotNetBar.ButtonItem();
            this.pasteButton = new DevComponents.DotNetBar.ButtonItem();
            this.undoButton = new DevComponents.DotNetBar.ButtonItem();
            this.redoButton = new DevComponents.DotNetBar.ButtonItem();
            this.fontComboBox = new DevComponents.DotNetBar.ComboBoxItem();
            this.fontSizeComboBox = new DevComponents.DotNetBar.ComboBoxItem();
            this.boldButton = new DevComponents.DotNetBar.ButtonItem();
            this.italicButton = new DevComponents.DotNetBar.ButtonItem();
            this.underlineButton = new DevComponents.DotNetBar.ButtonItem();
            this.strikeOutButton = new DevComponents.DotNetBar.ButtonItem();
            this.fontColorPicker = new DevComponents.DotNetBar.ColorPickerDropDown();
            this.richTextBox = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.styleManager = new DevComponents.DotNetBar.StyleManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.toolBar)).BeginInit();
            this.SuspendLayout();
            // 
            // toolBar
            // 
            this.toolBar.AntiAlias = true;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cutButton,
            this.copyButton,
            this.pasteButton,
            this.undoButton,
            this.redoButton,
            this.fontComboBox,
            this.fontSizeComboBox,
            this.boldButton,
            this.italicButton,
            this.underlineButton,
            this.strikeOutButton,
            this.fontColorPicker});
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(649, 28);
            this.toolBar.Stretch = true;
            this.toolBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.toolBar.TabIndex = 0;
            this.toolBar.TabStop = false;
            // 
            // cutButton
            // 
            this.cutButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cutButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.cut;
            this.cutButton.Name = "cutButton";
            this.cutButton.Text = "Cut";
            this.cutButton.Tooltip = "Cut";
            // 
            // copyButton
            // 
            this.copyButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.copyButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.copy;
            this.copyButton.Name = "copyButton";
            this.copyButton.Text = "Copy";
            this.copyButton.Tooltip = "Copy";
            // 
            // pasteButton
            // 
            this.pasteButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pasteButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.paste;
            this.pasteButton.Name = "pasteButton";
            this.pasteButton.Text = "Paste";
            this.pasteButton.Tooltip = "Paste";
            // 
            // undoButton
            // 
            this.undoButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.undoButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.undo;
            this.undoButton.Name = "undoButton";
            this.undoButton.Text = "Undo";
            this.undoButton.Tooltip = "Undo";
            // 
            // redoButton
            // 
            this.redoButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.redoButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.redo;
            this.redoButton.Name = "redoButton";
            this.redoButton.Text = "Redo";
            this.redoButton.Tooltip = "Redo";
            // 
            // fontComboBox
            // 
            this.fontComboBox.Caption = "Font";
            this.fontComboBox.ComboWidth = 160;
            this.fontComboBox.DropDownHeight = 106;
            this.fontComboBox.ItemHeight = 17;
            this.fontComboBox.Name = "fontComboBox";
            this.fontComboBox.Text = "Font";
            this.fontComboBox.Tooltip = "Font";
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.DropDownHeight = 106;
            this.fontSizeComboBox.ItemHeight = 17;
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Text = "Font size";
            this.fontSizeComboBox.Tooltip = "Font size";
            // 
            // boldButton
            // 
            this.boldButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.boldButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.bold;
            this.boldButton.Name = "boldButton";
            this.boldButton.Text = "Bold";
            this.boldButton.Tooltip = "Bold";
            // 
            // italicButton
            // 
            this.italicButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.italicButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.italic;
            this.italicButton.Name = "italicButton";
            this.italicButton.Text = "Italic";
            this.italicButton.Tooltip = "Italic";
            // 
            // underlineButton
            // 
            this.underlineButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.underlineButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.underline;
            this.underlineButton.Name = "underlineButton";
            this.underlineButton.Text = "Underline";
            this.underlineButton.Tooltip = "Underline";
            // 
            // strikeOutButton
            // 
            this.strikeOutButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.strikeOutButton.Image = global::Das.Framework.Windows.Controls.Properties.Resources.Strikethrough16;
            this.strikeOutButton.Name = "strikeOutButton";
            this.strikeOutButton.Text = "Strikeout";
            this.strikeOutButton.Tooltip = "Strikeout";
            // 
            // fontColorPicker
            // 
            this.fontColorPicker.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fontColorPicker.Name = "fontColorPicker";
            this.fontColorPicker.Symbol = "";
            this.fontColorPicker.Text = "Font colour";
            // 
            // richTextBox
            // 
            // 
            // 
            // 
            this.richTextBox.BackgroundStyle.Class = "RichTextBoxBorder";
            this.richTextBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Location = new System.Drawing.Point(0, 28);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang3081{\\fonttbl{\\f0\\fnil\\fcharset0 Microsoft S" +
    "ans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this.richTextBox.Size = new System.Drawing.Size(649, 216);
            this.richTextBox.TabIndex = 1;
            // 
            // styleManager
            // 
            this.styleManager.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // DasRtfEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.toolBar);
            this.Name = "DasRtfEditor";
            this.Size = new System.Drawing.Size(649, 244);
            ((System.ComponentModel.ISupportInitialize)(this.toolBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Bar toolBar;
        private DevComponents.DotNetBar.ButtonItem cutButton;
        private DevComponents.DotNetBar.ButtonItem copyButton;
        private DevComponents.DotNetBar.ButtonItem pasteButton;
        private DevComponents.DotNetBar.ButtonItem undoButton;
        private DevComponents.DotNetBar.ButtonItem redoButton;
        private DevComponents.DotNetBar.ComboBoxItem fontComboBox;
        private DevComponents.DotNetBar.ComboBoxItem fontSizeComboBox;
        private DevComponents.DotNetBar.ButtonItem boldButton;
        private DevComponents.DotNetBar.ButtonItem italicButton;
        private DevComponents.DotNetBar.ButtonItem underlineButton;
        private DevComponents.DotNetBar.ButtonItem strikeOutButton;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx richTextBox;
        private DevComponents.DotNetBar.StyleManager styleManager;
        private DevComponents.DotNetBar.ColorPickerDropDown fontColorPicker;
    }
}
