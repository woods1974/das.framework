﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar;
using CommandManager = Das.Framework.Windows.Input.CommandManager;

namespace Das.Framework.Windows.Controls
{
    public partial class DasRtfEditor : DasUserControl, IDasRtfEditor
    {
        public DasRtfEditor()
        {
            InitializeComponent();
            Initialise();
        }

        private ICommandManager _commandManager;

        private ICommandManager CommandManager
        {
            get
            {
                return _commandManager ?? (_commandManager = new CommandManager());
            }
        }

        public void Initialise()
        {
            toolBar.Style = eDotNetBarStyle.Metro;
            BoldCommand = new DelegateCommand(Bold, CanBold);
            ItalicCommand = new DelegateCommand(Italic, CanItalic);
            UnderlineCommand = new DelegateCommand(Underline, CanUnderline);
            StrikeoutCommand = new DelegateCommand(StrikeThrough, CanStrikeThrough);
            CutCommand = new DelegateCommand(Cut, CanCut);
            PasteCommand = new DelegateCommand(Paste, CanPaste);
            CopyCommand = new DelegateCommand(Copy, CanCopy);
            UndoCommand = new DelegateCommand(Undo, CanUndo);
            RedoCommand = new DelegateCommand(Redo, CanRedo);
            ForeColorCommand = new DelegateCommand(SetForeColor, CanSetForeColor);

            CommandManager.Bind(fontColorPicker, ForeColorCommand);
            CommandManager.Bind(redoButton, RedoCommand);
            CommandManager.Bind(undoButton, UndoCommand);
            CommandManager.Bind(copyButton, CopyCommand);
            CommandManager.Bind(pasteButton, PasteCommand);
            CommandManager.Bind(cutButton, CutCommand);
            CommandManager.Bind(boldButton, BoldCommand);
            CommandManager.Bind(italicButton, ItalicCommand);
            CommandManager.Bind(underlineButton, UnderlineCommand);
            CommandManager.Bind(strikeOutButton, StrikeoutCommand);

            SelectedFontColor = Color.Black;
            fontColorPicker.ForeColor = SelectedFontColor;

            SubscribeToEvents();
            InitialiseContextMenu();
            InitialiseFonts();
            Evaluate();
            Invalidate(true);
        }

        private bool CanSetForeColor()
        {
            return true;
        }

        private void SetForeColor(object obj)
        {
            richTextBox.SelectionColor = SelectedFontColor;
        }

        private bool CanRedo()
        {
            return true;
        }

        private void Redo(object obj)
        {
            richTextBox.Redo();
        }

        private bool CanUndo()
        {
            return true;
        }

        private void Undo(object obj)
        {
            richTextBox.Undo();
        }

        private void InitialiseFonts()
        {
            foreach (var font in FontFamily.Families)
            {
                fontComboBox.Items.Add(font);
                if (font.Name == "Arial")
                {
                    SelectedFontFamily = font;
                }
            }

            fontComboBox.DisplayMember = "Name";
            fontComboBox.ComboWidth = 140;

            var sizes = new List<float> { 8, 9, 10, 11, 12, 14, 16, 18, 20, 24, 26, 28, 30, 32, 48, 72 };
            foreach (var size in sizes)
            {
                fontSizeComboBox.Items.Add(size);
            }

            SelectedFontSize = 10;
            SelectedFontColor = Color.Black;
        }

        private void InitialiseContextMenu()
        {
            richTextBox.ContextMenuStrip = new ContextMenuStrip();

            var cutMenuItem = new ToolStripMenuItem("Cut", cutButton.Image);
            var copyMenuItem = new ToolStripMenuItem("Copy", copyButton.Image);
            var pasteMenuItem = new ToolStripMenuItem("Paste", pasteButton.Image);

            cutMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            copyMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            pasteMenuItem.ShortcutKeyDisplayString = "Ctrl+V";

            cutMenuItem.ShowShortcutKeys = true;
            copyMenuItem.ShowShortcutKeys = true;
            pasteMenuItem.ShowShortcutKeys = true;

            CommandManager.Bind(cutMenuItem,  CutCommand);
            CommandManager.Bind(copyMenuItem, CopyCommand);
            CommandManager.Bind(pasteMenuItem, PasteCommand);

            richTextBox.ContextMenuStrip.Items.Add(cutMenuItem);
            richTextBox.ContextMenuStrip.Items.Add(copyMenuItem);
            richTextBox.ContextMenuStrip.Items.Add(pasteMenuItem);
        }

        public virtual bool CanCopy()
        {
            return SelectedText.Length > 0;
        }

        public virtual void Copy(object obj)
        {
            richTextBox.Copy();
        }

        public virtual bool CanPaste()
        {
            return Clipboard.ContainsText() || Clipboard.ContainsImage();
        }

        public virtual void Paste(object obj)
        {
            richTextBox.Paste();
        }

        public virtual bool CanCut()
        {
            return SelectedText.Length > 0;
        }

        public virtual void Cut(object obj)
        {
            richTextBox.Cut();
        }

        public virtual bool CanStrikeThrough()
        {
            return richTextBox.SelectionFont != null;
        }

        public virtual void StrikeThrough(object obj)
        {
            Strikeout();
            Evaluate();
        }

        public virtual bool CanUnderline()
        {
            return richTextBox.SelectionFont != null;
        }

        public virtual void Underline(object obj)
        {
            Underline();
            Evaluate();
        }

        public virtual bool CanItalic()
        {
            return richTextBox.SelectionFont != null;
        }

        public virtual void Italic(object obj)
        {
            Italic();
            Evaluate();
        }

        private void UnSubscribeToEvents()
        {
            richTextBox.RichTextBox.KeyDown -= RichTextboxKeyDownEvent;
            richTextBox.RichTextBox.MouseClick -= RichTextBoxMouseClickEvent;
            richTextBox.RichTextBox.SelectionChanged -= RichTextBoxSelectionChangedEvent;
            fontComboBox.SelectedIndexChanged -= FontSelectedIndexChangedEvent;
            fontSizeComboBox.SelectedIndexChanged -= FontSizeSelectedIndexChangedEvent;
            fontColorPicker.SelectedColorChanged -= FontPickerColorChangedEvent;
            Resize -= EditorResizeEvent;
        }

        private void SubscribeToEvents()
        {
            richTextBox.RichTextBox.KeyDown += RichTextboxKeyDownEvent;
            richTextBox.RichTextBox.MouseClick += RichTextBoxMouseClickEvent;
            richTextBox.RichTextBox.SelectionChanged += RichTextBoxSelectionChangedEvent;
            fontComboBox.SelectedIndexChanged += FontSelectedIndexChangedEvent;
            fontSizeComboBox.SelectedIndexChanged += FontSizeSelectedIndexChangedEvent;
            fontColorPicker.SelectedColorChanged += FontPickerColorChangedEvent;
            Resize += EditorResizeEvent;
        }

        private void EditorResizeEvent(object sender, EventArgs e)
        {
            richTextBox.Invalidate(true);
        }

        public virtual void FontPickerColorChangedEvent(object sender, EventArgs e)
        {
            richTextBox.SelectionColor = SelectedFontColor;
            fontColorPicker.ForeColor = SelectedFontColor;
        }

        public virtual void FontSizeSelectedIndexChangedEvent(object sender, EventArgs e)
        {
            if (richTextBox.SelectionFont != null)
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont.FontFamily, SelectedFontSize);
        }

        public virtual void FontSelectedIndexChangedEvent(object sender, EventArgs e)
        {
            if (richTextBox.SelectionFont != null)
            {
                try
                {
                    richTextBox.SelectionFont = new Font(SelectedFontFamily, SelectedFontSize);
                }
                catch (Exception)
                {
                    // Some font families do not support the 'regular' style. In this case just consume the exception
                }
            }
        }

        public virtual void RichTextBoxSelectionChangedEvent(object sender, EventArgs e)
        {
            Evaluate();
        }

        public virtual void RichTextBoxMouseClickEvent(object sender, MouseEventArgs e)
        {
            Evaluate();
        }

        public virtual void RichTextboxKeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.B)
            {
                CommandManager.Execute(BoldCommand);
            }
            else if (e.Control && e.KeyCode == Keys.I)
            {
                CommandManager.Execute(ItalicCommand);
                e.SuppressKeyPress = true;
            }
            else if (e.Control && e.KeyCode == Keys.U)
            {
                CommandManager.Execute(UnderlineCommand);
            }
            Evaluate();
        }

        public virtual void Bold(object obj)
        {
            Bold();
            Evaluate();
        }

        public virtual bool CanBold()
        {
            return richTextBox.SelectionFont != null;
        }


        public virtual void Evaluate()
        {
            boldButton.Checked = IsBold;
            italicButton.Checked = IsItalic;
            underlineButton.Checked = IsUnderline;
            strikeOutButton.Checked = IsStrikeout;

            if (richTextBox.SelectionFont != null)
            {
                SelectedFontFamily = richTextBox.SelectionFont.FontFamily;
                SelectedFontSize = richTextBox.SelectionFont.Size;
            }

            CommandManager.EvaluateCommands();
        }

        #region Implementation of IDasRtfEditor

        public virtual string RtfText
        {
            get { return richTextBox.Rtf; }
            set { richTextBox.Rtf = value; }
        }
        public virtual string PlainText
        {
            get { return richTextBox.Text; }
            set { richTextBox.Text = value; }
        }

        public string SelectedText
        {
            get { return richTextBox.SelectedText; }
        }
        public IDelegateCommand BoldCommand { get;  private set; }
        public IDelegateCommand ItalicCommand { get;  private set; }
        public IDelegateCommand UnderlineCommand { get;  private set; }
        public IDelegateCommand UndoCommand { get;  private set; }
        public IDelegateCommand RedoCommand { get;  private set; }
        public IDelegateCommand StrikeoutCommand { get;  private set; }
        public IDelegateCommand CutCommand { get;  private set; }
        public IDelegateCommand PasteCommand { get;  private set; }
        public IDelegateCommand CopyCommand { get;  private set; }
        public IDelegateCommand ForeColorCommand { get;  private set; }

        public virtual void Bold()
        {
            richTextBox.SelectionFont = new Font(richTextBox.SelectionFont.FontFamily, richTextBox.SelectionFont.Size, richTextBox.SelectionFont.Style ^ FontStyle.Bold);
        }

        public virtual void Underline()
        {
            richTextBox.SelectionFont = new Font(richTextBox.SelectionFont.FontFamily, richTextBox.SelectionFont.Size, richTextBox.SelectionFont.Style ^ FontStyle.Underline);
        }

        public virtual bool IsUnderline
        {
            get { return richTextBox.SelectionFont != null && richTextBox.SelectionFont.Underline; }
        }

        public virtual bool IsBold
        {
            get { return richTextBox.SelectionFont != null && richTextBox.SelectionFont.Bold; }
        }

        public virtual void Italic()
        {
            richTextBox.SelectionFont = new Font(richTextBox.SelectionFont.FontFamily, richTextBox.SelectionFont.Size, richTextBox.SelectionFont.Style ^ FontStyle.Italic);
        }

        public virtual bool IsItalic
        {
            get { return richTextBox.SelectionFont != null && richTextBox.SelectionFont.Italic; }
        }

        public virtual void Strikeout()
        {
            richTextBox.SelectionFont = new Font(richTextBox.SelectionFont.FontFamily, richTextBox.SelectionFont.Size, richTextBox.SelectionFont.Style ^ FontStyle.Strikeout);
        }

        public virtual bool IsStrikeout
        {
            get { return richTextBox.SelectionFont != null && richTextBox.SelectionFont.Strikeout; }
        }

        public virtual FontFamily SelectedFontFamily
        {
            get { return fontComboBox.SelectedItem as FontFamily; }
            set { fontComboBox.SelectedItem = value; }
        }

        public virtual float SelectedFontSize
        {
            get { return (float)fontSizeComboBox.SelectedItem; }
            set { fontSizeComboBox.SelectedItem = value; }
        }

        public virtual Color SelectedFontColor
        {
            get { return fontColorPicker.SelectedColor; }
            set { fontColorPicker.SelectedColor = value; }
        }

        #endregion

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                UnSubscribeToEvents();
                if (CommandManager != null)
                {
                    CommandManager.Dispose();
                }
                components.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
