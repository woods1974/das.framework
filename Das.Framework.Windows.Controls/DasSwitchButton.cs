using System.ComponentModel;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar.Controls;

namespace Das.Framework.Windows.Controls
{
    public class DasSwitchButton : SwitchButton, IDasControl
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;
     
        public DasSwitchButton()
        {
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
        }

        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateSwitchButton();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateSwitchButton();
            }
        }

        #endregion

        private void UpdateSwitchButton()
        {
            Font = DasFonts.SetFont(this);
            Invalidate();
        }
    }
}