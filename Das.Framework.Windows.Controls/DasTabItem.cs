using System.ComponentModel;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar;

namespace Das.Framework.Windows.Controls
{
    public class DasTabItem : TabItem, IDasControl
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;

        #region Implementation of ICribControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateTabItem();
            }
        }

        /// <summary>
        /// Control Font Size
        /// </summary>
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateTabItem();
            }
        }

        #endregion

        private void UpdateTabItem()
        {
            //this.AttachedControl.Font = CribFonts.SetFont(this);
        }
    }
}