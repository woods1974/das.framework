﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Das.Framework.Core.Extensions;
using Das.Framework.Windows.Controls.Common;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Data;
using Das.Framework.Windows.Data.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using DevComponents.DotNetBar.Controls;

namespace Das.Framework.Windows.Controls
{
    public class DasTextBox : TextBoxX, IDasControl, IRegularExpressionValidator, ITextFormat
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;

        public DasTextBox()
        {
            Initialise();
            UpdateTextBox();
        }

        private void Initialise()
        {
            SubscribeToEvents();
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;
            Capitalisation = Capitalisation.Default;
        }

        private void SubscribeToEvents()
        {
            KeyPress += KeyPressEvent;
            LostFocus += LostFocusEvent;
        }

        protected virtual void LostFocusEvent(object sender, EventArgs e)
        {
            Text = TextFormatAssistant.ToCapitalisation(Capitalisation, Text);           
        }

        protected virtual void UnsubscribeToEvents()
        {
            KeyPress -= KeyPressEvent;
            LostFocus -= LostFocusEvent;
        }

         private void KeyPressEvent(object sender, KeyPressEventArgs e)
         {
             if (!RegexEnabled || char.IsControl(e.KeyChar))
                 return;

             e.Handled = !RegularExpressionValidator.IsValid(e.KeyChar.ToString(CultureInfo.InvariantCulture), this);
         }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UnsubscribeToEvents();
            }
            base.Dispose(disposing);
        }
       
        [Browsable(true), DefaultValue(Windows.Controls.Capitalisation.Default), 
        Category(DasPropertySettings.PropertyCategory.Behaviour)]
        public Capitalisation Capitalisation { get; set; }

        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontWeight FontWeight
        {
            get { return _fontWeight; } 
            set
            {
                _fontWeight = value;
                UpdateTextBox();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateTextBox();
            }
        }

        #endregion

        private void UpdateTextBox()
        {
            Font = DasFonts.SetFont(this);
            Invalidate();
        }
        
        #region Implementation of IRegularExpressionValidator

        /// <summary>
        /// When enabled, restricts the input based on the regular expression.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(false)]
        [Category(DasPropertySettings.PropertyCategory.RegularExpression)]
        public bool RegexEnabled { get; set; }

        /// <summary>
        /// The regular expression to evaluate the input.
        /// </summary>
        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.RegularExpression)]
        public string RegularExpression { get; set; }

        /// <summary>
        /// True when the text of the control matches the regular expression.
        /// </summary>
        [Browsable(false)]
        public bool RegexMatched
        {
            get
            {
                return RegexEnabled && RegularExpressionValidator.IsValid(Text, this);
            }
        }

        /// <summary>
        /// Sets the regular expression option.
        /// </summary>
        [Browsable(true)]
        [Category(DasPropertySettings.PropertyCategory.RegularExpression)]
        public RegexOptions RegexOptions { get; set; }

        #endregion
    }
}
