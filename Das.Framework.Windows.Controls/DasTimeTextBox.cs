﻿using System;
using System.ComponentModel;

namespace Das.Framework.Windows.Controls
{
    public class DasTimeTextBox : DasTextBox
    {
        private TimeSpan _timeSpan;

        public DasTimeTextBox()
        {
            Initialise();
        }

        private void Initialise()
        {
            RegexEnabled = true;
            RegularExpression = "[0-9:]";
            MaxLength = 5;
            Time = new TimeSpan(0,6,0,0);
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            LostFocus += LostFocusEvent;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                UnsubscribeToEvents();
            base.Dispose(disposing);
        }

        protected override void UnsubscribeToEvents()
        {
            LostFocus -= LostFocusEvent;
            base.UnsubscribeToEvents();
        }

        protected override void LostFocusEvent(object sender, EventArgs e)
        {
            base.LostFocusEvent(sender, e);
            TryParseTime();
        }

        private void TryParseTime()
        {

            TimeSpan time;
            var text = Text;
            if(Text.Length == 4)
            {
                text = string.Format("{0}:{1}", Text.Substring(0, 2), Text.Substring(2));
            }
            else if(Text.Length == 3)
            {
                text = string.Format("0{0}:{1}", Text.Substring(0, 1), Text.Substring(1));
            }


            if (TimeSpan.TryParse(text, out time))
            {
                Time = time;
            }
            else
            {
                
                Time = new TimeSpan(0,0,0,0);
            }
        }

        [Browsable(true)]
        public TimeSpan Time
        {
            get { return _timeSpan; }
            set
            {
                _timeSpan = value;
                UpdateTextBox();
            }
        }

        private void UpdateTextBox()
        {
            if (Time.Hours < 12 && Time.Minutes < 10)
                Text = string.Format("0{0}:0{1}", Time.Hours, Time.Minutes);
            else if(Time.Hours > 12 && Time.Minutes < 10)
                Text = string.Format("{0}:0{1}", Time.Hours, Time.Minutes);
            else if (Time.Hours < 12)
                Text = string.Format("0{0}:{1}", Time.Hours, Time.Minutes);
            else
                Text = string.Format("{0}:{1}", Time.Hours, Time.Minutes);
        }

        [Browsable(false)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
    }
}
