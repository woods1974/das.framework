﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using Das.Framework.Core.Extensions;
using Das.Framework.Core.Notification.Interfaces;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Controls.Interfaces;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Controls
{
    public partial class DasUserControl : UserControl, IDasControl, IChangeNotification
    {
        private DasFontWeight _fontWeight;
        private DasFontSize _fontSize;
        private bool _overrideAppearance;
        
        public DasUserControl()
        {
            InitializeComponent();
            InitialiseUserControl();
        }

        private void InitialiseUserControl()
        {
            Initialise();
            UpdateUserControl();  
        }

        protected virtual void Initialise()
        {
            BackColor = Color.White;
            _overrideAppearance = false;
            FontSize = DasFontSize.Medium;
            FontWeight = DasFontWeight.Regular;          
        }

        private void UpdateUserControl()
        {
            if(OverrideAppearance)
            {
                Font = DasFonts.SetFont(this);
                foreach (var control in Controls.OfType<IDasControl>())
                {
                    control.FontSize = FontSize;
                    control.FontWeight = FontWeight;
                }
                Invalidate(true);
            }
        }

        /// <summary>
        /// When true, all controls contained will inherit the user control appearance
        /// such as the FontSize and FontWeight.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(false)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public virtual bool OverrideAppearance
        {
            get {return _overrideAppearance;}
            set
            {
                _overrideAppearance = value;
                UpdateUserControl();
            }
        }
        #region Implementation of IDasControl

        /// <summary>
        /// Delegate Command
        /// </summary>
        [Browsable(false)]
        public virtual IDelegateCommand DelegateCommand { get; set; }

        /// <summary>
        /// Control Font Weight
        /// </summary>
        [DefaultValue(DasFontWeight.Regular)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public virtual DasFontWeight FontWeight
        {
            get { return _fontWeight; }
            set
            {
                _fontWeight = value;
                UpdateUserControl();
            }
        }
        /// <summary>
        /// Control Font Size
        /// </summary>
        [DefaultValue(DasFontSize.Medium)]
        [Category(DasPropertySettings.PropertyCategory.Appearance)]
        public virtual DasFontSize FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                UpdateUserControl();
            }
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Implementation of IChangeNotification

        /// <summary>
        /// Implement this method to cycle through specified properties calling the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyNames">List of property names</param>
        public void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null || IsNotificationSuspended) return;

            foreach (var property in propertyNames)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
        }

        /// <summary>
        /// Sets the suspended state of the change notification.
        /// When true, will disable all property changed notification events.
        /// </summary>
        /// <param name="suspendNotification"></param>
        public void SetSuspendNotification(bool suspendNotification)
        {
            IsNotificationSuspended = suspendNotification;
        }

        /// <summary>
        /// Indicates whether the notification events has been suspended.
        /// </summary>
        public bool IsNotificationSuspended { get; private set; }
        
        #endregion

        /// <summary>
        /// Notify a property changed event.
        /// </summary>
        /// <param name="property">Property name that has changed.</param>
        public void NotifyPropertyChanged<T>(Expression<Func<T, object>> property)
        {
            // Get the property
            var propertyExpression = ExpressionExtensions.GetMemberExpression(property);
            if (propertyExpression == null)
                throw new InvalidOperationException("You must specify a property");

            // Property name
            var propertyName = propertyExpression.Member.Name;
            NotifyPropertyChanged(propertyName);
        }
    }
}
