namespace Das.Framework.Windows.Controls.Fonts
{
    public enum DasFontSize
    {
        Small,
        Medium,
        Tall
    }
}