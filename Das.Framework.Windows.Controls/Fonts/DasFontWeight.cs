namespace Das.Framework.Windows.Controls.Fonts
{
    public enum DasFontWeight
    {
        Bold,
        Light,
        Regular
    }
}