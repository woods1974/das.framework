using System.Drawing;
using Das.Framework.Windows.Controls.Interfaces;

namespace Das.Framework.Windows.Controls.Fonts
{
    public static class DasFonts
    {
        public const string FontSegoeUi = "Segoe UI";
        public const string FontSegoeUiLight = "Segoe UI Light";
        public const float FontSize12 = 12f;
        public const float FontSize14 = 14f;
        public const float FontSize18 = 18f;
        
        public static Font Light(float size)
        {
            return new Font(FontSegoeUiLight, size, FontStyle.Regular, GraphicsUnit.Pixel);
        }

        public static Font Regular(float size)
        {
            return new Font(FontSegoeUi, size, FontStyle.Regular, GraphicsUnit.Pixel);
        }

        public static Font Bold(float size)
        {
            return new Font(FontSegoeUi, size, FontStyle.Bold, GraphicsUnit.Pixel);
        }

        public static Font SetFont(IDasControl control)
        {
            if (control == null)
                return Regular(FontSize14);

            switch (control.FontSize)
            {
                case DasFontSize.Small:
                    if (control.FontWeight == DasFontWeight.Light)
                        return Light(FontSize12);
                    if (control.FontWeight == DasFontWeight.Regular)
                        return Regular(FontSize12);
                    if (control.FontWeight == DasFontWeight.Bold)
                        return Bold(FontSize12);
                    break;
                case DasFontSize.Medium:
                    if (control.FontWeight == DasFontWeight.Light)
                        return Light(FontSize14);
                    if (control.FontWeight == DasFontWeight.Regular)
                        return Regular(FontSize14);
                    if (control.FontWeight == DasFontWeight.Bold)
                        return Bold(FontSize14);
                    break;
                case DasFontSize.Tall:
                    if (control.FontWeight == DasFontWeight.Light)
                        return Light(FontSize18);
                    if (control.FontWeight == DasFontWeight.Regular)
                        return Regular(FontSize18);
                    if (control.FontWeight == DasFontWeight.Bold)
                        return Bold(FontSize18);
                    break;
            }

            return Regular(FontSize14);
        }
}

}