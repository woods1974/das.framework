﻿using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Controls.Interfaces
{
    public interface IDasControl
    {
        /// <summary>
        /// Delegate Command
        /// </summary>
        IDelegateCommand DelegateCommand { get; set; }
        /// <summary>
        /// Control Font Weight
        /// </summary>
        DasFontWeight FontWeight { get; set; }
        /// <summary>
        /// Control Font Size
        /// </summary>
        DasFontSize FontSize { get; set; }
    }
}
