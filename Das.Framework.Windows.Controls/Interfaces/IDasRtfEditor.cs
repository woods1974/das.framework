﻿using System.Drawing;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Controls.Interfaces
{
    public interface IDasRtfEditor
    {
        string RtfText { get; set; }
        string PlainText { get; set; }
        string SelectedText { get; }
        IDelegateCommand BoldCommand { get; }
        IDelegateCommand ItalicCommand { get; }
        IDelegateCommand UnderlineCommand { get; }
        IDelegateCommand UndoCommand { get; }
        IDelegateCommand RedoCommand { get; }
        IDelegateCommand StrikeoutCommand { get; }
        IDelegateCommand CutCommand { get; }
        IDelegateCommand PasteCommand { get; }
        IDelegateCommand CopyCommand { get; }
        IDelegateCommand ForeColorCommand { get; }
        void Bold();
        void Underline();
        bool IsUnderline { get; }
        bool IsBold { get; }
        void Italic();
        bool IsItalic { get; }
        void Strikeout();
        bool IsStrikeout { get; }
        FontFamily SelectedFontFamily { get; set; }
        float SelectedFontSize { get; set; }
        Color SelectedFontColor { get; set; }
    }
}
