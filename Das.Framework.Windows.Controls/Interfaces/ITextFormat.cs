namespace Das.Framework.Windows.Controls.Interfaces
{
    public interface ITextFormat
    {
        /// <summary>
        /// Set or get the Capitalisation of the text.
        /// </summary>
        Capitalisation Capitalisation { get; set; }
    }
}