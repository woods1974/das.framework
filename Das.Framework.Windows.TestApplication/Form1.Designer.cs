﻿namespace Das.Framework.Windows.TestApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cribAddressInFormInput1 = new Das.Framework.Windows.Controls.Crib.CribAddressInFormInput();
            this.cribDatePicker1 = new Das.Framework.Windows.Controls.Crib.CribDatePicker();
            this.cribTimePicker1 = new Das.Framework.Windows.Controls.Crib.CribTimePicker();
            this.SuspendLayout();
            // 
            // cribAddressInFormInput1
            // 
            this.cribAddressInFormInput1.AddressLine1 = "";
            this.cribAddressInFormInput1.AddressLine2 = "";
            this.cribAddressInFormInput1.AddressLine3 = "";
            this.cribAddressInFormInput1.BackColor = System.Drawing.Color.White;
            this.cribAddressInFormInput1.Countries = null;
            this.cribAddressInFormInput1.DelegateCommand = null;
            this.cribAddressInFormInput1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cribAddressInFormInput1.Location = new System.Drawing.Point(12, 12);
            this.cribAddressInFormInput1.MinimumSize = new System.Drawing.Size(218, 186);
            this.cribAddressInFormInput1.Name = "cribAddressInFormInput1";
            this.cribAddressInFormInput1.OverrideAppearance = true;
            this.cribAddressInFormInput1.Postcode = "";
            this.cribAddressInFormInput1.SelectedCountry = null;
            this.cribAddressInFormInput1.SelectedState = null;
            this.cribAddressInFormInput1.Size = new System.Drawing.Size(283, 236);
            this.cribAddressInFormInput1.States = null;
            this.cribAddressInFormInput1.SuburbTown = "";
            this.cribAddressInFormInput1.TabIndex = 0;
            // 
            // cribDatePicker1
            // 
            this.cribDatePicker1.BackColor = System.Drawing.Color.White;
            this.cribDatePicker1.Date = new System.DateTime(2013, 9, 11, 0, 0, 0, 0);
            this.cribDatePicker1.DelegateCommand = null;
            this.cribDatePicker1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cribDatePicker1.Image = ((System.Drawing.Image)(resources.GetObject("cribDatePicker1.Image")));
            this.cribDatePicker1.Location = new System.Drawing.Point(85, 320);
            this.cribDatePicker1.MinimumSize = new System.Drawing.Size(120, 26);
            this.cribDatePicker1.Name = "cribDatePicker1";
            this.cribDatePicker1.Size = new System.Drawing.Size(120, 26);
            this.cribDatePicker1.TabIndex = 1;
            // 
            // cribTimePicker1
            // 
            this.cribTimePicker1.BackColor = System.Drawing.Color.White;
            this.cribTimePicker1.DelegateCommand = null;
            this.cribTimePicker1.Image = ((System.Drawing.Image)(resources.GetObject("cribTimePicker1.Image")));
            this.cribTimePicker1.Location = new System.Drawing.Point(225, 320);
            this.cribTimePicker1.MinimumSize = new System.Drawing.Size(70, 26);
            this.cribTimePicker1.Name = "cribTimePicker1";
            this.cribTimePicker1.Size = new System.Drawing.Size(70, 26);
            this.cribTimePicker1.TabIndex = 2;
            this.cribTimePicker1.Time = System.TimeSpan.Parse("06:00:00");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 406);
            this.Controls.Add(this.cribTimePicker1);
            this.Controls.Add(this.cribDatePicker1);
            this.Controls.Add(this.cribAddressInFormInput1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Crib.CribAddressInFormInput cribAddressInFormInput1;
        private Controls.Crib.CribDatePicker cribDatePicker1;
        private Controls.Crib.CribTimePicker cribTimePicker1;




    }
}