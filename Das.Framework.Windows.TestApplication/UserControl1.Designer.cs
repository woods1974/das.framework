﻿namespace Das.Framework.Windows.TestApplication
{
    partial class UserControl1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cribDateTextBox1 = new Das.Framework.Windows.Controls.Crib.CribDateTextBox();
            this.SuspendLayout();
            // 
            // cribDateTextBox1
            // 
            this.cribDateTextBox1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.cribDateTextBox1.Border.Class = "TextBoxBorder";
            this.cribDateTextBox1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cribDateTextBox1.Date = new System.DateTime(2013, 8, 13, 0, 0, 0, 0);
            this.cribDateTextBox1.DelegateCommand = null;
            this.cribDateTextBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cribDateTextBox1.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Small;
            this.cribDateTextBox1.ForeColor = System.Drawing.Color.Black;
            this.cribDateTextBox1.Location = new System.Drawing.Point(60, 118);
            this.cribDateTextBox1.MaxLength = 11;
            this.cribDateTextBox1.Name = "cribDateTextBox1";
            this.cribDateTextBox1.RegexOptions = System.Text.RegularExpressions.RegexOptions.None;
            this.cribDateTextBox1.RegularExpression = null;
            this.cribDateTextBox1.Size = new System.Drawing.Size(100, 23);
            this.cribDateTextBox1.TabIndex = 0;
            this.cribDateTextBox1.Text = "13-Aug-2013";
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cribDateTextBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.FontSize = Das.Framework.Windows.Controls.Fonts.DasFontSize.Tall;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(335, 370);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Crib.CribDateTextBox cribDateTextBox1;

    }
}
