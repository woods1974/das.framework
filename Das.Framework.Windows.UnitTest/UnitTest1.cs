﻿using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Windows.Controls;
using Das.Framework.Windows.Controls.Fonts;
using Das.Framework.Windows.Data;
using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar.Metro;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Das.Framework.Windows.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var myview = new MyView(){Size = new Size(200,200)};
            myview.ShowDialog();
        }
    }

    public class MyView : MetroForm, IMyView
    {
        private readonly ControlBindingManager<MyView> _controlBindingManager;
        private readonly TextBox _familyNameTextBox;
        private readonly TextBox _givenNameTextBox;
      
        public MyView()
        {
            _familyNameTextBox = new DasTextBox()
                                     {
                                         FontWeight = DasFontWeight.Light,
                                         FontSize =  DasFontSize.Small
                                     };

            
            _givenNameTextBox = new TextBoxX(){Location = new Point(150,0)};

            Controls.Add(_familyNameTextBox);
            Controls.Add(_givenNameTextBox);

            _controlBindingManager = new ControlBindingManager<MyView>(this);
            _controlBindingManager.PropertyChanged += _controlBindingManager_PropertyChanged;
            _controlBindingManager.Bind<TextBox>(_familyNameTextBox, p => p.Text, ds => ds.FamilyName);

        }

        void _controlBindingManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            MessageBox.Show(e.PropertyName);
        }

        #region Implementation of IMyView

        public string FamilyName
        {
            get { return _familyNameTextBox.Text; }
            set { _familyNameTextBox.Text = value; }
        }

        #endregion
    }
    public interface IMyView
    {
        string FamilyName { get; set; }
    }
}
