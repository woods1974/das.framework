﻿using System;
using Das.Framework.Core.Notification;
using Das.Framework.Core.Notification.Interfaces;
using Das.Framework.Windows.Input;
using Das.Framework.Windows.Input.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;

namespace Das.Framework.Windows
{
    /// <summary>
    /// A class that is shared by the application.
    /// </summary>
    public class ApplicationContext : IApplicationContext
    {

        private bool _isDisposed;

        /// <summary>
        /// A class that is shared by the application.
        /// </summary>
         public ApplicationContext()
         {
             InitialiseContext();
         }
        
        /// <summary>
        /// A class that is shared by the application.
        /// </summary>
        /// <param name="unityContainer">Unity Dependency Injection Container.</param>
        public ApplicationContext(IUnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
            InitialiseContext();
        }

        private void InitialiseContext()
        {
            Id = Guid.NewGuid();
            var eventBroker = new EventBroker(new EventAggregator());
            var commandManager = new CommandManager();
            UnityContainer.RegisterInstance<IEventBroker>(eventBroker);
            UnityContainer.RegisterInstance<ICommandManager>(commandManager);
        }

        /// <summary>
        /// Unique application context Id.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Manages the event publishing and subscriptions within the winforms application.
        /// </summary>
        public IEventBroker EventBroker
        {
            get { return UnityContainer.Resolve<IEventBroker>(); }
        }

        /// <summary>
        /// Manage bindings between a command and control.
        /// </summary>
        public ICommandManager CommandManager
        {
            get
            {
                return UnityContainer.Resolve<ICommandManager>();
            }
        }

        private IUnityContainer _unityContainer;

        /// <summary>
        /// Unity Dependency Injection Container
        /// </summary>
        public IUnityContainer UnityContainer
        {
            get
            {
                return _unityContainer ?? (_unityContainer = new UnityContainer());
            }
        }

       
        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_isDisposed)
            {
                if (_unityContainer != null)
                {
                    _isDisposed = true;
                    _unityContainer.Dispose();
                }
            }
        }

        #endregion
    }
}
