﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Forms;
using Das.Framework.Windows.Data.Interfaces;

namespace Das.Framework.Windows.Data
{
    public class ControlBindingManager<T> :  IDirtyable, IControlBindingManager<T> where T : Control
    {
        public ControlBindingManager(T dataSource)
        {
            DataSource = dataSource;
        }

        /// <summary>
        /// Bind a control property to a data member.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        public virtual void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember) where TControl : Control
        {
            Bind(control, property, dataMember, DataSourceUpdateMode.OnValidation);  
        }
        /// <summary>
        /// Bind a control property to a data member and set the data source update mode.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        /// <param name="dataSourceUpdateMode">The data source update mode</param>
        public virtual void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember, DataSourceUpdateMode dataSourceUpdateMode) where TControl : Control
        {
            Bind(control, property, dataMember, DataSourceUpdateMode.OnValidation, ControlUpdateMode.OnPropertyChanged);  
        }
        /// <summary>
        /// Bind a control property to a data member and set both the data source and control update modes.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        /// <param name="dataSourceUpdateMode">The data source update mode</param>
        /// <param name="controlUpdateMode">The control update mode</param>
        public virtual void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember, DataSourceUpdateMode dataSourceUpdateMode, ControlUpdateMode controlUpdateMode) where TControl : Control
        {
            var dataMemberName = GetPropertyName(dataMember);
            var propertyName = GetPropertyName(property);

            var binding = new Binding(propertyName, DataSource, dataMemberName)
            {
                ControlUpdateMode = controlUpdateMode,
                DataSourceUpdateMode = dataSourceUpdateMode
            };
            
            binding.Parse += ParseBindingEvent;
            control.DataBindings.Add(binding);
        }

        private void ParseBindingEvent(object sender, ConvertEventArgs e)
        {
            if(sender is Binding && !IsNotificationSuspended)
            {
                NotifyPropertyChanged(((Binding)sender).BindingMemberInfo.BindingMember);
                SetIsDirty(true);
            }
        }

        /// <summary>
        /// Binding Data Source.
        /// </summary>
        public T DataSource { get; private set; }

        protected ControlBindingsCollection ControlBindings { get; private set; }
        
        #region IDisposable Implementation

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            foreach (Binding controlBinding in DataSource.DataBindings)
            {
                controlBinding.Parse -= ParseBindingEvent;
            }

            DataSource.DataBindings.Clear();
        }

        #endregion
        
        #region Notification

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Implement this method to cycle through specified properties calling the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyNames">List of property names</param>
        public void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null || IsNotificationSuspended) return;

            foreach (var property in propertyNames)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
        }

        /// <summary>
        /// Sets the suspended state of the change notification.
        /// When true, will disable all property changed notification events.
        /// </summary>
        /// <param name="suspendNotification"></param>
        public void SetSuspendNotification(bool suspendNotification)
        {
            IsNotificationSuspended = suspendNotification;
        }

        /// <summary>
        /// Indicates whether the notification events has been suspended.
        /// </summary>
        public bool IsNotificationSuspended { get; private set; }

#pragma warning disable 693
        private static string GetPropertyName<T>(Expression<Func<T, object>> expression)
#pragma warning restore 693
        {
            // Get the property
            var propertyExpression = GetMemberExpression(expression);
            if (propertyExpression == null)
                throw new InvalidOperationException("You must specify a property");

            // Property name
            return propertyExpression.Member.Name;
        }

#pragma warning disable 693
        private static MemberExpression GetMemberExpression<T>(Expression<Func<T, object>> expression)
#pragma warning restore 693
        {
            // Default expression
            MemberExpression memberExpression = null;
            // Convert
            switch (expression.Body.NodeType)
            {
                case ExpressionType.Convert:
                    {
                        var body = (UnaryExpression)expression.Body;
                        memberExpression = body.Operand as MemberExpression;
                    }
                    break;
                case ExpressionType.MemberAccess:
                    memberExpression = expression.Body as MemberExpression;
                    break;
            }
            // Not a member access
            if (memberExpression == null)
                throw new ArgumentException("Not a member access", "expression");
            // Return the member expression
            return memberExpression;
        }

        #endregion

        #region IDirtyable Implementation

        public bool IsDirty { get; private set; }
       
        public void SetIsDirty(bool isDirty)
        {
            IsDirty = isDirty;
        }

        #endregion
    }

   
}
