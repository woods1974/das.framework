﻿namespace Das.Framework.Windows.Data.Interfaces
{
    /// <summary>
    /// Interface that defines if an object is busy.
    /// </summary>
    public interface IBusy
    {
        /// <summary>
        /// Returns true if the object is busy.
        /// </summary>
        bool IsBusy { get; }

        /// <summary>
        /// Set the busy state of the object.
        /// </summary>
        /// <param name="isBusy"></param>
        void SetIsBusy(bool isBusy);
    }
}
