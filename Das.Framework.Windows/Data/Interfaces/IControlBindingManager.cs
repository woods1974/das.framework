using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace Das.Framework.Windows.Data.Interfaces
{
    public interface IControlBindingManager<T> where T : Control
    {
        /// <summary>
        /// Bind a control property to a data member.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember) where TControl : Control;

        /// <summary>
        /// Bind a control property to a data member and set the data source update mode.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        /// <param name="dataSourceUpdateMode">The data source update mode</param>
        void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember, DataSourceUpdateMode dataSourceUpdateMode) where TControl : Control;

        /// <summary>
        /// Bind a control property to a data member and set both the data source and control update modes.
        /// </summary>
        /// <typeparam name="TControl">type of control</typeparam>
        /// <param name="control">Control to bind</param>
        /// <param name="property">Property name on the control to bind</param>
        /// <param name="dataMember">The data member name on the data source to bind</param>
        /// <param name="dataSourceUpdateMode">The data source update mode</param>
        /// <param name="controlUpdateMode">The control update mode</param>
        void Bind<TControl>(Control control, Expression<Func<TControl, object>> property, Expression<Func<T, object>> dataMember, DataSourceUpdateMode dataSourceUpdateMode, ControlUpdateMode controlUpdateMode) where TControl : Control;

        /// <summary>
        /// Binding Data Source.
        /// </summary>
        T DataSource { get; }
        
        /// <summary>
        /// Indicates whether the notification events has been suspended.
        /// </summary>
        bool IsNotificationSuspended { get; }

        event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Implement this method to cycle through specified properties calling the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyNames">List of property names</param>
        void NotifyPropertyChanged(params string[] propertyNames);

        /// <summary>
        /// Sets the suspended state of the change notification.
        /// When true, will disable all property changed notification events.
        /// </summary>
        /// <param name="suspendNotification"></param>
        void SetSuspendNotification(bool suspendNotification);
    }
}