﻿namespace Das.Framework.Windows.Data.Interfaces
{
    /// <summary>
    /// Interface that defines the state of a model, view or presenter.
    /// </summary>
    public interface IDirtyable
    {
        /// <summary>
        /// Indicates whether a change to a property has occurred.
        /// </summary>
        bool IsDirty { get; }

        /// <summary>
        /// Sets the state of the IsDirty property.
        /// </summary>
        /// <param name="isDirty"></param>
        void SetIsDirty(bool isDirty);
    }
}
