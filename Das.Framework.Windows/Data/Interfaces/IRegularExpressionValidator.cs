﻿using System.Text.RegularExpressions;

namespace Das.Framework.Windows.Data.Interfaces
{
    public interface IRegularExpressionValidator
    {
        /// <summary>
        /// When enabled, restricts the input based on the regular expression.
        /// </summary>
        bool RegexEnabled { get; set; }

        /// <summary>
        /// The regular expression to evaluate the input.
        /// </summary>
        string RegularExpression { get; set; }

        /// <summary>
        /// True when the text of the control matches the regular expression.
        /// </summary>
        bool RegexMatched { get; }

        /// <summary>
        /// Sets the regular expression option.
        /// </summary>
        RegexOptions RegexOptions { get; set; }
    }
}
