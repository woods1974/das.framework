﻿using System;
using System.Collections.ObjectModel;

namespace Das.Framework.Windows.Data.Interfaces
{
    /// <summary>
    /// Interface for managing validation rules.
    /// </summary>
    public interface IValidationRepository
    {
        /// <summary>
        /// Validates all rules withing the repository.
        /// </summary>
        void Validate();

        /// <summary>
        /// Returns true when one or more validation rules have failed.
        /// </summary>
        bool HasExceptions { get; }

        /// <summary>
        /// Clears all validation exceptions.
        /// </summary>
        void ClearExceptions();

        /// <summary>
        /// Add a validation rule to the repository.
        /// </summary>
        /// <param name="rule">The rule to be evaluated.</param>
        /// <param name="exceptionMessage">The exception message to return when the rule fails.</param>
        void AddRule(Func<bool> rule, string exceptionMessage);

        /// <summary>
        /// Collection of exceptions raised during the validation process.
        /// </summary>
        ObservableCollection<IValidationResult> Exceptions { get; }

        /// <summary>
        /// Outputs all exceptions to string.
        /// </summary>
        /// <returns></returns>
        string ToString();
    }
}
