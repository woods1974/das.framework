﻿namespace Das.Framework.Windows.Data.Interfaces
{
    /// <summary>
    /// Interface that handles the validation message result.
    /// </summary>
    public interface IValidationResult
    {
        /// <summary>
        /// Resulting message to be shown.
        /// </summary>
        string Message { get; set; }
    }
}
