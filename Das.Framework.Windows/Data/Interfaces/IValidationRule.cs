using System;

namespace Das.Framework.Windows.Data.Interfaces
{
    /// <summary>
    /// Interface for maintaining the rule to be evaluated.
    /// </summary>
    public interface IValidationRule
    {
        /// <summary>
        /// The condition to be evaluated during the validatin process.
        /// Returns true, when the condition is met.
        /// </summary>
        Func<bool> Condition { get; set; }

        /// <summary>
        /// The reason for why the validation had failed. 
        /// </summary>
        string ExceptionMessage { get; set; }
    }
}