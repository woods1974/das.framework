﻿using System.Text.RegularExpressions;
using Das.Framework.Windows.Data.Interfaces;

namespace Das.Framework.Windows.Data
{
    public static class RegularExpressionValidator
    {
        public static bool IsValid(string input, IRegularExpressionValidator regularExpressionValidator)
        {
            return regularExpressionValidator != null && !string.IsNullOrWhiteSpace(input) && 
                Regex.IsMatch(input,regularExpressionValidator.RegularExpression, regularExpressionValidator.RegexOptions);
        }
    }
}
