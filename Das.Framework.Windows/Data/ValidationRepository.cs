using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Das.Framework.Windows.Data.Interfaces;

namespace Das.Framework.Windows.Data
{
    /// <summary>
    /// Class for managing validation rules.
    /// </summary>
    public class ValidationRepository : IValidationRepository
    {
        private IList<IValidationRule> _validationRule;

        /// <summary>
        /// Class for managing validation rules.
        /// </summary>
        public ValidationRepository()
        {
            Exceptions = new ObservableCollection<IValidationResult>();
        }

        /// <summary>
        /// Add a validation rule to the repository.
        /// </summary>
        /// <param name="rule">The rule to be evaluated.</param>
        /// <param name="exceptionMessage">The exception message to return when the rule fails.</param>
        public void AddRule(Func<bool> rule, string exceptionMessage)
        {
            Rules.Add(new ValidationRule() { Condition = rule, ExceptionMessage = exceptionMessage });
        }

        /// <summary>
        /// Validates all rules withing the repository.
        /// </summary>
        public void Validate()
        {
            ClearExceptions();
            foreach (var rule in Rules.Where(rule => rule.Condition.Invoke()))
                Exceptions.Add(new ValidationResult() { Message = rule.ExceptionMessage });
        }

        /// <summary>
        /// Collection of exceptions raised during the validation process.
        /// </summary>
        public ObservableCollection<IValidationResult> Exceptions { get; private set; }

        /// <summary>
        /// Returns true when one or more validation rules have failed.
        /// </summary>
        public bool HasExceptions
        {
            get
            {
                return Exceptions.Count > 0;
            }
        }

        /// <summary>
        /// Clears all validation exceptions.
        /// </summary>
        public void ClearExceptions()
        {
            Exceptions.Clear();
        }

        private IList<IValidationRule> Rules
        {
            get
            {
                return _validationRule ?? (_validationRule = new List<IValidationRule>());
            }
        }


        /// <summary>
        /// Outputs all exceptions to string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (!HasExceptions)
            {
                return string.Empty;
            }

            var stringBuilder = new StringBuilder();
            foreach (var exception in Exceptions)
            {
                stringBuilder.AppendLine(exception.Message);
            }

            return stringBuilder.ToString();
        }

    }
}