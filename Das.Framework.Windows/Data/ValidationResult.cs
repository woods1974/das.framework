using Das.Framework.Core.Notification;
using Das.Framework.Windows.Data.Interfaces;

namespace Das.Framework.Windows.Data
{
    /// <summary>
    /// Class that contains the validation message result.
    /// </summary>
    public class ValidationResult : ChangeNotification<IValidationResult>, IValidationResult
    {

        private string _message;

        /// <summary>
        /// Resulting message to be shown.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message != value)
                {
                    _message = value;
                    NotifyPropertyChanged(vm => vm.Message);
                }
            }
        }
    }
}