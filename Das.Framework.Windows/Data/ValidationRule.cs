using System;
using Das.Framework.Windows.Data.Interfaces;

namespace Das.Framework.Windows.Data
{
    /// <summary>
    /// Class to maintain the rule to be evaluated.
    /// </summary>
    public class ValidationRule : IValidationRule
    {
        /// <summary>
        /// The condition to be evaluated during the validatin process.
        /// Returns true, when the condition is met.
        /// </summary>
        public Func<bool> Condition { get; set; }

        /// <summary>
        /// The reason for why the validation had failed. 
        /// </summary>
        public string ExceptionMessage { get; set; }
    }
}