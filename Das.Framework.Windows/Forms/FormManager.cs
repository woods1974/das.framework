﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Das.Framework.Windows.Forms.Interfaces;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Metro;

namespace Das.Framework.Windows.Forms
{
    /// <summary>
    /// Form Manager Class that managers the registration and events of a form.
    /// </summary>
    public static class FormManager
    {
        private static readonly Dictionary<string, Form> ActiveForms = new Dictionary<string, Form>();

        /// <summary>
        /// Register a form defined by the Form Parameters.
        /// </summary>
        /// <param name="parameters">Parameters that define the layout and display.</param>
        /// <returns>Returns the newly registered form without showing.</returns>
        public static Form RegisterForm(FormParameters parameters)
        {
            return parameters.IsMetroForm ? CreateMetroForm(parameters) : CreateForm(parameters);
        }

        /// <summary>
        ///  Register and Show the form based on the parameters provided.
        /// </summary>
        /// <param name="parameters">Form Parameters that define the form layout and display.</param>
        /// <returns>Returns the registered form.</returns>
        public static Form ShowForm(FormParameters parameters)
        {
            var form = RegisterForm(parameters);

            if (parameters.IsModal)
            {
                form.ShowDialog();
                return form;
            }

            form.Show();
            return form;
        }

        /// <summary>
        /// Show a Metro Style MessageBox.
        /// </summary>
        /// <param name="parameters">Metro Style MessageBox Parameters</param>
        public static void ShowMessageBox(IMessageBoxParameters parameters)
        {
            if (parameters.IsMetro)
                StyleManager.Style = eStyle.Metro;

            MessageBoxEx.DefaultStartPosition = parameters.DefaultStartPosition;
            MessageBoxEx.ButtonsDividerVisible = parameters.ButtonDividerVisible;
            MessageBoxEx.MessageBoxTextColor = parameters.TextColor;
            parameters.Result = MessageBoxEx.Show(parameters.Text, parameters.Caption,
                                    parameters.Buttons, parameters.Icon, parameters.DefaultButton);
        }

        private static StyleManager _styleManager = new StyleManager();

        /// <summary>
        /// Applies the Color to any registered Metro Form.
        /// Use the OverrideMetroColorStyle property in FormParameters to override this default
        /// when registering a Metro Form.
        /// </summary>
        public static StyleManager StyleManager
        {
            get { return _styleManager; }
            set { _styleManager = value; }
        }

        private static MetroForm CreateMetroForm(FormParameters parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters");

            if (string.IsNullOrEmpty(parameters.Id))
                throw new ArgumentNullException("parameters.Id");

            MetroForm form;

            if (TryGetForm(parameters.Id, out form))
            {
                form.Activate();
                return form;
            }

            form = new MetroForm()
            {
                FormBorderStyle = FormBorderStyle.Sizable,
                Text = parameters.Text,
                ShowIcon = parameters.ShowIcon
            };
             
            if (parameters.Icon != null)
                form.Icon = Icon.FromHandle(parameters.Icon.GetHicon());

            // Form Size and start position settings 
            form.Width = parameters.FormSize.Width;
            form.Height = parameters.FormSize.Height;
            form.StartPosition = parameters.FormStartPosition;
            form.FormBorderStyle = parameters.FormBorderStyle;

            // Form control feature settings
            form.MaximizeBox = parameters.MaximizeBox;
            form.MinimizeBox = parameters.MinimizeBox;
            form.ControlBox = parameters.ControlBox;


            if (parameters.ContentControl != null)
                form.Controls.Add(parameters.ContentControl.Invoke());

            if (form.Controls.Count > 0 && form.Controls[0] != null)
                form.Controls[0].Dock = DockStyle.Fill;

            form.Tag = parameters;
            form.WindowState = parameters.WindowState;

            SubscribeToEvents(form);
            ActiveForms.Add(parameters.Id, form);
            return form;
        }

        private static Form CreateForm(IFormParameters parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters");

            if (string.IsNullOrEmpty(parameters.Id))
                throw new ArgumentNullException("parameters.Id");

            Form form;

            if (TryGetForm(parameters.Id, out form))
            {
                form.Activate();
                return form;
            }

            form = new Form
            {
                Text = parameters.Text,
                ShowIcon = parameters.ShowIcon
            };

            if (parameters.Icon != null)
                form.Icon = Icon.FromHandle(parameters.Icon.GetHicon());

            // Form Size and start position settings 
            form.Width = parameters.FormSize.Width;
            form.Height = parameters.FormSize.Height;
            form.StartPosition = parameters.FormStartPosition;
            form.FormBorderStyle = parameters.FormBorderStyle;

            // Form control feature settings
            form.MaximizeBox = parameters.MaximizeBox;
            form.MinimizeBox = parameters.MinimizeBox;
            form.ControlBox = parameters.ControlBox;

            if (parameters.ContentControl != null)
                form.Controls.Add(parameters.ContentControl.Invoke());

            if (form.Controls.Count > 0 && form.Controls[0] != null)
                form.Controls[0].Dock = DockStyle.Fill;

            form.Tag = parameters;
            form.WindowState = parameters.WindowState;

            SubscribeToEvents(form);
            ActiveForms.Add(parameters.Id, form);
            return form;
        }

        private static void FormShownEvent(object sender, EventArgs e)
        {
            var form = (Form)sender;

            if (form.Controls.Count == 0)
                return;

            var view = form.Controls[0] as IView;

            if (view != null)
                view.RaiseOnViewLoaded();
        }

        private static void FormLoadEvent(object sender, EventArgs e)
        {
            var form = (Form)sender;

            if (form.Controls.Count == 0)
                return;

            if (!(form.Controls[0] is IView))
                return;

            var view = form.Controls[0] as IView;
            view.RaiseOnViewLoad();
        }

        private static void SubscribeToEvents(Form form)
        {
            form.Load += FormLoadEvent;
            form.Shown += FormShownEvent;
            form.FormClosing += BeforeFormClosingEvent;
            form.Closed += FormClosedEvent;
        }

        private static void FormClosedEvent(object sender, EventArgs e)
        {
            var form = (Form)sender;
            if (form.Tag is FormParameters)
                ActiveForms.Remove(((FormParameters)form.Tag).Id);
        }

        /// <summary>
        /// Returns the parameters associated with the form.
        /// </summary>
        /// <param name="form">The form that contains Form Parameters.</param>
        /// <param name="parameters">The parameters returned from the form.</param>
        /// <returns>True, when the parameters are successfully returned.</returns>
        public static bool TryGetParameters(Form form, out FormParameters parameters)
        {
            if (!(form.Tag is FormParameters))
            {
                parameters = null;
                return false;
            }
            var tag = form.Tag as FormParameters;
            parameters = tag;
            return true;
        }

        /// <summary>
        /// Close a specific form by it's Identifer in Form Manager.
        /// </summary>
        /// <param name="id">Form Identifer that is registered with Form Manager</param>
        public static void Close(string id)
        {
            Form form;
            if (!TryGetForm(id, out form)) return;

            UnsubscribeToEvents(form);
            var formParameters = form.Tag as FormParameters;

            if (formParameters != null)
                ActiveForms.Remove(formParameters.Id);

            form.Close();
        }

        private static void BeforeFormClosingEvent(object sender, FormClosingEventArgs e)
        {
            var form = sender as Form;

            if (form == null)
                return;

            if (form.Controls.Count > 0 && form.Controls[0] is IView)
            {
                var view = form.Controls[0] as IView;
                var viewArgs = new OnViewUnloadEventArgs();
                view.RaiseOnViewUnload(viewArgs);
                e.Cancel = viewArgs.Cancel;
            }

            if (!(form.Tag is FormParameters))
                return;

            var parameters = form.Tag as FormParameters;
            if (!parameters.IsParentForm) return;

            foreach (var activeForm in ActiveForms.Where(activeForm => activeForm.Value != form).ToList())
                activeForm.Value.Close();
        }

        private static void UnsubscribeToEvents(Form form)
        {
            form.Load -= FormLoadEvent;
            form.Shown -= FormShownEvent;
            form.FormClosing -= BeforeFormClosingEvent;
            form.Closed -= FormClosedEvent;
        }

        /// <summary>
        /// Returns a specific type of form based on the forms Identifier.
        /// Usually the form can be of type Form or MetroForm.
        /// </summary>
        /// <param name="id">The forms Identifier</param>
        /// <param name="form">The form returned by the Form Manager. Returns null if not found.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>True, if Form Manager has successfully returned the type of form.</returns>
        public static bool TryGetForm<T>(string id, out T form) where T : class
        {
            if (ActiveForms.ContainsKey(id))
            {
                form = ActiveForms[id] as T;
                return true;
            }

            form = null;
            return false;
        }
    }
}
