﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Windows.Forms.Interfaces;

namespace Das.Framework.Windows.Forms
{
    /// <summary>
    /// A class that defines how a form should be created.
    /// </summary>
    public class FormParameters : IFormParameters
    {
        /// <summary>
        /// A class that defines how a form should be created.
        /// </summary>
        /// <param name="applicationContext"></param>
        public FormParameters(IApplicationContext applicationContext)
        {
            Context = applicationContext;
            InitialiseParameters();
        }

        private void InitialiseParameters()
        {
            Id = Guid.NewGuid().ToString();
            FormStartPosition = FormStartPosition.CenterParent;
            ShowIcon = true;
            FormBorderStyle = FormBorderStyle.Sizable;
            MinimizeBox = true;
            MaximizeBox = true;
            ControlBox = true;
        }

        #region Implementation of IFormParameters

        /// <summary>
        /// Form Identifier used by Form Manager.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Content such as a View (UserControl) that can be invoked by the Form Manager.
        /// </summary>
        public Func<UserControl> ContentControl { get; set; }

        /// <summary>
        /// Application Context that managers the application. 
        /// </summary>
        public IApplicationContext Context { get; set; }

        /// <summary>
        /// Icon associated with the form.
        /// </summary>
        public Bitmap Icon { get; set; }

        /// <summary>
        /// When true, will show the form Icon. Note that the Icon is not shown in Metro Forms.
        /// </summary>
        public bool ShowIcon { get; set; }

        /// <summary>
        /// Specifies the initial start position of the form.
        /// </summary>
        public FormStartPosition FormStartPosition { get; set; }

        /// <summary>
        /// Set or get the Width and Height of the form.
        /// </summary>
        public Size FormSize { get; set; }

        /// <summary>
        /// When true, show the minimze button.
        /// </summary>
        public bool MinimizeBox { get; set; }

        /// <summary>
        /// When true, show the maximized button.
        /// </summary>
        public bool MaximizeBox { get; set; }

        /// <summary>
        /// When true, enable the control box context menu.
        /// </summary>
        public bool ControlBox { get; set; }

        /// <summary>
        /// Indicate whether the form should be modal.
        /// </summary>
        public bool IsModal { get; set; }

        /// <summary>
        /// Indiciate whether the form registered should be a Metro Form.
        /// </summary>
        public bool IsMetroForm { get; set; }

        /// <summary>
        /// Specifies the Border Style of form.
        /// </summary>
        public FormBorderStyle FormBorderStyle { get; set; }

        /// <summary>
        /// Indiciate how a form should be displayed.
        /// </summary>
        public FormWindowState WindowState { get; set; }
        
        /// <summary>
        /// When true, will use the Form Parameters MetroColorStyle instead of the Form Manager MetroColorStyle.
        /// </summary>
        public bool OverrideMetroColorStyle { get; set; }

        /// <summary>
        /// Set this property to true when if this is the Application Form (Shell).
        /// The property is used to indicate ownership of child forms within Form Manager.
        /// </summary>
        public bool IsParentForm { get; set; }

        /// <summary>
        /// Form Title.
        /// </summary>
        public string Text { get; set; }

        #endregion
    }
}
