﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Das.Framework.Windows.Forms.Interfaces
{
    /// <summary>
    /// Form Parameters Interface.
    /// </summary>
    public interface IFormParameters
    {
        /// <summary>
        /// Form Identifier used by Form Manager.
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Content such as a View (UserControl) that can be invoked by the Form Manager.
        /// </summary>
        Func<UserControl> ContentControl { get; set; }
        /// <summary>
        /// Application Context that managers the application. 
        /// </summary>
        IApplicationContext Context { get; set; }
        /// <summary>
        /// Icon associated with the form.
        /// </summary>
        Bitmap Icon { get; set; }
        /// <summary>
        /// When true, will show the form Icon. Note that the Icon is not shown in Metro Forms.
        /// </summary>
        bool ShowIcon { get; set; }
        /// <summary>
        /// Specifies the initial start position of the form.
        /// </summary>
        FormStartPosition FormStartPosition { get; set; }
        /// <summary>
        /// The Width and height of the form.
        /// </summary>
        Size FormSize { get; set; }
        /// <summary>
        /// When true, show the minimze button.
        /// </summary>
        bool MinimizeBox { get; set; }
        /// <summary>
        /// When true, show the maximized button.
        /// </summary>
        bool MaximizeBox { get; set; }
        /// <summary>
        /// When true, enable the control box context menu.
        /// </summary>
        bool ControlBox { get; set; }
        /// <summary>
        /// Indicate whether the form should be modal.
        /// </summary>
        bool IsModal { get; set; }
        /// <summary>
        /// Indiciate whether the form registered should be a Metro Form.
        /// </summary>
        bool IsMetroForm { get; set; }
        /// <summary>
        /// Specifies the Border Style of form.
        /// </summary>
        FormBorderStyle FormBorderStyle { get; set; }
        /// <summary>
        /// Indiciate how a form should be displayed.
        /// </summary>
        FormWindowState WindowState { get; set; }
        
        /// <summary>
        /// When true, will use the Form Parameters MetroColorStyle instead of the Form Manager MetroColorStyle.
        /// </summary>
        bool OverrideMetroColorStyle { get; set; }
        /// <summary>
        /// Set this property to true when if this is the Application Form (Shell).
        /// The property is used to indicate ownership of child forms within Form Manager.
        /// </summary>
        bool IsParentForm { get; set; }
        /// <summary>
        /// Form Title.
        /// </summary>
        string Text { get; set; }
    }
}
