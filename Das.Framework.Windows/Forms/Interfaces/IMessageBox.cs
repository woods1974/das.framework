﻿namespace Das.Framework.Windows.Forms.Interfaces
{
    public interface IMessageBox
    {
        void ShowMessageBox(IMessageBoxParameters parameters);
    }
}
