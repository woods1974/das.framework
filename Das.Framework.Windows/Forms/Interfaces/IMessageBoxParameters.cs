using System.Drawing;
using System.Windows.Forms;

namespace Das.Framework.Windows.Forms.Interfaces
{
    /// <summary>
    /// Interface that defines how a message box should be created.
    /// </summary>
    public interface IMessageBoxParameters
    {
        /// <summary>
        /// The body of the message.
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// The Caption / Title of the message box.
        /// </summary>
        string Caption { get; set; }

        /// <summary>
        /// Forces the message box to be at the to most position.
        /// </summary>
        bool ToMost { get; set; }

        /// <summary>
        /// Indicates whether a divider should be shown between the message and the buttons.
        /// </summary>
        bool ButtonDividerVisible { get; set; }

        /// <summary>
        /// Defines the buttons to be shown.
        /// </summary>
        MessageBoxButtons Buttons { get; set; }

        /// <summary>
        /// Determines the icon that is displayed.
        /// </summary>
        MessageBoxIcon Icon { get; set; }
        /// <summary>
        /// Specifies the return value from the message box.
        /// </summary>
        DialogResult Result { get; set; }
        /// <summary>
        /// The default message box button that will be set.
        /// </summary>
        MessageBoxDefaultButton DefaultButton { get; set; }
        /// <summary>
        /// Position of the message box.
        /// </summary>
        FormStartPosition DefaultStartPosition { get; set; }
        /// <summary>
        /// Fore Color of the text to be rendered.
        /// </summary>
        Color TextColor { get; set; }
        /// <summary>
        /// Determines whether the messagebox should be render in Metro Style.
        /// </summary>
        bool IsMetro { get; set; }
    }
}