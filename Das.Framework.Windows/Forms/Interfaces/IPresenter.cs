﻿using System;
using Das.Framework.Core.Notification;
using Das.Framework.Windows.Data.Interfaces;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Forms.Interfaces
{
    public interface IPresenter<TView> : IBusy, IDirtyable, IDisposable where TView : IView
    {
        /// <summary>
        /// Raised when the presenter has loaded.
        /// </summary>
        event EventHandler OnPresenterLoaded;
        /// <summary>
        /// Raised when the busy state of the presenter has changed.
        /// </summary>
        event EventHandler<PresenterIsBusyChangedEventArgs> IsBusyChanged;
        /// <summary>
        /// Rised when the stat of the presenters data has changed.
        /// </summary>
        event EventHandler<PresenterIsDirtyChangedEventArgs> IsDirtyChanged;
        /// <summary>
        /// Invokes the Busy Changed Event.
        /// </summary>
        /// <param name="isBusy"></param>
        void RaisePresenterBusyChanged(PresenterIsBusyChangedEventArgs isBusy);
        /// <summary>
        /// Invokes the Is Dirty Changed Event.
        /// </summary>
        /// <param name="isDirty"></param>
        void RaiseIsDirtyChanged(PresenterIsDirtyChangedEventArgs isDirty);
        /// <summary>
        /// Context that is used by the application.
        /// </summary>
        IApplicationContext ApplicationContext { get; }
        /// <summary>
        /// Used to manage the bindings between a command and it's control (button).
        /// </summary>
        ICommandManager CommandManager { get; }
    }
}
