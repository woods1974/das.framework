﻿using System;

namespace Das.Framework.Windows.Forms.Interfaces
{
    /// <summary>
    /// Interface for managing Views in MVP
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Triggered when the view is being initialised.
        /// </summary>
        event EventHandler<EventArgs> OnViewLoad;
        /// <summary>
        /// Triggered when the view has fully initialised.
        /// This event will also trigger the presenter to intialise.
        /// </summary>
        event EventHandler<EventArgs> OnViewLoaded;
        /// <summary>
        /// Triggers the view and it's presenter to uninitialise.
        /// </summary>
        event EventHandler<OnViewUnloadEventArgs> OnViewUnload;
        /// <summary>
        ///  Triggered when the view has fully uninitialised.
        /// </summary>
        event EventHandler<EventArgs> OnViewUnLoaded;

        /// <summary>
        /// Raises the On View Loaded event.
        /// </summary>
        void RaiseOnViewLoaded();
        /// <summary>
        /// Raises the On View Unload event.
        /// </summary>
        void RaiseOnViewUnload(OnViewUnloadEventArgs e);
        /// <summary>
        /// Raises the On View Load event.
        /// </summary>
        void RaiseOnViewLoad();

        /// <summary>
        /// Raises the On View Unloaded event.
        /// </summary>
        void RaiseOnViewUnLoaded();
    }
}
