using System.Drawing;
using System.Windows.Forms;
using Das.Framework.Windows.Forms.Interfaces;

namespace Das.Framework.Windows.Forms
{
    public class MessageBoxParameters : IMessageBoxParameters
    {
        public MessageBoxParameters()
        {
            ToMost = true;
            DefaultStartPosition = FormStartPosition.CenterParent;
            TextColor = Color.Black;
            IsMetro = true;
        }

        public string Text { get; set; }
        public string Caption { get; set; }
        public MessageBoxButtons Buttons { get; set; }
        public MessageBoxIcon Icon { get; set; }
        public DialogResult Result { get; set; }
        public MessageBoxDefaultButton DefaultButton { get; set; }
        public bool ToMost { get; set; }
        public bool ButtonDividerVisible { get; set; }
        public FormStartPosition DefaultStartPosition { get; set; }
        public Color TextColor { get; set; }
        public bool IsMetro { get; set; }
    }
}