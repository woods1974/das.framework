﻿using System;

namespace Das.Framework.Windows.Forms
{
    /// <summary>
    /// Class to determine whether the view should unload fully.
    /// Use this to prevent unsaved changes made by the view and presenter.
    /// </summary>
    public class OnViewUnloadEventArgs : EventArgs
    {
        /// <summary>
        /// Class to determine whether the view should unload fully.
        /// </summary>
        public OnViewUnloadEventArgs()
        { }
        /// <summary>
        /// Class to determine whether the view should unload fully.
        /// When true, will prevent the view from unloading.
        /// </summary>
        public OnViewUnloadEventArgs(bool cancel)
        {
            Cancel = cancel;
        }
        /// <summary>
        /// When true, will prevent the view from unloading.
        /// </summary>
        public bool Cancel { get; set; }
        /// <summary>
        /// To hold addition information.
        /// </summary>
        public object Tag { get; set; }
    }
}
