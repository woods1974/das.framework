using System;
using System.ComponentModel;
using Das.Framework.Windows.Data;
using Das.Framework.Windows.Data.Interfaces;
using Das.Framework.Windows.Forms.Interfaces;
using Das.Framework.Windows.Input;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Forms
{
    /// <summary>
    /// Class to provide data to the view.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public abstract class Presenter<TView, TPayload, TResult> : IPresenter<TView> where TView : IView
    {
        private ITaskCommand<TResult> _taskCommand;
        private ICommandManager _commandManager;
        private IValidationRepository _validationRepository;

        protected Presenter(TView view, IApplicationContext applicationContext)
        {
            ApplicationContext = applicationContext;
            View = view;
            InitialisePresenter();
        }

        protected Presenter(TView view, IApplicationContext applicationContext, TPayload payload)
        {
            ApplicationContext = applicationContext;
            View = view;
            Payload = payload;
            InitialisePresenter();
        }

        private void InitialisePresenter()
        {
            SubscribeToEvents();
            BackgroundTask.Action = () => LoadFromData(Payload);
        }

        /// <summary>
        /// Performs background operations.
        /// Generally used by the Presenter on initialise for gathering data for the view.
        /// </summary>
        public ITaskCommand<TResult> BackgroundTask
        {
            get { return _taskCommand ?? (_taskCommand = new TaskCommand<TResult>(true)); }
        }

        /// <summary>
        /// Provides input validation against the view and it's presenter.
        /// </summary>
        protected IValidationRepository ValidationRepository
        {
            get
            {
                return _validationRepository ?? (_validationRepository = new ValidationRepository());
            }
        }

        /// <summary>
        /// The payload used when the presenter was first initialised.
        /// </summary>
        protected TPayload Payload { get; private set; }

        /// <summary>
        /// Triggered when the binding manager detects a property value change.
        /// Use this method to evaluate the property changed value.
        /// </summary>
        /// <param name="sender">object that raised the event.</param>
        /// <param name="e">The Property Changed Arguments such as the propetry name that raised the event.</param>
        protected abstract void PropertyChangedEvent(object sender, PropertyChangedEventArgs e);

        /// <summary>
        /// Method to register event handlers and event subscriptions.
        /// </summary>
        protected virtual void SubscribeToEvents()
        {
            BackgroundTask.TaskCompleted += TaskCompletedEvent;
            BackgroundTask.TaskCancelled += TaskCancelledEvent;
            View.OnViewLoaded += ViewLoadedEvent;
            View.OnViewUnload += ViewUnloadEvent;
        }
        
        private void ViewUnloadEvent(object sender, object e)
        {
            Dispose(true);
        }

        private void ViewLoadedEvent(object sender, object e)
        {
            SetIsBusy(true);
            BackgroundTask.Execute();
        }

        private void TaskCancelledEvent(object sender, TaskCancelledResult e)
        {
            PresenterLoad(BackgroundTask.Result);
        }

        private void TaskCompletedEvent(object sender, TaskCompletedResult e)
        {
            if (e.Exception != null)
            {
                SetIsBusy(false);
                throw e.Exception;
            }
            PresenterLoad(BackgroundTask.Result);
        }

        private void PresenterLoad(TResult result)
        {
            BindFromData(result);
            SetIsBusy(false);
            RaiseOnPresenterLoaded();
        }

        /// <summary>
        /// Method to unregister events and subscriptions.
        /// </summary>
        protected virtual void UnsubscribeToEvents()
        {
            if (ApplicationContext != null && ApplicationContext.EventBroker != null)
                ApplicationContext.EventBroker.Unsubscribe(this);

            BackgroundTask.TaskCompleted -= TaskCompletedEvent;
            BackgroundTask.TaskCancelled -= TaskCancelledEvent;
            View.OnViewLoaded -= ViewLoadedEvent;
            View.OnViewUnload -= ViewUnloadEvent;
        }

        /// <summary>
        /// Invokes a background task to load data.
        /// </summary>
        /// <param name="payload">The payload used by LoadFromData.</param>
        /// <returns>Returns the resulting data to be passed into the BindFromData method.</returns>
        protected abstract TResult LoadFromData(TPayload payload);

        /// <summary>
        ///  To bind data returned from LoadFromData method.
        ///  This method is invoked after the LoadFromData has successully completed.
        /// </summary>
        /// <param name="result">resulting data returned from LoadFromData</param>
        protected abstract void BindFromData(TResult result);

        /// <summary>
        /// Raises the Presenters on loaded event.
        /// </summary>
        protected void RaiseOnPresenterLoaded()
        {
            if (OnPresenterLoaded != null)
                OnPresenterLoaded.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// The View that used by the presenter.
        /// </summary>
        public TView View { get; private set; }

        #region Implementation of IPresenter

        /// <summary>
        /// Context that is used by the application.
        /// </summary>
        public IApplicationContext ApplicationContext { get; private set; }
        
        public event EventHandler OnPresenterLoaded;

        public event EventHandler<PresenterIsBusyChangedEventArgs> IsBusyChanged;

        public event EventHandler<PresenterIsDirtyChangedEventArgs> IsDirtyChanged;
        /// <summary>
        /// Used to manage the binding between a command and control.
        /// </summary>
        public ICommandManager CommandManager
        {
            get
            {
                return _commandManager ?? (_commandManager = new CommandManager());
            }
        }
        
        /// <summary>
        /// Raises the Presenters Busy Changed Event.
        /// </summary>
        /// <param name="isBusy">Indicate whether the Presenter is busy.</param>
        public void RaisePresenterBusyChanged(PresenterIsBusyChangedEventArgs isBusy)
        {
            if (IsBusyChanged != null)
                IsBusyChanged.Invoke(this, isBusy);
        }
        /// <summary>
        /// Raises the Is Dirty Changed Event.
        /// </summary>
        /// <param name="isDirty">Indicates whether the presenter is dirty.</param>
        public void RaiseIsDirtyChanged(PresenterIsDirtyChangedEventArgs isDirty)
        {
            if (IsDirtyChanged != null)
                IsDirtyChanged.Invoke(this, isDirty);
        }

        #endregion

        #region Implementation of IBusy

        /// <summary>
        /// Returns true if the object is busy.
        /// </summary>
        public bool IsBusy { get; private set; }

        /// <summary>
        /// Set the busy state of the object.
        /// </summary>
        /// <param name="isBusy"></param>
        public void SetIsBusy(bool isBusy)
        {
            IsBusy = isBusy;
            RaisePresenterBusyChanged(new PresenterIsBusyChangedEventArgs(isBusy));
        }

        #endregion

        #region Implementation of IDirtyable

        /// <summary>
        /// Indicates whether a change to a property has occurred.
        /// </summary>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// Sets the state of the IsDirty property.
        /// </summary>
        /// <param name="isDirty"></param>
        public void SetIsDirty(bool isDirty)
        {
            IsDirty = isDirty;
            RaiseIsDirtyChanged(new PresenterIsDirtyChangedEventArgs(isDirty));
        }

        #endregion

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                UnsubscribeToEvents();
                if (BackgroundTask != null)
                    BackgroundTask.Dispose();
            }
        }

        #endregion
    }
}