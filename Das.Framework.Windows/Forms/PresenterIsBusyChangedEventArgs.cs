﻿using System;

namespace Das.Framework.Windows.Forms
{
    public class PresenterIsBusyChangedEventArgs : EventArgs
    {
        public PresenterIsBusyChangedEventArgs()
        { }
        public PresenterIsBusyChangedEventArgs(bool isBusy)
        {
            IsBusy = isBusy;
        }
        public bool IsBusy { get; private set; }
    }
}
