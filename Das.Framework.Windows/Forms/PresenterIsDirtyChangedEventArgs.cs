using System;

namespace Das.Framework.Windows.Forms
{
    public class PresenterIsDirtyChangedEventArgs : EventArgs
    {
        public PresenterIsDirtyChangedEventArgs()
        { }

        public PresenterIsDirtyChangedEventArgs(bool isDirty)
        {
            IsDirty = isDirty;
        }

        public bool IsDirty { get; private set; }
    }
}