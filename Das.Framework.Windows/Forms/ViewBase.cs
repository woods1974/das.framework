﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Das.Framework.Windows.Forms.Interfaces;

namespace Das.Framework.Windows.Forms
{
    /// <summary>
    /// Base class for the creation and management of Views.
    /// </summary>
    public class ViewBase : UserControl, IView, ISupportInitialize
    {
        protected virtual void InitialiseView(IView view)
        {
            View = view;
            InitialiseViewBase();
        }

        /// <summary>
        /// Initialises the view, events, and bindings.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="applicationContext"></param>
        protected virtual void InitialiseView(IView view, IApplicationContext applicationContext)
        {
            View = view;
            ApplicationContext = applicationContext;
            InitialiseViewBase();
        }

        /// <summary>
        /// Initialises the view, events, and bindings.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="applicationContext"></param>
        /// <param name="payload"></param>
        protected virtual void InitialiseView(IView view, IApplicationContext applicationContext, object payload)
        {
            View = view;
            ApplicationContext = applicationContext;
            Payload = payload;
            InitialiseViewBase();
        }

        private void InitialiseViewBase()
        {
            BeginInit();
            SubscribeToEvents();
            BindControls();
            EndInit();
        }

        /// <summary>
        /// The winforms context used for the application.
        /// </summary>
        protected IApplicationContext ApplicationContext { get; private set; }

        /// <summary>
        /// The view that will represent the data to the form controls.
        /// </summary>
        protected IView View { get; private set; }

        protected override void Dispose(bool disposing)
        {
            if (!_isIntialising && disposing)
            {
                GC.SuppressFinalize(this);
                UnsubscribeToEvents();
            }
            base.Dispose(disposing);
        }

        #region Implementation of IView

        /// <summary>
        /// Triggered when the view is being initialised.
        /// </summary>
        public event EventHandler<EventArgs> OnViewLoad;
        /// <summary>
        /// Triggered when the view has fully initialised.
        /// This event will also trigger the presenter to intialise.
        /// </summary>
        public event EventHandler<EventArgs> OnViewLoaded;
        /// <summary>
        /// Triggers the view and it's presenter to uninitialise.
        /// </summary>
        public event EventHandler<OnViewUnloadEventArgs> OnViewUnload;
        /// <summary>
        ///  Triggered when the view has fully uninitialised.
        /// </summary>
        public event EventHandler<EventArgs> OnViewUnLoaded;

        /// <summary>
        /// Raises the On View Loaded event.
        /// </summary>
        public void RaiseOnViewLoaded()
        {
            if (!_isIntialising && OnViewLoaded != null)
                OnViewLoaded.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raises the On View Unload event.
        /// </summary>
        public void RaiseOnViewUnload(OnViewUnloadEventArgs e)
        {
            if (OnViewUnload == null) return;
            OnViewUnload.Invoke(this, e);
        }

        /// <summary>
        /// Raises the On View Load event.
        /// </summary>
        public void RaiseOnViewLoad()
        {
            if (!_isIntialising && OnViewLoad != null)
                OnViewLoad.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raises the On View Unloaded event.
        /// </summary>
        public void RaiseOnViewUnLoaded()
        {
            OnViewUnLoaded.Invoke(this, EventArgs.Empty);
        }

        #endregion

        /// <summary>
        /// Payload used by the view and it's presenter.
        /// </summary>
        public object Payload { get; set; }

        /// <summary>
        /// Subscribe to system and composite events.
        /// Invoked when the InitialiseView method is called.
        /// </summary>
        protected virtual void SubscribeToEvents()
        {
            OnViewUnload += OnViewUnloadEvent;
            OnViewLoaded += OnViewLoadEvent;
            Load += ViewBaseLoadEvent;
        }

        protected virtual void ViewBaseLoadEvent(object sender, EventArgs e)
        {}
        
        /// <summary>
        /// Triggred when the view is about to initialise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public virtual void OnViewLoadEvent(object sender, EventArgs args)
        { }

        /// <summary>
        /// Triggered when the state of the presenters dirty state changes.
        /// </summary>
        /// <param name="sender">object raising the event.</param>
        /// <param name="args">The dirty state of the presenter.</param>
        protected virtual void IsDirtyChangedEvent(object sender, PresenterIsDirtyChangedEventArgs args)
        { }

        /// <summary>
        /// Triggered when the busy state changes on the presenter.
        /// </summary>
        /// <param name="sender">object raising the event.</param>
        /// <param name="args">The busy state of the presenter.</param>
        protected virtual void IsBusyChangedEvent(object sender, PresenterIsBusyChangedEventArgs args)
        { }

        /// <summary>
        /// Provide an option to cancel the view from unloading.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void OnViewUnloadEvent(object sender, OnViewUnloadEventArgs args)
        { }

        /// <summary>
        /// Unsubscribe to system and composite events.
        /// Is invoked when WinformsViewBase is disposed.
        /// </summary>
        protected virtual void UnsubscribeToEvents()
        {
            if (ApplicationContext != null && ApplicationContext.EventBroker != null)
                ApplicationContext.EventBroker.Unsubscribe(this);

            OnViewUnload -= OnViewUnloadEvent;
            OnViewLoaded -= OnViewLoadEvent;
            Load -= ViewBaseLoadEvent;
        }

        /// <summary>
        /// Binds View properties to UI Controls such as buttons and textboxes.
        /// </summary>
        protected virtual void BindControls()
        { }

        #region Implementation of ISupportInitialize

        private bool _isIntialising;

        /// <summary>
        /// Signals the object that initialization is starting.
        /// </summary>
        public void BeginInit()
        {
            _isIntialising = true;
        }

        /// <summary>
        /// Signals the object that initialization is complete.
        /// </summary>
        public void EndInit()
        {
            _isIntialising = false;
        }

        #endregion
    }
}
