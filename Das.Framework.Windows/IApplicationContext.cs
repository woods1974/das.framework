﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Das.Framework.Core.Notification.Interfaces;
using Das.Framework.Windows.Input.Interfaces;
using Microsoft.Practices.Unity;

namespace Das.Framework.Windows
{
    /// <summary>
    /// Class that manages the Winforms Application Context.
    /// </summary>
    public interface IApplicationContext : IDisposable
    {
        /// <summary>
        /// Winforms Application Controller Id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Manages the event publishing and subscriptions within the winforms application.
        /// </summary>
        IEventBroker EventBroker { get; }
        /// <summary>
        /// Used to manage the bindings between a command and it's control (button).
        /// </summary>
        ICommandManager CommandManager { get; }

        /// <summary>
        /// Unity Dependency Injection Container
        /// </summary>
        IUnityContainer UnityContainer { get; }
    }
}
