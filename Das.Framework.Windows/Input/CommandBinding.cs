using System;
using System.Reflection;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Used to bind a single Command to a UI Control and it's event handler.
    /// </summary>
    public class CommandBinding : ICommandBinding
    {
        public CommandBinding(IDelegateCommand command, object commandParameter, object control, EventTrigger bindingType)
        {
            Command = command;
            CommandParameter = commandParameter;
            Control = control;
            EventTrigger = bindingType;
        }
        /// <summary>
        /// The command to be executed.
        /// </summary>
        public IDelegateCommand Command { get; set; }

        /// <summary>
        /// The parameter used during the execution process.
        /// </summary>
        public object CommandParameter { get; set; }

        /// <summary>
        /// The UI control of which the command is bounded to.
        /// </summary>
        public object Control { get; set; }

        /// <summary>
        /// Indicates which event should trigger the command execution.
        /// </summary>
        public EventTrigger EventTrigger { get; set; }

        /// <summary>
        /// The event handler of which the command is triggered to execute from.
        /// </summary>
        public Delegate EventHandler { get; set; }

        /// <summary>
        /// Used to discover whether the event requested is contained on the UI Control e.g. DoubleClick.
        /// </summary>
        public EventInfo Event { get; set; }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (Event != null && EventHandler != null && Control != null)
            {
                Event.RemoveEventHandler(Control, EventHandler);
            }
            Event = null;
            EventHandler = null;
            Control = null;
            Command = null;
            CommandParameter = null;
        }

        #endregion
    }
}