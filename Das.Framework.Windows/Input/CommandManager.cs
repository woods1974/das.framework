﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Manages the relationship between a UI Control and it's command.
    /// </summary>
    public class CommandManager : ICommandManager
    {
        /// <summary>
        /// Manages the relationship between a UI Control and it's command.
        /// </summary>
        public CommandManager()
        {
            Id = Guid.NewGuid();
            Bindings = new List<CommandBinding>();
        }

        private IList<CommandBinding> Bindings { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Bindings.Count == 0)
                    return;

                foreach (var binding in Bindings.Where(binding => binding.Control != null))
                {
                    binding.Command.CanExecuteChanged -= (CanExecuteChangedEvent);
                    binding.Dispose();
                }

                Bindings.Clear();
                Bindings = null;
            }
        }

        /// <summary>
        /// Bind the UI Control to the IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property.</param>
        /// <param name="command">DelegateCommand to bind.</param>
        public void Bind(object control, IDelegateCommand command)
        {
            Bind(control, EventTrigger.Click, command);
        }

        /// <summary>
        /// Bind the UI Control to the IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="parameter">The parameter used during the evaluation and execution of the command.</param>
        /// <param name="control">UI Control to bind to.</param>
        /// <param name="command">DelegateCommand to bind.</param>
        public void Bind(object control, IDelegateCommand command, object parameter)
        {
            Bind(control, EventTrigger.Click, command, parameter);
        }

        /// <summary>
        /// Bind the UI Control to the first instrance of a IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property</param>
        public void Bind(object control)
        {
            Bind(control, EventTrigger.Click);
        }

        /// <summary>
        /// Bind to the first IDelegateCommand property on the UI control.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <exception cref="NullReferenceException">Raised when no command is available.</exception>
        public void Bind(object control, EventTrigger eventTrigger)
        {
            var command = GetCommand(control);
            if (command == null)
                throw new NullReferenceException(string.Format("Missing IDelegateCommand on control {0}", control.GetType()));

            Bind(control, eventTrigger, command);
        }

        public Guid Id { get; private set; }

        /// <summary>
        /// Number of bindings contained by the command manager. 
        /// </summary>
        public int BindingCount
        {
            get { return Bindings.Count; }
        }

        /// <summary>
        /// Return an existing binding.
        /// </summary>
        /// <param name="control">Control that is registered with the command manager.</param>
        /// <returns>Returns an existing binding associated with the control.</returns>
        public ICommandBinding GetCommandBinding(object control)
        {
            return Bindings.FirstOrDefault(b => b.Control == control);
        }

        /// <summary>
        /// Returns the first IDeligateCommand contained on the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public IDelegateCommand GetCommand(object control)
        {
            return (from propertyInfo in control.GetType().GetProperties()
                    where propertyInfo.PropertyType == typeof(IDelegateCommand)
                    select (IDelegateCommand)propertyInfo.GetValue(control)).FirstOrDefault();
        }

        /// <summary>
        /// Bind the UI Control to the first instrance of a IDelegate Command property and setting 
        /// the default parameter.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property.</param>
        /// <param name="parameter">The parameter to be used during the exection.</param>
        public void Bind(object control, object parameter)
        {
            var command = GetCommand(control);
            if (command == null)
                throw new ArgumentNullException(string.Format("Missing IDelegateCommand on control {0}", control.GetType()));

            Bind(control, EventTrigger.Click, command, parameter);
        }

        /// <summary>
        /// Binds a single UI Control to a command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTriggerType">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        /// <param name="commandParameter">Command parameter used during the evaluation and execution of the command.</param>
        public void Bind(object control, EventTrigger eventTriggerType, IDelegateCommand command, object commandParameter)
        {
            if (command == null)
                return;

            SetBind(control, eventTriggerType, command, commandParameter);
        }

        /// <summary>
        /// Binds a single UI Control to a command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTriggerType">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        public void Bind(object control, EventTrigger eventTriggerType, IDelegateCommand command)
        {
            Bind(control, eventTriggerType, command, null);
        }

        /// <summary>
        /// Binds a single UI Control to a Thread background thread command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        public void Bind<TResult>(object control, EventTrigger eventTrigger, ITaskCommand<TResult> command)
        {
            Bind(control, eventTrigger, command, null);
        }

        /// <summary>
        /// Binds a single UI Control to a System Thread Task.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        /// <param name="parameter">The action that will be invoked when the command is executed.</param>
        public void Bind<TResult>(object control, EventTrigger eventTrigger, ITaskCommand<TResult> command, object parameter)
        {
            SetBind(control, eventTrigger, command, parameter);
        }

        /// <summary>
        /// Unbinds the UI Control form it's command.
        /// </summary>
        /// <param name="control">UI Control to be unbounded.</param>
        public void UnBind(object control)
        {
            var binding = Bindings.FirstOrDefault(b => b.Control == control);
            if (binding == null)
                return;

            binding.Event.RemoveEventHandler(control, binding.EventHandler);

            Bindings.Remove(binding);
        }

        /// <summary>
        /// Unbinds all UI Controls from it's commands.
        /// </summary>
        public void UnBindAll()
        {
            foreach (var commandBinding in Bindings)
            {
                commandBinding.Event.RemoveEventHandler(commandBinding.Control, commandBinding.EventHandler);
            }

            Bindings.Clear();
        }

        /// <summary>
        /// Evaluates all commands state.
        /// </summary>
        public void EvaluateCommands()
        {
            foreach (var commandBinding in Bindings.Where(commandBinding => commandBinding.Command != null))
            {
                commandBinding.Command.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Evaluates the UI Control command state. 
        /// </summary>
        /// <param name="control">The UI Control of which it's command is to be evaluated.</param>
        public void EvaluateCommand(object control)
        {
            var binding = Bindings.FirstOrDefault(b => b.Control == control);
            if (binding == null)
                return;

            binding.Command.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Assigns the command on the UI Control with a parameter.
        /// </summary>
        /// <param name="control">UI Control of which has been bounded.</param>
        /// <param name="parameter">The command parameter to be set.</param>
        public void SetCommandParameter(object control, object parameter)
        {
            var binding = Bindings.FirstOrDefault(b => b.Control == control);

            if (binding == null || binding.Command == null)
                return;

            binding.CommandParameter = parameter;
            binding.Command.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Assigns a parameter to a single command.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameter"></param>
        public void SetCommandParameter(IDelegateCommand command, object parameter)
        {
            var binding = Bindings.FirstOrDefault(b => b.Command == command);

            if (binding == null || binding.Command == null)
                return;

            binding.CommandParameter = parameter;
            binding.Command.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Assigns a parameter to a one or more commands of the same type.
        /// </summary>
        /// <typeparam name="TCommand">Type of command.</typeparam>
        /// <param name="parameter">command parameter to be assigned.</param>
        public void SetCommandParameter<TCommand>(object parameter)
        {
            var bindings = Bindings.Where(b => b.Command.GetType() == typeof(TCommand)).ToList();
            foreach (var binding in bindings)
            {
                binding.CommandParameter = parameter;
                binding.Command.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Executes the command with a parameter.
        /// </summary>
        /// <param name="command">The command to be executed.</param>
        /// <param name="parameter">The command parameter to be used during the execution process.</param>
        public void Execute(IDelegateCommand command, object parameter)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            if (command.CanExecute(parameter))
                command.Execute(parameter);
        }

        /// <summary>
        /// Executes a single command without a parameter.
        /// </summary>
        /// <param name="command">The command to be executed.</param>
        public void Execute(IDelegateCommand command)
        {
            Execute(command, null);
        }

        /// <summary>
        /// Executes a command without a parameter.
        /// </summary>
        /// <param name="control">The control that contains a command to be executed.</param>
        /// <exception cref="NullReferenceException">Raised when no command is available.</exception>
        public void Execute(object control)
        {
            var command = GetCommand(control);
            if (command == null)
                throw new NullReferenceException("Missing IDelegateCommand");

            Execute(command);
        }

        /// <summary>
        /// Executes a command without a parameter.
        /// </summary>
        /// <param name="control">The control that contains a command to be executed.</param>
        /// <param name="parameter">The command parameter to be used during the execution process.</param>
        public void Execute(object control, object parameter)
        {
            var command = GetCommand(control);
            if (command == null)
                throw new NullReferenceException("Missing IDelegateCommand");

            Execute(command, parameter);
        }

        /// <summary>
        /// Executes a task command
        /// </summary>
        /// <typeparam name="TResult">Returns the result once exection has completed.</typeparam>
        /// <param name="command">Command to be executed.</param>
        public void Execute<TResult>(ITaskCommand<TResult> command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            if (command.Action == null)
                throw new ArgumentNullException("command.Action");

            if (command.CanExecute(command.Action))
                command.Execute(command.Action);
        }

        /// <summary>
        /// Return a list of commands by type.
        /// </summary>
        /// <typeparam name="TCommand">Type of command to return.</typeparam>
        /// <returns>List of IDelegate commands.</returns>
        public List<IDelegateCommand> GetCommand<TCommand>()
        {
            var bindings = Bindings.Where(b => b.Command.GetType() == typeof(TCommand)).ToList();
            return bindings.Select(binding => binding.Command).ToList();
        }

        private void CanExecuteChangedEvent(object sender, EventArgs e)
        {
            var command = sender as IDelegateCommand;
            if (command == null)
                return;

            foreach (var binding in Bindings.Where(b => b.Command == command))
            {
                var method = binding.Control.GetType().GetProperty("Enabled");

                if (method == null)
                    continue;

                method.SetValue(binding.Control, command.CanExecute(binding.CommandParameter), null);
            }
        }

        private void SetBind(object control, EventTrigger bindingType, IDelegateCommand command, object commandParameter)
        {
            switch (bindingType)
            {
                case EventTrigger.Click:
                    BindClickEvent(control, command, commandParameter);
                    break;
                case EventTrigger.DoubleClick:
                    BindDoubleClickEvent(control, command, commandParameter);
                    break;
            }
            command.CanExecuteChanged += (CanExecuteChangedEvent);
        }

        private void BindClickEvent(object control, IDelegateCommand command, object commandParameter)
        {
            var evt = GetEvent(control, "Click");

            if (evt == null || evt.EventHandlerType == null)
                throw new ArgumentException(string.Format("Click event not supported on {0}", control.GetType().Name));

            var method = typeof(CommandManager).GetMethod("ControlClickEvent");
            var binding = new CommandBinding(command, commandParameter, control, EventTrigger.Click)
            {
                EventHandler = Delegate.CreateDelegate(evt.EventHandlerType, this, method),
                Event = evt
            };
            binding.Event.AddEventHandler(control, binding.EventHandler);
            Bindings.Add(binding);
        }

        private void BindDoubleClickEvent(object control, IDelegateCommand command, object commandParameter)
        {
            var evt = GetEvent(control, "DoubleClick");

            if (evt == null || evt.EventHandlerType == null)
                throw new ArgumentException(string.Format("DoubleClick event not supported on {0}", control.GetType().Name));

            var method = typeof(CommandManager).GetMethod("ControlDoubleClickEvent");
            var binding = new CommandBinding(command, commandParameter, control, EventTrigger.DoubleClick)
            {
                EventHandler = Delegate.CreateDelegate(evt.EventHandlerType, this, method),
                Event = evt
            };
            binding.Event.AddEventHandler(control, binding.EventHandler);
            Bindings.Add(binding);
        }

        private static EventInfo GetEvent(object control, string eventName)
        {
            return control.GetType().GetEvent(eventName);
        }

        /// <summary>
        /// The event fired during the DoubleClick event execution. Not intended for use outside this manager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ControlDoubleClickEvent(object sender, EventArgs e)
        {
            var binding = Bindings.FirstOrDefault(b => b.Control == sender && b.EventTrigger == EventTrigger.DoubleClick);

            if (binding == null)
                return;

            if (binding.Command == null)
                return;

            if (binding.Command.CanExecute(binding.CommandParameter))
                binding.Command.Execute(binding.CommandParameter);
        }
        /// <summary>
        /// The event fired during the Click event execution. Not intended for use outside this manager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ControlClickEvent(object sender, EventArgs e)
        {
            var binding = Bindings.FirstOrDefault(b => b.Control == sender && b.EventTrigger == EventTrigger.Click);

            if (binding == null)
                return;

            if (binding.Command == null)
                return;

            if (binding.Command.CanExecute(binding.CommandParameter))
                binding.Command.Execute(binding.CommandParameter);
        }
    }
}
