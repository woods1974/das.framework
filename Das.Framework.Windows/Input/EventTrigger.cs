namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Indiciates which event should be used during the UI control binding process.
    /// </summary>
    public enum EventTrigger
    {
        /// <summary>
        /// Click Event
        /// </summary>
        Click,
        /// <summary>
        /// Double Click Event
        /// </summary>
        DoubleClick
    }
}