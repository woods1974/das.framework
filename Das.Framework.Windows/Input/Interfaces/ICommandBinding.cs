using System;
using System.Reflection;

namespace Das.Framework.Windows.Input.Interfaces
{
    /// <summary>
    /// Used to bind a single Command to a UI Control and it's event handler.
    /// </summary>
    public interface ICommandBinding 
    {
        /// <summary>
        /// The UI control of which the command is bounded to.
        /// </summary>
        object Control { get; set; }
        /// <summary>
        /// Indicates which event should trigger the command execution.
        /// </summary>
        EventTrigger EventTrigger { get; set; }

        /// <summary>
        /// The event handler of which the command is triggered to execute from.
        /// </summary>
        Delegate EventHandler { get; set; }

        /// <summary>
        /// Used to discover whether the event requested is contained on the UI Control such as Click or Double Click.
        /// </summary>
        EventInfo Event { get; set; }
    }
}