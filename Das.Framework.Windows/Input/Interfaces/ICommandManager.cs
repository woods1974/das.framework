using System;
using System.Collections.Generic;

namespace Das.Framework.Windows.Input.Interfaces
{
    public interface ICommandManager : IDisposable
    {
        /// <summary>
        /// Unique Identifier 
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Number of bindings contained by the command manager. 
        /// </summary>
        int BindingCount { get; }

        /// <summary>
        /// Return an existing binding.
        /// </summary>
        /// <param name="control">Control that is registered with the command manager.</param>
        /// <returns>Returns an existing binding associated with the control.</returns>
        ICommandBinding GetCommandBinding(object control);

        /// <summary>
        /// Obtains the command on the control.
        /// </summary>
        /// <param name="control">Control containing the IDelegateCommand property.</param>
        /// <returns>Returns the first IDelegateCommand on the control</returns>
        IDelegateCommand GetCommand(object control);

        /// <summary>
        /// Bind the UI Control to the first instrance of a IDelegate Command property and setting 
        /// the default parameter.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property.</param>
        /// <param name="parameter">The parameter to be used during the exection.</param>
        void Bind(object control, object parameter);

        /// <summary>
        /// Bind the UI Control to the IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property.</param>
        /// <param name="command">DelegateCommand to bind.</param>
        void Bind(object control, IDelegateCommand command);

        /// <summary>
        /// Bind the UI Control to the IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="parameter">The parameter used during the evaluation and execution of the command.</param>
        /// <param name="control">UI Control to bind to.</param>
        /// <param name="command">DelegateCommand to bind.</param>
        void Bind(object control, IDelegateCommand command, object parameter);

        /// <summary>
        /// Bind the UI Control to the first instrance of a IDelegate Command.
        /// Click Event will be the default event trigger.
        /// </summary>
        /// <param name="control">Control containing the IDelegate Command property</param>
        void Bind(object control);

        /// <summary>
        /// Bind the UI Control to the first instrance of a IDelegate Command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <exception cref="NullReferenceException">Raised when no command is available.</exception>
        void Bind(object control, EventTrigger eventTrigger);

        /// <summary>
        /// Binds a UI Control to a command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        /// <param name="commandParameter">Command parameter used during the evaluation and execution of the command.</param>
        void Bind(object control, EventTrigger eventTrigger, IDelegateCommand command, object commandParameter);

        /// <summary>
        /// Binds a UI Control to a command.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        void Bind(object control, EventTrigger eventTrigger, IDelegateCommand command);

        /// <summary>
        /// Binds a UI Control to a System Thread Task.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        void Bind<TResult>(object control, EventTrigger eventTrigger, ITaskCommand<TResult> command);

        /// <summary>
        /// Binds a UI Control to a System Thread Task.
        /// </summary>
        /// <param name="control">UI Control of which the command belongs to.</param>
        /// <param name="eventTrigger">Indiciates which trigger should be used to execute the command.</param>
        /// <param name="command">The command of which is to be executed when the Event Trigger is fired.</param>
        /// <param name="parameter">Parameter that will be used during the execution of the command. </param>
        void Bind<TResult>(object control, EventTrigger eventTrigger, ITaskCommand<TResult> command, object parameter);

        /// <summary>
        /// Unbinds the UI Control form it's command.
        /// </summary>
        /// <param name="control">UI Control to be unbounded.</param>
        void UnBind(object control);

        /// <summary>
        /// Unbinds all UI Controls from it's commands.
        /// </summary>
        void UnBindAll();

        /// <summary>
        /// Evaluates the state of each command.
        /// </summary>
        void EvaluateCommands();

        /// <summary>
        /// Evaluates the state of the command. 
        /// </summary>
        /// <param name="control">The UI Control of which it's command is to be evaluated.</param>
        void EvaluateCommand(object control);

        /// <summary>
        /// Assigns the command on the UI Control with a parameter.
        /// </summary>
        /// <param name="control">UI Control of which has been bounded.</param>
        /// <param name="parameter">The command parameter to be set.</param>
        void SetCommandParameter(object control, object parameter);

        /// <summary>
        /// Assigns a parameter to a single command.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameter"></param>
        void SetCommandParameter(IDelegateCommand command, object parameter);

        /// <summary>
        /// Assigns a parameter to a one or more commands of the same type.
        /// </summary>
        /// <typeparam name="TCommand">Type of command.</typeparam>
        /// <param name="parameter">command parameter to be assigned.</param>
        void SetCommandParameter<TCommand>(object parameter);

        /// <summary>
        /// Executes the nominated command with a parameter.
        /// </summary>
        /// <param name="command">The command to be executed.</param>
        /// <param name="parameter">The command parameter to be used during the execution process.</param>
        void Execute(IDelegateCommand command, object parameter);

        /// <summary>
        /// Executes a single command without a parameter.
        /// </summary>
        /// <param name="command">The command to be executed.</param>
        void Execute(IDelegateCommand command);

        /// <summary>
        /// Executes a command without a parameter.
        /// </summary>
        /// <param name="control">The control that contains a command to be executed.</param>
        void Execute(object control);

        /// <summary>
        /// Executes a command without a parameter.
        /// </summary>
        /// <param name="control">The control that contains a command to be executed.</param>
        /// <param name="parameter">The command parameter to be used during the execution process.</param>
        void Execute(object control, object parameter);

        /// <summary>
        /// Executes a task command
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="command"></param>
        void Execute<TResult>(ITaskCommand<TResult> command);

        /// <summary>
        /// Return a list of commands by type.
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <returns></returns>
        List<IDelegateCommand> GetCommand<TCommand>();
    }
}