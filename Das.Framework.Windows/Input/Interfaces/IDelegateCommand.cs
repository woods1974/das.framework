﻿using System;
using System.Windows.Input;

namespace Das.Framework.Windows.Input.Interfaces
{
    /// <summary>
    /// Interface for executing a preconfigured action with the support of command state change notification.
    /// </summary>
    public interface IDelegateCommand : ICommand, IDisposable
    {
        /// <summary>
        /// Notify the state of the command.
        /// </summary>
        void RaiseCanExecuteChanged();
    }
}
