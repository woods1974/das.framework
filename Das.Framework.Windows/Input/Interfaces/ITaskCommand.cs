using System;
using System.Threading;

namespace Das.Framework.Windows.Input.Interfaces
{
    public interface ITaskCommand<TResult> : IDelegateCommand
    {
        event EventHandler<TaskCompletedResult> TaskCompleted;
        event EventHandler<TaskCancelledResult> TaskCancelled;
        Func<TResult> Action { get; set; }
        TResult Result { get; }
        bool IsCancelled { get; }
        bool IsCompleted { get; }
        bool IsFaulted { get; }
        bool IsRunning { get; }
        bool SupportCancellation { get; }
        CancellationToken CancellationToken { get; }
        Exception Exception { get; }
        void Execute();
        void Cancel();
    }
}