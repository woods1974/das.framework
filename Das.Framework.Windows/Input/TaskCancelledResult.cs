using System;

namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Class to determine whether a background task was cancelled during execution.
    /// </summary>
    public class TaskCancelledResult : EventArgs
    {
        public TaskCancelledResult()
        { }
        public TaskCancelledResult(bool cancelled)
        {
            IsCancelled = cancelled;
        }
        /// <summary>
        /// Determines whether the background task was cancelled during execution.
        /// </summary>
        public bool IsCancelled { get; private set; }
    }
}