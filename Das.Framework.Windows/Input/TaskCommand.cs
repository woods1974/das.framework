﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Das.Framework.Windows.Input.Interfaces;

namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Executes a System Thread Task (background task).
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class TaskCommand<TResult> : ITaskCommand<TResult>
    {
        private CancellationTokenSource _cancellationTokenSource;

        private Task<TResult> _task;

        /// <summary>
        /// Executes a System Thread Task.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        public TaskCommand(bool supportCancellation)
        {
            SupportCancellation = supportCancellation;
            IntitaliseTaskCommand();
        }

        private void IntitaliseTaskCommand()
        {
            if (!SupportCancellation)
                return;

            _cancellationTokenSource = new CancellationTokenSource();
            CancellationToken = _cancellationTokenSource.Token;
            CancellationToken.Register(RaiseCancelNotification);
        }
        /// <summary>
        /// Determines if the command is in a valid state before it is executed on the background task.
        /// Returns true, when an action exists and the existing background task is not running.
        /// </summary>
        /// <param name="parameter">Payload the can be evaluated and used for execution.</param>
        /// <returns></returns>
        public virtual bool CanExecute(object parameter)
        {
            return Action != null && !IsRunning;
        }

        /// <summary>
        /// Executes the command on the background task.
        /// </summary>
        public void Execute()
        {
            IsCancelled = false;

            _task = SupportCancellation ? Task.Factory.StartNew(Action, CancellationToken) :
                                          Task.Factory.StartNew(Action);

            Task.Factory.ContinueWhenAll(new Task[] { _task }, (tasks) =>
            {
                if (TaskCancelled != null && IsCancelled)
                {
                    TaskCancelled.Invoke(this, new TaskCancelledResult(true));
                    return;
                }

                if (TaskCompleted != null)
                {
                    TaskCompleted.Invoke(this,
                        new TaskCompletedResult(tasks[0].Exception, !tasks[0].IsFaulted));
                }
            });



        }

        /// <summary>
        /// Propagates notification that the action should be cancelled.
        /// </summary>
        public CancellationToken CancellationToken { get; private set; }

        /// <summary>
        /// When invoked, will notify the background task to cancel the execution.
        /// The task will only be cancelled if the action is running and support cancellation is true.
        /// </summary>
        public void Cancel()
        {
            if (IsRunning && SupportCancellation && CancellationToken.CanBeCanceled)
            {
                IsCancelled = true;
                _cancellationTokenSource.Cancel();

            }
        }

        /// <summary>
        /// When true, indicates that the background task had failed due to an unhandled exception.
        /// </summary>
        public bool IsFaulted
        {
            get { return _task.IsFaulted; }
        }

        /// <summary>
        /// Determines whether the background task is executing.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return _task != null && _task.Status == TaskStatus.Running;
            }
        }

        /// <summary>
        /// Indicates whether the background task should support cancellation during execution.
        /// </summary>
        public bool SupportCancellation { get; private set; }

        /// <summary>
        /// The exception returned when the background task has faulted.
        /// </summary>
        public Exception Exception
        {
            get { return _task.Exception; }
        }

        /// <summary>
        /// Invokes the Background tasks cancel notification.
        /// </summary>
        private void RaiseCancelNotification()
        {
            if (TaskCancelled != null)
                TaskCancelled.Invoke(this, new TaskCancelledResult(true));
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public void Execute(object parameter)
        {
            if (Action == null)
                throw new ArgumentException("parameter must of type Func<TResult>");

            Execute();
        }

        /// <summary>
        /// Determines if the command be executed.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// raised when the background task has completed regardless to the Fault state.
        /// </summary>
        public event EventHandler<TaskCompletedResult> TaskCompleted;

        /// <summary>
        /// Raised when the background task has been cancelled.
        /// </summary>
        public event EventHandler<TaskCancelledResult> TaskCancelled;



        /// <summary>
        /// The Action to be invoked by the background task.
        /// </summary>
        public Func<TResult> Action { get; set; }

        /// <summary>
        /// The result returned from the background task.
        /// </summary>
        public TResult Result
        {
            get { return _task.Result; }
        }

        /// <summary>
        /// Determines the Command state.
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Indicates whetherhe background task was cancelled.
        /// </summary>
        public bool IsCancelled
        {
            get;
            private set;
        }

        /// <summary>
        /// Determines where the background task was completed.
        /// </summary>
        public bool IsCompleted
        {
            get { return _task != null && _task.IsCompleted; }
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (IsRunning)
                {
                    Cancel();
                    _task.Wait();
                }

                _task.Dispose();
                _cancellationTokenSource.Dispose();
            }

        }

        #endregion
    }
}
