﻿using System;

namespace Das.Framework.Windows.Input
{
    /// <summary>
    /// Class to support the Task Command Completed Result.
    /// </summary>
    public class TaskCompletedResult : EventArgs
    {
        public TaskCompletedResult(Exception exception, bool success)
        {
            Success = success;
            Exception = exception;
        }
        /// <summary>
        /// Determines whether the task had completed successfully.
        /// </summary>
        public bool Success { get; private set; }
        /// <summary>
        /// If faulted, holds the exception that was raised within the background task.
        /// </summary>
        public Exception Exception { get; private set; }
    }
}
