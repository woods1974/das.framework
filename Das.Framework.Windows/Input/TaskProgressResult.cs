using System;

namespace Das.Framework.Windows.Input
{
    public class TaskProgressResult : EventArgs
    {
        public TaskProgressResult()
        { }

        public TaskProgressResult(string message, object data)
        {
            Message = message;
            Data = data;
        }

        public string Message { get; set; }

        public object Data { get; set; }
    }
}